#![cfg(feature = "serializable_config")]
use serde::Serializer;
use xand_secrets::ExposeSecret;
pub use xand_secrets::Secret;

/// Utility method to expose the secret for Serialization
pub(crate) fn serialize_secret<S>(x: &Option<Secret<String>>, s: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    match x {
        None => s.serialize_none(),
        Some(sec) => s.serialize_some(sec.expose_secret()),
    }
}

#[cfg(test)]
mod tests {
    use super::serialize_secret;

    use serde::{Deserialize, Serialize};
    use xand_secrets::{ExposeSecret, Secret};

    use insta::assert_yaml_snapshot;

    #[derive(Debug, Serialize, Deserialize)]
    struct TestSecretContainer {
        #[serde(serialize_with = "serialize_secret")]
        pub secret: Option<Secret<String>>,
    }

    #[test]
    fn serialize_secret_none_serializes_to_null() {
        assert_yaml_snapshot!(TestSecretContainer { secret: None });
    }

    #[test]
    fn serialize_secret_some_serializes_to_secret_value() {
        assert_yaml_snapshot!(TestSecretContainer {
            secret: Some(Secret::new("somesecret".to_string())),
        })
    }

    #[test]
    fn serialize_secret_serde_roundtrip() {
        // Given
        let s = TestSecretContainer {
            secret: Some(Secret::new("somesecret".to_string())),
        };
        let serialized = serde_yaml::to_string(&s).unwrap();

        // When
        let deserialized: TestSecretContainer = serde_yaml::from_str(&serialized).unwrap();

        // Then
        assert_eq!(deserialized.secret.unwrap().expose_secret(), "somesecret");
    }
}
