#[cfg(test)]
mod tests;

#[cfg(feature = "serializable_config")]
use serde::Serialize;

use serde::{de, Deserialize};
use std::path::PathBuf;

use std::str::FromStr;
pub use url::Url;
pub use xand_address::Address;
pub use xand_secrets::Secret;
pub use xand_secrets_local_file::LocalFileSecretStoreConfiguration;
pub use xand_secrets_vault::VaultConfiguration;

/// The Config struct required for starting an instance of the Member API.
/// By default, MemberAPIConfig does not impl Serialize since it contains Secrets. Within the Member API,
/// we want to disallow serializing the struct to avoid accidentally serializing secrets to logs or be included in an http response.
/// Opt-in to this feature for a Serializable config type. (See xand-network-generator for example usage)
#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "kebab-case")]
#[cfg_attr(feature = "serializable_config", derive(Serialize))]
pub struct MemberApiConfig {
    pub path: Option<String>,
    pub port: Option<u64>,
    pub workers: Option<usize>,

    #[serde(default = "default_database")]
    pub database: PathBuf,

    /// If set, uses the JWT secret identified by this key to authorize all requests to the webserver
    pub jwt_secret: Option<String>,

    pub xand_api: XandApiConfig,

    pub secret_store: SecretStoreConfig,

    #[serde(deserialize_with = "parse_address")]
    pub address: Address,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "kebab-case")]
#[serde(deny_unknown_fields)]
#[cfg_attr(feature = "serializable_config", derive(Serialize))]
pub enum SecretStoreConfig {
    Vault(VaultConfiguration),
    LocalFile(LocalFileSecretStoreConfiguration),
}

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "kebab-case")]
#[serde(deny_unknown_fields)]
#[cfg_attr(feature = "serializable_config", derive(Serialize))]
pub struct XandApiConfig {
    #[serde(with = "url_serde")]
    pub url: Url,
    pub auth: Option<XandApiAuthConfig>,
    pub timeout_seconds: Option<u64>,
}

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "kebab-case")]
#[serde(deny_unknown_fields)]
#[cfg_attr(feature = "serializable_config", derive(Serialize))]
pub struct XandApiAuthConfig {
    /// The key to lookup the JWT used to authenticate against the XandApi
    pub jwt: Option<String>,
}

pub fn default_database() -> PathBuf {
    "./data/member-api.db".into()
}

fn parse_address<'de, D: de::Deserializer<'de>>(
    deserializer: D,
) -> std::result::Result<Address, D::Error> {
    let s: String = de::Deserialize::deserialize(deserializer)?;
    let address = Address::from_str(&s).map_err(de::Error::custom)?;
    Ok(address)
}
