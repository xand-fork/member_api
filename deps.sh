sudo apt update
sudo apt install \
    libsqlite3-dev

cargo install diesel_cli --no-default-features --features "sqlite-bundled"