<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Docs](#docs)
  - [Tools](#tools)
    - [doctoc](#doctoc)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Docs

These docs are mainly intended for engineering use. We maintain user-facing docs in the [`xand-qa` repo](https://gitlab.com/TransparentIncDevelopment/product/testing/xand-qa/-/tree/master/user-guides/members). 
In order to reduce duplication, and where possible, engineering docs should reference user-facing docs. 

We should be careful when producing user-facing docs that they do not carry implicit assumptions about access
to source code, this repo, or private artifact repositories.

## Tools

### doctoc

`doctoc` is used to re-generate the table of contents based off of the headers defined
in the markdown documents.

```bash
npm install -g doctoc
```

To regenerate the table of contents for the README,

```bash
doctoc .
```