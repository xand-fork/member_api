#!/bin/bash
set -e
IMAGE=transparentinc-docker-external.jfrog.io/member-api
OUTPUT_FILE=xand.yaml
K8S_NAMESPACE=xand
OVERLAY=overlays/default

##### Main
while [ "$1" != "" ]; do
    case $1 in
        -a | --member-address ) shift
            MEMBER_ADDRESS=$1
            ;;

        -d | --dns-address ) shift
            MEMBER_API_URL=$1
            ;;

        -i | --docker-image ) shift
            IMAGE=$1
            ;;

        -l | --overlay ) shift
            OVERLAY=$1
            ;;

        -n | --kubernetes-namespace ) shift
            K8S_NAMESPACE=$1
            ;;

        -f | --input-file ) shift
            OUTPUT_FILE=$1
            ;;

        -v | --version ) shift
            VERSION=$1
            ;;

        -h | --help ) usage
            exit
            ;;
    esac
    shift
done

### INTERACTIVE
while [ -z "${K8S_NAMESPACE+x}" ]; do
  echo "What namespace will you be deploying to in kubernetes?"
  read K8S_NAMESPACE
done

while [ -z "$MEMBER_API_URL" ]; do
  echo "What is the DNS entry that will point to Member API?"
  read MEMBER_API_URL
done

while [ -z "$VERSION" ]; do
  echo "What is the version of the member api that will be deployed?"
  read VERSION
done

MEMBER_API_DOCKER_IMAGE=$IMAGE:$VERSION

##### Functions
usage()
{
    echo "USAGE:"
    echo "Execute the script without parameters to be prompted for the required parameters."
    echo ""
    echo "PARAMETERS:"
    echo "-a | --member-address        : (Required):  Specify your member's wallet address."
    echo "-d | --dns-address           : (Required):  Specify the member api dns entry."
    echo "-v | --version               : (Required):  Specify the member api version."
    echo "-n | --kubernetes-namespace  : (Optional):  Specify the kubernetes namespace. Default is $K8S_NAMESPACE."
    echo "-f | --input -file           : (Optional):  Specify the input file. Default value is $OUTPUT_FILE. It will be deleted and recreated with each run of this script."
    echo "-i | --docker-image          : (Optional):  Specify the docker image. Default is $IMAGE."
    echo "-h | --help                  : Display usage information."
}

display_settings () {
    echo Creating kubernetes configuration file $OUTPUT_FILE with the following parameters:
    echo Member API Docker Image : $MEMBER_API_DOCKER_IMAGE
    echo Member API DNS          : $MEMBER_API_URL
    echo Member Address          : $MEMBER_ADDRESS
    echo Kubernetes Namespace    : $K8S_NAMESPACE
    echo Input File              : $OUTPUT_FILE
}

sedi () {
    sed --version >/dev/null 2>&1 && sed -i -- "$@" || sed -i "" "$@"
}

### SCRIPT SECTION
display_settings

# Replace "." with "-"
MEMBER_API_TLS_SECRET=$(echo $MEMBER_API_URL | sed "s/\./-/g")

kustomize build $OVERLAY > $OUTPUT_FILE

sedi "s,#{member.address},$MEMBER_ADDRESS,g" $OUTPUT_FILE
sedi "s,#{member-api-url},$MEMBER_API_URL,g" $OUTPUT_FILE
sedi "s,#{member-api-tls-secret},$MEMBER_API_TLS_SECRET,g" $OUTPUT_FILE
sedi "s,#{docker.image.name},$MEMBER_API_DOCKER_IMAGE,g" $OUTPUT_FILE
sedi "s/#{kubernetes.namespace}/$K8S_NAMESPACE/g" $OUTPUT_FILE

echo Review the output file $OUTPUT_FILE. If you are happy with the result, you can deploy using:
echo 
echo "    kubectl apply -f $OUTPUT_FILE"
echo 
