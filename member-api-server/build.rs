#![forbid(unsafe_code)]

use static_files::resource_dir;

fn main() {
    resource_dir("./content").build().unwrap();
}
