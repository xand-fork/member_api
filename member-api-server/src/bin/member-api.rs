#![forbid(unsafe_code)]

use member_api_server::{cli::Cli, lib_main, Result};
use structopt::StructOpt;

#[actix_rt::main]
async fn main() -> Result<()> {
    lib_main(Cli::from_args(), true).await
}
