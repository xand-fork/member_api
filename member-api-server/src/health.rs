use crate::web_server::ServerState;
use actix_web::{
    web::{scope, Data},
    HttpResponse, Scope,
};
use member_api::health::ServiceHealth;

/// Configure routes on an http server.
pub fn routes() -> Scope {
    scope("/api/v1/service/health").service(health)
}

#[actix_web::get("")]
async fn health(state: Data<ServerState>) -> HttpResponse {
    health_impl(state).await
}

/// Turns out, actix web handlers aren't compiled as functions
/// when using the macro to set the route.
/// You can't call one from the other so if you need to share
/// logic between route handlers you HAVE to extract into a
/// separate function.
async fn health_impl(state: Data<ServerState>) -> HttpResponse {
    let services = state.service.services();
    let result = ServiceHealth::check(services).await;

    if result.up() {
        HttpResponse::Ok().json(&result)
    } else {
        HttpResponse::InternalServerError().json(&result)
    }
}

#[cfg(test)]
mod tests {
    use crate::{
        service::MockMemberService,
        tests::test_context::{web_service::client::TestWebServiceClient, TestContext},
    };
    use http::StatusCode;
    use member_api::health::{Service, ServiceStatus};

    #[derive(Clone)]
    struct MockService {
        name: String,
        health: ServiceStatus,
    }
    impl MockService {
        fn new(name: &str, health: ServiceStatus) -> Self {
            Self {
                name: name.to_string(),
                health,
            }
        }
    }
    #[async_trait::async_trait]
    impl Service for MockService {
        fn name(&self) -> String {
            self.name.clone()
        }

        async fn health(&self) -> ServiceStatus {
            self.health.clone()
        }
    }

    #[actix_rt::test]
    async fn all_services_up_returns_ok() {
        // Given
        let mut service = MockMemberService::new();
        service.expect_services().returning(|| {
            let services: Vec<Box<dyn Service>> = vec![Box::new(MockService::new(
                "My Mock Service",
                ServiceStatus::Up,
            ))];
            services
        });

        let context = TestContext::default();
        let app = context.start_web_server_with_service(service).await;

        // When
        let resp = app.get("/api/v1/service/health").await;

        // Then
        assert_eq!(StatusCode::OK, resp.status());
    }

    #[actix_rt::test]
    async fn one_service_down_returns_err() {
        let mut service = MockMemberService::new();
        service.expect_services().returning(|| {
            let services: Vec<Box<dyn Service>> = vec![
                Box::new(MockService::new(
                    "My Mock Service",
                    ServiceStatus::Down {
                        details: "Nun of your beeswax!".to_string(),
                    },
                )),
                Box::new(MockService::new("Another mocky service", ServiceStatus::Up)),
                Box::new(MockService::new(
                    "And another mocked service",
                    ServiceStatus::Up,
                )),
            ];
            services
        });

        let context = TestContext::default();
        let app = context.start_web_server_with_service(service).await;

        // When
        let resp = app.get("/api/v1/service/health").await;

        // Then
        assert_eq!(StatusCode::INTERNAL_SERVER_ERROR, resp.status());
    }

    #[actix_rt::test]
    async fn all_services_down_returns_err() {
        let mut service = MockMemberService::new();
        service.expect_services().returning(|| {
            let services: Vec<Box<dyn Service>> = vec![Box::new(MockService::new(
                "My Mock Service",
                ServiceStatus::Down {
                    details: "Nun of your beeswax!".to_string(),
                },
            ))];
            services
        });

        let context = TestContext::default();
        let app = context.start_web_server_with_service(service).await;

        // When
        let resp = app.get("/api/v1/service/health").await;

        // Then
        assert_eq!(StatusCode::INTERNAL_SERVER_ERROR, resp.status());
    }
}
