pub const FRIENDLY_ACCESS_FAILURE_MESSAGE: &str = "Could not connect to the database";
pub const FRIENDLY_VALIDATION_FAILURE_MESSAGE: &str = "The request data is invalid";
pub const FRIENDLY_UNKNOWN_FAILURE_MESSAGE: &str = "An unknown error occurred";
pub const FRIENDLY_NOT_FOUND_MESSAGE: &str = "Bank account was not found";
pub const FRIENDLY_REPOSITORY_FAILURE_MESSAGE: &str =
    "The database encountered an error during its action";
