pub mod constants;

use crate::accounts::errors::constants::{
    FRIENDLY_ACCESS_FAILURE_MESSAGE, FRIENDLY_NOT_FOUND_MESSAGE,
    FRIENDLY_REPOSITORY_FAILURE_MESSAGE, FRIENDLY_UNKNOWN_FAILURE_MESSAGE,
    FRIENDLY_VALIDATION_FAILURE_MESSAGE,
};
use actix_web::HttpResponse;
use member_api::accounts::repository::errors::AccountsRepositoryError;

use crate::{error::ErrorContent, LoggingEvent};

fn log_error_and_create_friendly_error_content(
    msg: String,
    obfuscated_msg: String,
    should_obfuscate: bool,
) -> ErrorContent {
    error!(LoggingEvent::AccountsRepositoryError(msg.clone()));
    ErrorContent {
        message: if should_obfuscate {
            obfuscated_msg
        } else {
            msg
        },
    }
}

pub(crate) fn create_http_error_response(error: AccountsRepositoryError) -> HttpResponse {
    let error_content = create_error_content(error.clone());
    match error {
        AccountsRepositoryError::AccessFailure { .. } => {
            HttpResponse::ServiceUnavailable().json(error_content)
        }
        AccountsRepositoryError::ValidationFailure { .. } => {
            HttpResponse::BadRequest().json(error_content)
        }
        AccountsRepositoryError::Unknown { .. } => {
            HttpResponse::InternalServerError().json(error_content)
        }
        AccountsRepositoryError::NotFound => HttpResponse::NotFound().json(error_content),
        AccountsRepositoryError::RepositoryFailure { .. } => {
            HttpResponse::InternalServerError().json(error_content)
        }
    }
}

fn create_error_content(error: AccountsRepositoryError) -> ErrorContent {
    let generalize_error_message = error.clone().contains_internal_db_msg();
    match error {
        AccountsRepositoryError::AccessFailure {
            msg,
            source: _source,
        } => log_error_and_create_friendly_error_content(
            msg,
            FRIENDLY_ACCESS_FAILURE_MESSAGE.to_string(),
            generalize_error_message,
        ),
        AccountsRepositoryError::ValidationFailure {
            msg,
            source: _source,
        } => log_error_and_create_friendly_error_content(
            msg,
            FRIENDLY_VALIDATION_FAILURE_MESSAGE.to_string(),
            generalize_error_message,
        ),
        AccountsRepositoryError::Unknown {
            msg,
            source: _source,
        } => log_error_and_create_friendly_error_content(
            msg,
            FRIENDLY_UNKNOWN_FAILURE_MESSAGE.to_string(),
            generalize_error_message,
        ),
        AccountsRepositoryError::NotFound => log_error_and_create_friendly_error_content(
            FRIENDLY_NOT_FOUND_MESSAGE.to_string(),
            FRIENDLY_NOT_FOUND_MESSAGE.to_string(),
            generalize_error_message,
        ),
        AccountsRepositoryError::RepositoryFailure {
            msg,
            source: _source,
        } => log_error_and_create_friendly_error_content(
            msg,
            FRIENDLY_REPOSITORY_FAILURE_MESSAGE.to_string(),
            generalize_error_message,
        ),
    }
}

#[cfg(test)]
pub mod tests {
    use crate::accounts::errors::create_http_error_response;
    use http::StatusCode;
    use member_api::accounts::repository::errors::{
        AccountsRepositoryError, AccountsRepositoryErrorSource,
    };

    #[test]
    fn accounts_api_maps_repository_access_failures() {
        let failure = AccountsRepositoryError::AccessFailure {
            msg: "Arbitrary Failure!".to_string(),
            source: AccountsRepositoryErrorSource::AccountsRepository,
        };

        let failure = create_http_error_response(failure);

        assert_eq!(failure.status(), StatusCode::SERVICE_UNAVAILABLE)
    }

    #[test]
    fn accounts_api_maps_repository_validation_failures() {
        let failure = AccountsRepositoryError::ValidationFailure {
            msg: "Arbitrary Failure!".to_string(),
            source: AccountsRepositoryErrorSource::AccountsRepository,
        };

        let failure = create_http_error_response(failure);

        assert_eq!(failure.status(), StatusCode::BAD_REQUEST)
    }

    #[test]
    fn accounts_api_maps_repository_unknown_failures() {
        let failure = AccountsRepositoryError::Unknown {
            msg: "Arbitrary Failure!".to_string(),
            source: AccountsRepositoryErrorSource::AccountsRepository,
        };

        let failure = create_http_error_response(failure);

        assert_eq!(failure.status(), StatusCode::INTERNAL_SERVER_ERROR)
    }

    #[test]
    fn accounts_api_maps_repository_not_found_failures() {
        let failure = AccountsRepositoryError::NotFound;

        let failure = create_http_error_response(failure);

        assert_eq!(failure.status(), StatusCode::NOT_FOUND)
    }

    #[test]
    fn accounts_api_maps_repository_operation_failure() {
        let failure = AccountsRepositoryError::RepositoryFailure {
            msg: "Arbitrary Failure!".to_string(),
            source: AccountsRepositoryErrorSource::AccountsRepository,
        };

        let failure = create_http_error_response(failure);

        assert_eq!(failure.status(), StatusCode::INTERNAL_SERVER_ERROR);
    }
}
