use crate::{
    accounts::{AccountBalanceResponse, EditAccount, NewAccount},
    service::MockMemberService,
    tests::{
        test_context::{web_service::client::TestWebServiceClient, TestContext},
        ParseResponse,
    },
};
use http::StatusCode;
use member_api::{
    accounts::{
        repository::errors::{AccountsRepositoryError, AccountsRepositoryErrorSource},
        web::ApiAccount,
        Account, AccountDetails,
    },
    banks::BankAccountBalance,
};
use mockall::predicate::eq;

const DEFAULT_ID: i32 = 0;

#[actix_rt::test]
async fn route_get_accounts_happy_path() {
    // Given
    let mut service = MockMemberService::new();
    service
        .expect_accounts()
        .return_const(Ok(TestContext::default_accounts()));
    let context = TestContext::default();
    let app = context.start_web_server_with_service(service).await;

    // When
    let resp = app.get("/api/v1/accounts").await;

    // Then
    assert_eq!(StatusCode::OK, resp.status());

    let body: Vec<ApiAccount> = resp.parse().await.unwrap();
    assert_eq!(body.len(), TestContext::default_accounts().len());
}

#[actix_rt::test]
async fn route_get_account_happy_path() {
    // Given
    let expected = TestContext::default_account(); // pick an account, the default account

    let mut service = MockMemberService::new();
    service
        .expect_account()
        .with(eq(DEFAULT_ID))
        .return_const(Ok(TestContext::default_account()));
    let context = TestContext::default();
    let app = context.start_web_server_with_service(service).await;

    let uri = format!("/api/v1/accounts/{}", DEFAULT_ID);

    // When
    let resp = app.get(&uri).await;

    // Then
    assert_eq!(StatusCode::OK, resp.status());

    let actual: ApiAccount = resp.parse().await.unwrap();
    assert_eq!(expected.id, actual.id);
    assert_eq!(
        expected.masked_account_number(),
        actual.masked_account_number
    );
    assert_eq!(expected.routing_number, actual.routing_number);
    assert_eq!(expected.bank_id, actual.bank_id);
    assert_eq!(expected.short_name, actual.short_name);
}

#[actix_rt::test]
async fn route_get_nonexistent_account_returns_not_found() {
    // Given
    let mut service = MockMemberService::new();
    service
        .expect_account()
        .with(eq(DEFAULT_ID))
        .return_const(Err(member_api::Error::AccountRepositoryErrors {
            source: AccountsRepositoryError::NotFound,
        }));
    let context = TestContext::default();
    let app = context.start_web_server_with_service(service).await;

    let uri = format!("/api/v1/accounts/{}", DEFAULT_ID);

    // When
    let resp = app.get(&uri).await;

    // Then
    assert_eq!(StatusCode::NOT_FOUND, resp.status());
}

#[actix_rt::test]
async fn route_post_happy_path() {
    // Given
    let body = NewAccount {
        bank_id: 0,
        short_name: "Account".to_string(),
        account_number: "123123123".to_string(),
    };

    let account: Account = body.clone().into();

    let ret = AccountDetails {
        id: 0,
        bank_id: 0,
        bank_name: "".to_string(),
        short_name: body.short_name.clone(),
        account_number: body.account_number.clone(),
        routing_number: "".to_string(),
    };

    let mut service = MockMemberService::new();
    service
        .expect_add_account()
        .with(eq(account))
        .return_const(Ok(ret));

    let context = TestContext::default();
    let app = context.start_web_server_with_service(service).await;

    // When
    let resp = app.post_json("/api/v1/accounts", &body).await;

    // Then
    assert_eq!(StatusCode::CREATED, resp.status(),);
}

#[actix_rt::test]
async fn route_post_client_err() {
    // Given
    let body = NewAccount {
        bank_id: 0,
        short_name: "Account".to_string(),
        account_number: "123123123".to_string(),
    };

    let account: Account = body.clone().into();

    let mut service = MockMemberService::new();
    service
        .expect_add_account()
        .with(eq(account))
        .return_const(Err(member_api::Error::AccountRepositoryErrors {
            source: AccountsRepositoryError::ValidationFailure {
                msg: "Unable to validate account".to_string(),
                source: AccountsRepositoryErrorSource::AccountsRepository,
            },
        }));

    let context = TestContext::default();
    let app = context.start_web_server_with_service(service).await;

    // When
    let resp = app.post_json("/api/v1/accounts", &body).await;

    // Then
    assert_eq!(StatusCode::BAD_REQUEST, resp.status(),);
}

#[actix_rt::test]
async fn route_put_happy_path_new_account() {
    // Given
    let account = Account {
        id: Some(DEFAULT_ID),
        bank_id: 0,
        short_name: "fredbob".to_string(),
        account_number: "89887734".to_string(),
    };

    let ret = AccountDetails {
        id: DEFAULT_ID,
        bank_id: 0,
        bank_name: "".to_string(),
        short_name: "fredbob".to_string(),
        account_number: "89887734".to_string(),
        routing_number: "".to_string(),
    };

    let mut service = MockMemberService::new();
    service
        .expect_account()
        .with(eq(DEFAULT_ID))
        .return_const(Err(member_api::Error::AccountRepositoryErrors {
            source: AccountsRepositoryError::NotFound,
        }));

    service
        .expect_update_account()
        .with(eq(account))
        .return_const(Ok(ret));

    let context = TestContext::default();
    let app = context.start_web_server_with_service(service).await;

    let body = EditAccount {
        short_name: "fredbob".to_string(),
        account_number: "89887734".to_string(),
    };

    // When
    let uri = format!("/api/v1/accounts/{}", DEFAULT_ID);

    let resp = app.put_json(&uri, &body).await;

    // Then
    assert_eq!(
        StatusCode::CREATED,
        resp.status(),
        "{:?}",
        resp.into_string().await
    );

    let response: ApiAccount = resp.parse().await.unwrap();
    assert_eq!(body.short_name, response.short_name);
    assert_eq!("XXXXXXXX7734".to_string(), response.masked_account_number);
}

#[actix_rt::test]
async fn route_put_happy_path_existing_account() {
    // Given
    let account = Account {
        id: Some(DEFAULT_ID),
        bank_id: 0,
        short_name: "fredbob".to_string(),
        account_number: "89887734".to_string(),
    };

    let ret = AccountDetails {
        id: DEFAULT_ID,
        bank_id: 0,
        bank_name: "".to_string(),
        short_name: "fredbob".to_string(),
        account_number: "89887734".to_string(),
        routing_number: "".to_string(),
    };

    let mut service = MockMemberService::new();

    service
        .expect_account()
        .with(eq(DEFAULT_ID))
        .return_const(Ok(ret.clone()));

    service
        .expect_update_account()
        .with(eq(account))
        .return_const(Ok(ret));

    let context = TestContext::default();
    let app = context.start_web_server_with_service(service).await;

    let body = EditAccount {
        short_name: "fredbob".to_string(),
        account_number: "89887734".to_string(),
    };

    // When
    let uri = format!("/api/v1/accounts/{}", DEFAULT_ID);

    let resp = app.put_json(&uri, &body).await;

    // Then
    assert_eq!(
        StatusCode::OK,
        resp.status(),
        "{:?}",
        resp.into_string().await
    );

    let response: ApiAccount = resp.parse().await.unwrap();
    assert_eq!(body.short_name, response.short_name);
    assert_eq!("XXXXXXXX7734".to_string(), response.masked_account_number);
}

#[actix_rt::test]
async fn route_put_client_err() {
    // Given
    let account = Account {
        id: Some(DEFAULT_ID),
        bank_id: 0,
        short_name: "fredbob".to_string(),
        account_number: "89887734".to_string(),
    };

    let ret = AccountDetails {
        id: DEFAULT_ID,
        bank_id: 0,
        bank_name: "".to_string(),
        short_name: "fredbob".to_string(),
        account_number: "89887734".to_string(),
        routing_number: "".to_string(),
    };

    let mut service = MockMemberService::new();

    service
        .expect_account()
        .with(eq(DEFAULT_ID))
        .return_const(Ok(ret));

    service
        .expect_update_account()
        .with(eq(account))
        .return_const(Err(member_api::Error::AccountRepositoryErrors {
            source: AccountsRepositoryError::ValidationFailure {
                msg: "Request failed validation".to_string(),
                source: AccountsRepositoryErrorSource::AccountsRepository,
            },
        }));

    let context = TestContext::default();
    let app = context.start_web_server_with_service(service).await;

    let body = EditAccount {
        short_name: "fredbob".to_string(),
        account_number: "89887734".to_string(),
    };

    // When
    let uri = format!("/api/v1/accounts/{}", DEFAULT_ID);

    let resp = app.put_json(&uri, &body).await;

    // Then
    assert_eq!(
        StatusCode::BAD_REQUEST,
        resp.status(),
        "{:?}",
        resp.into_string().await
    );
}

#[actix_rt::test]
async fn route_delete() {
    // Given
    let id = TestContext::default_account().id;

    let mut service = MockMemberService::new();
    service
        .expect_delete_account()
        .with(eq(id))
        .return_const(Ok(()));
    let context = TestContext::default();
    let app = context.start_web_server_with_service(service).await;

    let uri = format!("/api/v1/accounts/{}", id);

    // When
    let resp = app.delete(&uri).await;

    // Then
    assert_eq!(
        StatusCode::OK,
        resp.status(),
        "{:?}",
        resp.into_string().await
    );
}

#[actix_rt::test]
async fn route_balance_happy_path() {
    // Given
    const BALANCE: i64 = 42;
    let mut service = MockMemberService::new();
    service.expect_bank_balance().returning(|_| {
        Box::pin(async {
            Ok(BankAccountBalance {
                current_balance_in_minor_unit: BALANCE,
                available_balance_in_minor_unit: BALANCE,
            })
        })
    });
    let context = TestContext::default();
    let app = context.start_web_server_with_service(service).await;
    let id = 3;

    let uri = format!("/api/v1/accounts/{}/balance", id);

    // When
    let resp = app.get(&uri).await;

    // Then
    assert_eq!(
        StatusCode::OK,
        resp.status(),
        "{:?}",
        resp.into_string().await
    );

    let response: AccountBalanceResponse = resp.parse().await.unwrap();
    assert_eq!(response.available_balance_in_minor_unit, BALANCE);
    assert_eq!(response.account_id, id.to_string());
    assert_eq!(response.current_balance_in_minor_unit, BALANCE);
}

#[actix_rt::test]
async fn route_balance_not_found() {
    // Given
    let mut service = MockMemberService::new();
    service
        .expect_bank_balance()
        .with(eq(DEFAULT_ID))
        .returning(|_| {
            Box::pin(async {
                Err(member_api::Error::AccountRepositoryErrors {
                    source: AccountsRepositoryError::NotFound,
                })
            })
        });
    let context = TestContext::default();
    let app = context.start_web_server_with_service(service).await;

    let uri = format!("/api/v1/accounts/{}/balance", DEFAULT_ID);

    // When
    let resp = app.get(&uri).await;

    // Then
    assert_eq!(
        StatusCode::NOT_FOUND,
        resp.status(),
        "{:?}",
        resp.into_string().await
    );
}
