use member_api::accounts::Account;

pub mod routes;

pub mod errors;
#[cfg(test)]
pub mod tests;

#[derive(Clone, Debug, Deserialize, Eq, PartialEq, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct EditAccount {
    pub short_name: String,
    pub account_number: String,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct AccountBalanceResponse {
    pub account_id: String, // TODO: Convert to i32 after old banks routes are removed.
    pub current_balance_in_minor_unit: i64,
    pub available_balance_in_minor_unit: i64,
}

#[derive(Clone, Debug, Deserialize, Eq, PartialEq, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct NewAccount {
    pub bank_id: i32,
    pub short_name: String,
    pub account_number: String,
}

impl From<NewAccount> for Account {
    fn from(input: NewAccount) -> Self {
        Account {
            id: None,
            bank_id: input.bank_id,
            short_name: input.short_name,
            account_number: input.account_number,
        }
    }
}
