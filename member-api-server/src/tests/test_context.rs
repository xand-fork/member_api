// Do not care in tests
#![allow(clippy::future_not_send)]

use crate::{
    service::MockMemberService,
    tests::test_context::web_service::{start_web_service_test_instance, TestWebServiceClient},
    web_server::{create_route_config, ServerState},
};
use chrono::Utc;
use member_api::{
    accounts::AccountDetails,
    banks::{Bank, BankAdapter, McbAdapterConfig, TreasuryPrimeAdapterConfig},
    transaction::{Status, TransactionState, TransactionType},
    Receipt, Transaction,
};

pub(crate) mod web_service;

#[derive(Default)]
pub(crate) struct TestContext {
    pub(crate) secret: Option<String>,
}

impl TestContext {
    // Since this creates RouteConfig any secrets would need to be set prior to calling this function
    pub async fn start_web_server(&self) -> impl TestWebServiceClient {
        let route_config = create_route_config(&self.secret).unwrap();

        let server_state = self.build_server_state(None);
        start_web_service_test_instance(server_state, route_config).await
    }

    pub async fn start_web_server_with_service(
        &self,
        service: MockMemberService,
    ) -> impl TestWebServiceClient {
        let route_config = create_route_config(&self.secret).unwrap();

        let server_state = self.build_server_state(Some(service));
        start_web_service_test_instance(server_state, route_config).await
    }

    fn build_server_state(&self, mock_service: Option<MockMemberService>) -> ServerState {
        ServerState {
            service: Box::new(
                mock_service.unwrap_or_else(MockMemberService::with_default_expectations),
            ),
        }
    }

    pub fn transaction(id: Option<i32>) -> Transaction {
        Transaction {
            transaction_id: id.unwrap_or_default().to_string(),
            operation: TransactionType::Create,
            signer_address: Self::default_from(),
            amount_in_minor_unit: 0,
            destination_address: Self::default_to(),
            correlation_id: "correlation_id".to_string(),
            bank_account: None,
            status: Status {
                state: TransactionState::Unknown,
                details: None,
            },
            datetime: Utc::now(),
            confirmation_id: "confirming it!".to_string(),
            cancellation_id: "cancelling it >:}".to_string(),
        }
    }

    pub fn default_receipt() -> Receipt {
        Receipt {
            correlation_id: Some("correlation_id".to_string()),
            transaction_id: "transaction_id".to_string(),
        }
    }

    pub fn default_address() -> String {
        Self::default_from()
    }

    pub fn default_from() -> String {
        "5Gph9iMER7s492Rm7i51oJkCr5snibMVWV6oqfEXEuxhv5xM".to_string()
    }

    pub fn default_to() -> String {
        "5EgqcJ1sbFkUvySAmxc3bThRePvdmw3zCrXMjqYPaSSSzbkQ".to_string()
    }

    pub fn default_mcb() -> Bank {
        Bank {
            id: None,
            name: "Bank of Foo".to_string(),
            routing_number: "123456789".to_string(),
            claims_account: "963852741".to_string(),
            adapter: Self::default_bank_adapter(),
        }
    }

    pub fn default_treasury_prime() -> Bank {
        Bank {
            id: None,
            name: "Bar Bank Financial".to_string(),
            routing_number: "999999999".to_string(),
            claims_account: "111111111".to_string(),
            adapter: BankAdapter::TreasuryPrime(TreasuryPrimeAdapterConfig {
                url: "http://www.bing.com".parse().unwrap(),
                secret_api_key_id: "some-secret-bank-key".to_string(),
                secret_api_key_value: "some-secret-bank-value".to_string(),
            }),
        }
    }

    pub fn default_bank_adapter() -> BankAdapter {
        BankAdapter::Mcb(McbAdapterConfig {
            url: "http://www.google.com".parse().unwrap(),
            secret_user_name: "shhhhh".to_string(),
            secret_password: "password-protected".to_string(),
            secret_client_app_ident: "12345".to_string(),
            secret_organization_id: "foobank".to_string(),
        })
    }

    pub fn default_banks() -> Vec<Bank> {
        vec![
            TestContext::default_mcb(),
            TestContext::default_treasury_prime(),
        ]
    }

    pub fn account(id: i32) -> AccountDetails {
        AccountDetails {
            id,
            bank_id: 0,
            bank_name: "Bank of Foo".to_string(),
            short_name: "Account".to_string(),
            account_number: "123123123".to_string(),
            routing_number: "456456456".to_string(),
        }
    }

    pub fn default_account() -> AccountDetails {
        Self::account(0)
    }

    pub fn default_accounts() -> Vec<AccountDetails> {
        vec![Self::account(0), Self::account(1)]
    }
}

// Convenience so my dumb tests don't trip out over "no matching expectation"
// just set all expectations to Ok<T>
impl MockMemberService {
    fn with_default_expectations() -> Self {
        let mut service = MockMemberService::new();
        service.expect_banks().return_const(Ok(vec![]));
        service
    }
}
