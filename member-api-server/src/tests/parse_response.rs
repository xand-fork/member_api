use actix_web::web::Bytes;
use async_trait::async_trait;
use serde::de::DeserializeOwned;

#[async_trait(?Send)]
pub trait ParseResponse: Sized {
    /// Extracts a response body of type T from `input`
    /// Example:
    ///
    /// let result: AddressResponse = resp.parse().await.unwrap();
    async fn parse<T: DeserializeOwned>(self) -> Result<T, Box<dyn std::error::Error>> {
        let bytes = self.into_bytes().await;
        Ok(serde_json::from_slice(&bytes)?)
    }

    async fn into_string(self) -> Result<String, Box<dyn std::error::Error>> {
        let bytes = self.into_bytes().await;
        Ok(std::str::from_utf8(&bytes)?.to_string())
    }

    async fn into_bytes(self) -> Bytes;
}
