use crate::tests::ParseResponse;
use actix_http::Request;
use actix_service::Service;
use actix_web::{body::Body, dev::ServiceResponse, Error};

pub struct Response {
    actix_response: ServiceResponse<Body>,
}

impl Response {
    pub fn status(&self) -> http::StatusCode {
        self.actix_response.status()
    }
}

#[async_trait::async_trait(?Send)]
impl ParseResponse for Response {
    async fn into_bytes(self) -> actix_web::web::Bytes {
        actix_web::test::read_body(self.actix_response).await
    }
}

impl From<ServiceResponse<Body>> for Response {
    fn from(actix_response: ServiceResponse<Body>) -> Self {
        Response { actix_response }
    }
}

#[async_trait::async_trait(?Send)]
pub trait TestWebServiceClient {
    async fn get(&self, path: &str) -> Response;
    async fn get_with_headers<M, K, V>(&self, path: &str, headers: M) -> Response
    where
        M: IntoIterator<Item = (K, V)>,
        K: AsRef<str>,
        V: AsRef<str>;

    async fn put_json<T: serde::Serialize>(&self, path: &str, data: &T) -> Response;
    async fn post_json<T: serde::Serialize>(&self, path: &str, data: &T) -> Response;
    async fn delete(&self, path: &str) -> Response;
}

#[async_trait::async_trait(?Send)]
impl<S> TestWebServiceClient for S
where
    S: Service<Request, Response = ServiceResponse<Body>, Error = Error>,
{
    async fn get(&self, path: &str) -> Response {
        let req = actix_web::test::TestRequest::get().uri(path).to_request();
        let resp = actix_web::test::call_service(self, req).await;

        resp.into()
    }

    async fn get_with_headers<M, K, V>(&self, path: &str, headers: M) -> Response
    where
        M: IntoIterator<Item = (K, V)>,
        K: AsRef<str>,
        V: AsRef<str>,
    {
        let req = headers
            .into_iter()
            .fold(
                actix_web::test::TestRequest::get().uri(path),
                |req, (key, value)| req.insert_header((key.as_ref(), value.as_ref())),
            )
            .to_request();
        let resp = actix_web::test::call_service(self, req).await;

        resp.into()
    }

    async fn put_json<T: serde::Serialize>(&self, path: &str, data: &T) -> Response {
        let req = actix_web::test::TestRequest::put()
            .uri(path)
            .set_json(data)
            .to_request();
        let resp = actix_web::test::call_service(self, req).await;

        resp.into()
    }

    async fn post_json<T: serde::Serialize>(&self, path: &str, data: &T) -> Response {
        let req = actix_web::test::TestRequest::post()
            .uri(path)
            .set_json(data)
            .to_request();
        let resp = actix_web::test::call_service(self, req).await;

        resp.into()
    }

    async fn delete(&self, path: &str) -> Response {
        let req = actix_web::test::TestRequest::delete()
            .uri(path)
            .to_request();
        let resp = actix_web::test::call_service(self, req).await;

        resp.into()
    }
}
