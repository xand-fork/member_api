use actix_web::{
    web::{scope, Data, Json, Path, Query},
    HttpResponse, ResponseError, Scope,
};

use crate::web_server::ServerState;

pub fn routes() -> Scope {
    scope("/api/v1/transactions")
        .service(get_transactions)
        .service(get_transaction_by_id)
        .service(post_claim_create)
        .service(post_claim_send)
        .service(post_claim_redeem)
        .service(post_claim_fund)
}

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct PagingSpec {
    pub page_size: Option<usize>,
    pub page_number: Option<usize>,
}

#[actix_web::get("")]
async fn get_transactions(state: Data<ServerState>, query: Query<PagingSpec>) -> HttpResponse {
    let result = state
        .service
        .transactions(query.page_number, query.page_size)
        .await
        .map_err(crate::Error::from);

    match result {
        Ok(value) => HttpResponse::Ok().json(value),
        Err(e) => e.error_response(),
    }
}

#[actix_web::get("/{id}")]
pub async fn get_transaction_by_id(state: Data<ServerState>, id: Path<String>) -> HttpResponse {
    let id = id.into_inner();
    let result = state
        .service
        .transaction(&id)
        .await
        .map_err(crate::Error::from);

    match result {
        Err(e) => e.error_response(),
        Ok(value) => HttpResponse::Ok().json(&value),
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Payment {
    pub to_address: String,
    pub amount_in_minor_unit: u64,
}

#[actix_web::post("/claims/send")]
async fn post_claim_send(state: Data<ServerState>, body: Json<Payment>) -> HttpResponse {
    let request = body.into_inner();
    let to = request
        .to_address
        .to_string()
        .parse()
        .map_err(|e| crate::Error::InvalidAddress { source: e });
    if let Err(e) = to {
        return e.error_response();
    }

    let result = state
        .service
        .submit_payment(to.unwrap(), request.amount_in_minor_unit)
        .await
        .map_err(crate::Error::from);

    match result {
        Ok(value) => HttpResponse::Ok().json(value),
        Err(e) => e.error_response(),
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct CreateRequest {
    pub account_id: i32,
    pub amount_in_minor_unit: u64,
}

#[actix_web::post("/claims/create")]
async fn post_claim_create(state: Data<ServerState>, body: Json<CreateRequest>) -> HttpResponse {
    let result = state
        .service
        .issue_create_request(body.account_id, body.amount_in_minor_unit)
        .await
        .map_err(crate::Error::from);

    match result {
        Ok(value) => HttpResponse::Ok().json(value),
        Err(e) => e.error_response(),
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct RedeemRequest {
    pub account_id: i32,
    pub amount_in_minor_unit: u64,
}

#[actix_web::post("/claims/redeem")]
async fn post_claim_redeem(state: Data<ServerState>, body: Json<RedeemRequest>) -> HttpResponse {
    let result = state
        .service
        .redeem(body.account_id, body.amount_in_minor_unit)
        .await
        .map_err(crate::Error::from);

    match result {
        Ok(value) => HttpResponse::Ok().json(value),
        Err(e) => e.error_response(),
    }
}

#[derive(Clone, Debug, Deserialize, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct FundClaims {
    pub amount_in_minor_unit: u64,
    pub correlation_id: String,
    pub bank_account_id: u64,
}

// TODO - update logic to use transaction ID to double-check or eliminate need for correlation id
// TODO - OR add claim correlation id as the ID instead of transaction ID
#[actix_web::post("claims/{id}/fund")]
async fn post_claim_fund(
    state: Data<ServerState>,
    _id: Path<String>,
    body: Json<FundClaims>,
) -> HttpResponse {
    let account_id = body.bank_account_id as i32; // TODO cast

    let response = state
        .service
        .fund_claims(account_id, &body.correlation_id, body.amount_in_minor_unit)
        .await
        .map_err(crate::Error::from);

    match response {
        Ok(results) => HttpResponse::Ok().json(results),
        Err(e) => e.error_response(),
    }
}

#[cfg(test)]
mod tests {
    use crate::{
        service::MockMemberService,
        tests::{
            test_context::{web_service::client::TestWebServiceClient, TestContext},
            ParseResponse,
        },
        transaction::{CreateRequest, FundClaims, Payment, RedeemRequest},
    };
    use http::StatusCode;
    use member_api::{banks::api::FundClaimsResponse, Receipt, Transaction, TransactionHistory};

    const DEFAULT_ID: i32 = 0;

    #[actix_rt::test]
    async fn route_transactions_default() {
        // Given
        let mut service = MockMemberService::new();
        service.expect_transactions().returning(|_, _| {
            Box::pin(async {
                Ok(TransactionHistory {
                    total: 13,
                    transactions: vec![],
                })
            })
        });

        let context = TestContext::default();
        let app = context.start_web_server_with_service(service).await;

        // When
        let resp = app.get("/api/v1/transactions").await;

        // Then
        assert_eq!(StatusCode::OK, resp.status());

        let result: TransactionHistory = resp.parse().await.unwrap();
        assert_eq!(13, result.total, "total");
        assert_eq!(Vec::<Transaction>::new(), result.transactions);
    }

    #[actix_rt::test]
    async fn route_transaction_by_id_found() {
        // Given
        let mut service = MockMemberService::new();
        service
            .expect_transaction()
            .returning(|_| Box::pin(async { Ok(TestContext::transaction(Some(DEFAULT_ID))) }));

        let context = TestContext::default();
        let app = context.start_web_server_with_service(service).await;

        // When
        let uri = format!("/api/v1/transactions/{}", DEFAULT_ID);

        let resp = app.get(&uri).await;

        // Then
        assert_eq!(StatusCode::OK, resp.status());

        let result: Transaction = resp.parse().await.unwrap();
        assert_eq!(result.transaction_id, DEFAULT_ID.to_string());
    }

    #[actix_rt::test]
    async fn route_transaction_by_id_not_found() {
        // Given
        // Given
        let mut service = MockMemberService::new();
        service.expect_transaction().returning(|_| {
            Box::pin(async {
                Err(member_api::Error::NotFound {
                    message: "Transaction not found!".to_string(),
                })
                .map_err(Into::into)
            })
        });

        let context = TestContext::default();
        let app = context.start_web_server_with_service(service).await;

        // When
        let uri = format!("/api/v1/transactions/{}", DEFAULT_ID);

        let resp = app.get(&uri).await;

        // Then
        assert_eq!(
            StatusCode::NOT_FOUND,
            resp.status(),
            "status: {}",
            resp.status()
        );
    }

    #[actix_rt::test]
    async fn route_create_requests() {
        // Given
        let mut service = MockMemberService::new();
        service
            .expect_issue_create_request()
            .returning(|_, _| Box::pin(async move { Ok(TestContext::default_receipt()) }));
        let context = TestContext::default();
        let app = context.start_web_server_with_service(service).await;

        let request_body = CreateRequest {
            account_id: 0,
            amount_in_minor_unit: 42,
        };

        // When
        let resp = app
            .post_json("/api/v1/transactions/claims/create", &request_body)
            .await;

        // Then
        assert_eq!(
            StatusCode::OK,
            resp.status(),
            "status: {}, body: {:?}",
            resp.status(),
            resp.into_string().await.unwrap()
        );

        let body: Receipt = resp.parse().await.unwrap();
        assert_eq!(body, TestContext::default_receipt());
    }

    #[actix_rt::test]
    async fn route_send() {
        // Given
        let mut service = MockMemberService::new();
        service
            .expect_submit_payment()
            .returning(|_, _| Box::pin(async move { Ok(TestContext::default_receipt()) }));
        let context = TestContext::default();
        let app = context.start_web_server_with_service(service).await;

        let to_address = TestContext::default_to();
        let request_body = Payment {
            to_address,
            amount_in_minor_unit: 42,
        };

        // When
        let resp = app
            .post_json("/api/v1/transactions/claims/send", &request_body)
            .await;

        // Then
        assert_eq!(
            StatusCode::OK,
            resp.status(),
            "status: {}, body: {:?}",
            resp.status(),
            resp.into_string().await.unwrap()
        );

        let body: Receipt = resp.parse().await.unwrap();
        assert_eq!(body, TestContext::default_receipt());
    }

    #[actix_rt::test]
    async fn route_redeem() {
        // Given
        let mut service = MockMemberService::new();
        service
            .expect_redeem()
            .returning(|_, _| Box::pin(async move { Ok(TestContext::default_receipt()) }));
        let context = TestContext::default();
        let app = context.start_web_server_with_service(service).await;

        let request_body = RedeemRequest {
            account_id: 0,
            amount_in_minor_unit: 42,
        };

        // When
        let resp = app
            .post_json("/api/v1/transactions/claims/redeem", &request_body)
            .await;

        // Then
        assert_eq!(
            StatusCode::OK,
            resp.status(),
            "status: {}, body: {:?}",
            resp.status(),
            resp.into_string().await.unwrap()
        );

        let body: Receipt = resp.parse().await.unwrap();
        assert_eq!(body, TestContext::default_receipt());
    }

    #[actix_rt::test]
    async fn route_transactions_claims_fund() {
        // Given
        let mut service = MockMemberService::new();
        service.expect_fund_claims().returning(|_, _, _| {
            Box::pin(async {
                Ok(FundClaimsResponse {
                    amount_in_minor_unit: 42,
                })
            })
        });
        let context = TestContext::default();
        let app = context.start_web_server_with_service(service).await;
        let tx_id = 1; // TODO - update test to create a real create claim transaction or update to remove and change route to correlation Id

        let body = FundClaims {
            amount_in_minor_unit: 42,
            correlation_id: "correlation_id".to_string(),
            bank_account_id: 0,
        };

        let uri = format!("/api/v1/transactions/claims/{}/fund", tx_id);

        // When
        let resp = app.post_json(&uri, &body).await;

        // Then
        assert_eq!(
            StatusCode::OK,
            resp.status(),
            "{:?}",
            resp.into_string().await
        );

        let response: FundClaimsResponse = resp.parse().await.unwrap();
        assert_eq!(response.amount_in_minor_unit, body.amount_in_minor_unit);
    }

    #[actix_rt::test]
    async fn route_transactions_claims_fund_not_found() {
        // Given
        let mut service = MockMemberService::new();
        service.expect_fund_claims().returning(|_, _, _| {
            Box::pin(async {
                Err(member_api::Error::NotFound {
                    message: "Transaction not found".to_string(),
                })
            })
        });
        let context = TestContext::default();
        let app = context.start_web_server_with_service(service).await;
        let tx_id = 1; // TODO - update test to create a real create claim transaction or update to remove and change route to correlation Id

        let body = FundClaims {
            amount_in_minor_unit: 42,
            correlation_id: "correlation_id".to_string(),
            bank_account_id: 0,
        };

        let uri = format!("/api/v1/transactions/claims/{}/fund", tx_id);

        // When
        let resp = app.post_json(&uri, &body).await;

        // Then
        assert_eq!(
            StatusCode::NOT_FOUND,
            resp.status(),
            "{:?}",
            resp.into_string().await
        );
    }
}
