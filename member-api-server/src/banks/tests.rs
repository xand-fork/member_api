use crate::{
    banks::{assert_adapters_eq, ApiBank, ApiBankAdapter, NewBank},
    service::MockMemberService,
    tests::{
        test_context::{web_service::client::TestWebServiceClient, TestContext},
        ParseResponse,
    },
};
use http::StatusCode;
use member_api::banks::{
    repository::errors::{BanksRepositoryError, BanksRepositoryErrorSource},
    Bank,
};
use mockall::predicate::eq;

const DEFAULT_ID: i32 = 0;

#[actix_rt::test]
async fn route_get_banks_happy_path() {
    // Given
    let mut service = MockMemberService::new();
    service
        .expect_banks()
        .with()
        .return_const(Ok(TestContext::default_banks()));
    let context = TestContext::default();
    let app = context.start_web_server_with_service(service).await;

    // When
    let resp = app.get("/api/v1/banks").await;

    // Then
    assert_eq!(StatusCode::OK, resp.status());

    let body: Vec<ApiBank> = resp.parse().await.unwrap();
    assert_eq!(body.len(), TestContext::default_banks().len());
}

#[actix_rt::test]
async fn route_get_bank_happy_path() {
    // Given
    let expected = TestContext::default_mcb();
    let mut service = MockMemberService::new();
    service
        .expect_bank()
        .with(eq(DEFAULT_ID))
        .return_const(Ok(TestContext::default_mcb()));
    let context = TestContext::default();
    let app = context.start_web_server_with_service(service).await;

    let uri = format!("/api/v1/banks/{}", DEFAULT_ID);

    // When
    let resp = app.get(&uri).await;

    // Then
    assert_eq!(StatusCode::OK, resp.status());

    let actual: ApiBank = resp.parse().await.unwrap();
    assert_eq!(DEFAULT_ID, actual.id);
    assert_eq!(expected.routing_number, actual.routing_number);
    assert_eq!(expected.name, actual.name);
    assert_eq!(expected.claims_account, actual.claims_account);
    assert_adapters_eq(TestContext::default_mcb().adapter, actual.adapter);
}

#[actix_rt::test]
async fn get_nonexistent_bank_returns_not_found() {
    // Given
    let mut service = MockMemberService::new();
    service
        .expect_bank()
        .return_const(Err(member_api::Error::BankRepositoryErrors {
            source: BanksRepositoryError::NotFound,
        }));
    let context = TestContext::default();
    let app = context.start_web_server_with_service(service).await;

    let uri = format!("/api/v1/banks/{}", DEFAULT_ID);

    // When
    let resp = app.get(&uri).await;

    // Then
    assert_eq!(StatusCode::NOT_FOUND, resp.status());
}

#[actix_rt::test]
async fn route_post_happy_path() {
    // Given
    let body = NewBank {
        name: "fredbob".to_string(),
        routing_number: "123456789012".to_string(),
        claims_account: "12345678".to_string(),
        adapter: ApiBankAdapter::from(TestContext::default_bank_adapter()),
    };

    let bank: Bank = body.clone().into();

    let mut service = MockMemberService::new();
    service
        .expect_add_bank()
        .with(eq(bank.clone()))
        .return_const(Ok(bank.clone()));
    let context = TestContext::default();
    let app = context.start_web_server_with_service(service).await;

    // When
    let resp = app.post_json("/api/v1/banks", &body).await;

    // Then
    assert_eq!(StatusCode::CREATED, resp.status());
}

#[actix_rt::test]
async fn route_post_client_err() {
    // Given
    let body = NewBank {
        name: "fredbob".to_string(),
        routing_number: "123456789012".to_string(),
        claims_account: "12345678".to_string(),
        adapter: ApiBankAdapter::from(TestContext::default_bank_adapter()),
    };

    let bank: Bank = body.clone().into();

    let mut service = MockMemberService::new();
    service
        .expect_add_bank()
        .with(eq(bank.clone()))
        .return_const(Err(member_api::Error::BankRepositoryErrors {
            source: BanksRepositoryError::Validation {
                msg: "New bank failed validation".to_string(),
                source: BanksRepositoryErrorSource::BanksRepository,
            },
        }));

    let context = TestContext::default();
    let app = context.start_web_server_with_service(service).await;

    // When
    let resp = app.post_json("/api/v1/banks", &body).await;

    // Then
    assert_eq!(StatusCode::BAD_REQUEST, resp.status());
}

#[actix_rt::test]
async fn route_post_mcb_bank() {
    // Given
    let mut service = MockMemberService::new();
    service
        .expect_add_bank()
        .with(eq(TestContext::default_mcb()))
        .return_const(Ok(TestContext::default_mcb()));
    let context = TestContext::default();
    let app = context.start_web_server_with_service(service).await;

    let body: NewBank = TestContext::default_mcb().into();

    // When
    let resp = app.post_json("/api/v1/banks", &body).await;

    // Then
    assert_eq!(StatusCode::CREATED, resp.status());

    let body: ApiBank = resp.parse().await.unwrap();
    assert_eq!(body.name, TestContext::default_mcb().name);
    assert_eq!(
        body.routing_number,
        TestContext::default_mcb().routing_number
    );
    assert_eq!(
        body.claims_account,
        TestContext::default_mcb().claims_account
    );
    assert!(body.adapter.is_mcb());

    assert_adapters_eq(TestContext::default_mcb().adapter, body.adapter);
}

#[actix_rt::test]
async fn route_post_treasury_prime_bank() {
    // Given
    let mut service = MockMemberService::new();
    service
        .expect_add_bank()
        .with(eq(TestContext::default_treasury_prime()))
        .return_const(Ok(TestContext::default_treasury_prime()));
    let context = TestContext::default();
    let app = context.start_web_server_with_service(service).await;

    let body: NewBank = TestContext::default_treasury_prime().into();

    // When
    let resp = app.post_json("/api/v1/banks", &body).await;

    // Then
    assert_eq!(StatusCode::CREATED, resp.status());

    let body: ApiBank = resp.parse().await.unwrap();
    assert_eq!(body.id, DEFAULT_ID);
    assert_eq!(body.name, TestContext::default_treasury_prime().name);
    assert_eq!(
        body.routing_number,
        TestContext::default_treasury_prime().routing_number
    );
    assert_eq!(
        body.claims_account,
        TestContext::default_treasury_prime().claims_account
    );
    assert!(body.adapter.is_treasury_prime());

    assert_adapters_eq(TestContext::default_treasury_prime().adapter, body.adapter);
}

#[actix_rt::test]
async fn route_put_happy_path_new_bank() {
    // Given
    let body = NewBank {
        name: "fredbob".to_string(),
        routing_number: "123456789012".to_string(),
        claims_account: "12345678".to_string(),
        adapter: ApiBankAdapter::from(TestContext::default_bank_adapter()),
    };

    let bank = Bank {
        id: Some(DEFAULT_ID),
        name: body.name.clone(),
        routing_number: body.routing_number.clone(),
        claims_account: body.claims_account.clone(),
        adapter: body.adapter.clone().into(),
    };

    let mut service = MockMemberService::new();

    service
        .expect_bank()
        .return_const(Err(member_api::Error::BankRepositoryErrors {
            source: BanksRepositoryError::NotFound,
        }));

    service.expect_update_bank().with(eq(bank)).returning(Ok);

    let context = TestContext::default();
    let app = context.start_web_server_with_service(service).await;

    // When
    let uri = format!("/api/v1/banks/{}", DEFAULT_ID);

    let resp = app.put_json(&uri, &body).await;

    // Then
    assert_eq!(StatusCode::CREATED, resp.status());
}

#[actix_rt::test]
async fn route_put_happy_path_existing_bank() {
    // Given
    let body = NewBank {
        name: "fredbob".to_string(),
        routing_number: "123456789012".to_string(),
        claims_account: "12345678".to_string(),
        adapter: ApiBankAdapter::from(TestContext::default_bank_adapter()),
    };

    let bank = Bank {
        id: Some(DEFAULT_ID),
        name: body.name.clone(),
        routing_number: body.routing_number.clone(),
        claims_account: body.claims_account.clone(),
        adapter: body.adapter.clone().into(),
    };

    let mut service = MockMemberService::new();

    service
        .expect_bank()
        .with(eq(DEFAULT_ID))
        .return_const(Ok(TestContext::default_mcb()));
    service.expect_update_bank().with(eq(bank)).returning(Ok);

    let context = TestContext::default();
    let app = context.start_web_server_with_service(service).await;

    // When
    let uri = format!("/api/v1/banks/{}", DEFAULT_ID);

    let resp = app.put_json(&uri, &body).await;

    // Then
    assert_eq!(StatusCode::OK, resp.status());
}

#[actix_rt::test]
async fn route_put_client_err() {
    // Given
    let body = NewBank {
        name: "fredbob".to_string(),
        routing_number: "123456789012".to_string(),
        claims_account: "12345678".to_string(),
        adapter: ApiBankAdapter::from(TestContext::default_bank_adapter()),
    };

    let bank = Bank {
        id: Some(DEFAULT_ID),
        name: body.name.clone(),
        routing_number: body.routing_number.clone(),
        claims_account: body.claims_account.clone(),
        adapter: body.adapter.clone().into(),
    };

    let mut service = MockMemberService::new();

    service
        .expect_bank()
        .with(eq(DEFAULT_ID))
        .return_const(Ok(TestContext::default_mcb()));
    service
        .expect_update_bank()
        .with(eq(bank))
        .return_const(Err(member_api::Error::BankRepositoryErrors {
            source: BanksRepositoryError::Validation {
                msg: "New bank failed validation".to_string(),
                source: BanksRepositoryErrorSource::BanksRepository,
            },
        }));

    let context = TestContext::default();
    let app = context.start_web_server_with_service(service).await;

    // When
    let uri = format!("/api/v1/banks/{}", DEFAULT_ID);

    let resp = app.put_json(&uri, &body).await;

    // Then
    assert_eq!(StatusCode::BAD_REQUEST, resp.status());
}

#[actix_rt::test]
async fn route_delete() {
    // Given
    let mut service = MockMemberService::new();
    service
        .expect_delete_bank()
        .with(eq(DEFAULT_ID))
        .return_const(Ok(()));

    let context = TestContext::default();
    let app = context.start_web_server_with_service(service).await;

    let uri = format!("/api/v1/banks/{}", DEFAULT_ID);

    // When
    let resp = app.delete(&uri).await;

    // Then
    assert_eq!(
        StatusCode::OK,
        resp.status(),
        "{:?}",
        resp.into_string().await
    );
}

#[actix_rt::test]
async fn route_delete_bank_with_accounts() {
    // Given
    let mut service = MockMemberService::new();
    service
        .expect_delete_bank()
        .with(eq(DEFAULT_ID))
        .return_const(Err(member_api::Error::BankRepositoryErrors {
            source: BanksRepositoryError::Validation {
                msg: "Banks cannot be removed if they still have accounts.".to_string(),
                source: BanksRepositoryErrorSource::BanksRepository,
            },
        }));

    let context = TestContext::default();
    let app = context.start_web_server_with_service(service).await;

    let uri = format!("/api/v1/banks/{}", DEFAULT_ID);

    // When
    let resp = app.delete(&uri).await;

    // Then
    assert_eq!(resp.status(), StatusCode::BAD_REQUEST);

    let response_text = resp.into_string().await.unwrap();
    assert_eq!(
        response_text,
        "{\"message\":\"Banks cannot be removed if they still have accounts.\"}".to_string()
    );
}
