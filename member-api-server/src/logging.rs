use crate::error;

#[logging_event]
pub enum HttpEvents {
    Error { url: String, error: String },
}

#[logging_event]
pub enum LoggingEvent {
    Error(error::Error),
    BanksRepositoryError(String),
    AccountsRepositoryError(String),
    Informational(String),
    Fatal(String),
}
