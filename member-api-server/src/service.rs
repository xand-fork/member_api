use member_api::{
    accounts::{Account, AccountDetails},
    banks::{api::FundClaimsResponse, Bank, BankAccountBalance},
    health::Service,
    MemberApiService, Receipt, Transaction, TransactionHistory,
};
use xand_address::Address;

#[async_trait::async_trait]
#[cfg_attr(test, mockall::automock)]
pub trait MemberService: Send + Sync {
    async fn bank_balance(&self, _id: i32) -> member_api::Result<BankAccountBalance>;
    fn services(&self) -> Vec<Box<dyn Service>>;
    async fn xand_balance(&self) -> member_api::Result<u64>;
    async fn transactions(
        &self,
        page_number: Option<usize>,
        page_size: Option<usize>,
    ) -> member_api::Result<TransactionHistory>;
    async fn transaction(&self, id: &str) -> member_api::Result<Transaction>;
    async fn submit_payment(
        &self,
        receiver: Address,
        amount_in_minor_units: u64,
    ) -> member_api::Result<Receipt>;
    async fn issue_create_request(
        &self,
        id: i32,
        amount_in_minor_units: u64,
    ) -> member_api::Result<Receipt>;

    async fn fund_claims(
        &self,
        _id: i32,
        _correlation_id: &str,
        _amount_in_minor_units: u64,
    ) -> member_api::Result<FundClaimsResponse>;

    fn delete_bank(&self, id: i32) -> member_api::Result<()>;

    fn address(&self) -> &Address;

    fn add_account(&self, account: Account) -> member_api::Result<AccountDetails>;

    fn accounts(&self) -> member_api::Result<Vec<AccountDetails>>;

    fn account(&self, id: i32) -> member_api::Result<AccountDetails>;

    fn delete_account(&self, id: i32) -> member_api::Result<()>;

    fn update_account(&self, account: Account) -> member_api::Result<AccountDetails>;

    fn banks(&self) -> member_api::Result<Vec<Bank>>;

    fn bank(&self, id: i32) -> member_api::Result<Bank>;

    fn add_bank(&self, bank: Bank) -> member_api::Result<Bank>;

    fn update_bank(&self, bank: Bank) -> member_api::Result<Bank>;

    async fn redeem(&self, id: i32, amount_in_minor_units: u64) -> member_api::Result<Receipt>;
}

#[async_trait::async_trait]
impl MemberService for MemberApiService {
    async fn bank_balance(&self, id: i32) -> member_api::Result<BankAccountBalance> {
        self.bank_balance(id).await
    }

    fn services(&self) -> Vec<Box<dyn Service>> {
        self.services()
    }

    async fn xand_balance(&self) -> member_api::Result<u64> {
        self.xand_balance().await
    }

    async fn transactions(
        &self,
        page_number: Option<usize>,
        page_size: Option<usize>,
    ) -> member_api::Result<TransactionHistory> {
        self.transactions(page_number, page_size).await
    }

    async fn transaction(&self, id: &str) -> member_api::Result<Transaction> {
        self.transaction(id).await
    }

    async fn submit_payment(
        &self,
        receiver: Address,
        amount_in_minor_units: u64,
    ) -> member_api::Result<Receipt> {
        self.submit_payment(receiver, amount_in_minor_units).await
    }

    async fn issue_create_request(
        &self,
        id: i32,
        amount_in_minor_units: u64,
    ) -> member_api::Result<Receipt> {
        self.issue_create_request(id, amount_in_minor_units).await
    }

    async fn fund_claims(
        &self,
        id: i32,
        correlation_id: &str,
        amount_in_minor_units: u64,
    ) -> member_api::Result<FundClaimsResponse> {
        self.fund_claims(id, correlation_id, amount_in_minor_units)
            .await
    }

    fn delete_bank(&self, id: i32) -> member_api::Result<()> {
        self.delete_bank(id)
    }

    fn address(&self) -> &Address {
        self.address()
    }

    fn add_account(&self, account: Account) -> member_api::Result<AccountDetails> {
        self.add_account(account)
    }

    fn accounts(&self) -> member_api::Result<Vec<AccountDetails>> {
        self.accounts()
    }

    fn account(&self, id: i32) -> member_api::Result<AccountDetails> {
        self.account_by_id(id)
    }

    fn delete_account(&self, id: i32) -> member_api::Result<()> {
        self.delete_account(id)
    }

    fn update_account(&self, account: Account) -> member_api::Result<AccountDetails> {
        self.update_account(account)
    }

    fn banks(&self) -> member_api::Result<Vec<Bank>> {
        self.banks()
    }

    fn bank(&self, id: i32) -> member_api::Result<Bank> {
        self.bank(id)
    }

    fn add_bank(&self, bank: Bank) -> member_api::Result<Bank> {
        self.add_bank(bank)
    }

    fn update_bank(&self, bank: Bank) -> member_api::Result<Bank> {
        self.update_bank(bank)
    }

    async fn redeem(&self, id: i32, amount_in_minor_units: u64) -> member_api::Result<Receipt> {
        self.redeem(id, amount_in_minor_units).await
    }
}
