use crate::{auth::jwt::Claims, cli::JwtOptions, Error, Result};

/// Reads user input from stdin for generating a JWT
pub(crate) fn generate_jwt_token_cli(options: JwtOptions) -> Result<()> {
    let secret = rpassword::read_password_from_tty(Some("JWT Secret :"))?;
    let secret2 = rpassword::read_password_from_tty(Some("\nConfirm    :"))?;
    if secret != secret2 {
        return Err(Error::Validation {
            message: "Secrets do not match. Try again.".into(),
        });
    }

    let jwt = generate_jwt_token(options, secret);

    println!("\n{}", jwt);
    Ok(())
}

/// Returns a JsonWebToken clients can use to authorize against the given secret String
pub fn generate_jwt_token(options: JwtOptions, secret: String) -> String {
    let claims = Claims {
        sub: options.company.clone(),
        company: options.company,
        exp: options.expires.unwrap_or(usize::max_value()),
    };

    crate::auth::jwt::generate_jwt_token(secret, claims)
}
