use std::sync::Arc;

#[derive(Clone, Debug, Serialize, thiserror::Error)]
pub enum ActixError {
    #[error("Actix mailbox: {}", source)]
    MailBox {
        #[from(actix::MailboxError, Arc::new)]
        #[serde(serialize_with = "xand_utils::snafu_extensions::debug_serialize")]
        source: Arc<actix::MailboxError>,
    },
    #[error("Generating URL: {}", message)]
    UrlGeneration {
        // Couldn't fit actix_web::error::UrlGenerationError into the source
        message: String,
    },
}

#[cfg(test)]
mod test {
    use crate::{error::web::ActixError, Error};
    use actix_web::ResponseError;
    use http::StatusCode;
    use std::sync::Arc;

    #[test]
    fn mailbox_error() {
        let source = Arc::new(actix::MailboxError::Closed);

        let error: Error = ActixError::MailBox { source }.into();

        let response = error.error_response();

        assert_eq!(StatusCode::INTERNAL_SERVER_ERROR, response.status());
    }

    #[test]
    fn url_generation() {
        let source: Error = ActixError::UrlGeneration {
            message: "Error Test".to_string(),
        }
        .into();

        let response = source.error_response();

        assert_eq!(StatusCode::INTERNAL_SERVER_ERROR, response.status());
    }
}
