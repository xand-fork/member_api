pub(crate) mod handler;

use structopt::StructOpt;

#[derive(Debug, StructOpt)]
#[structopt(name = "member-api", about = "The Xand Member API.")]
pub struct Cli {
    #[structopt(subcommand, name = "jwt")]
    pub generate_jwt: Option<Command>,
}

#[derive(Debug, StructOpt)]
pub enum Command {
    #[structopt(help("Generate a jwt token from the given secret and emit it to stdout"))]
    Jwt(JwtOptions),
}

#[derive(Debug, StructOpt)]
pub struct JwtOptions {
    #[structopt(short, long, help("Your company or organization name."))]
    pub company: String,

    #[structopt(
        short,
        long,
        help("Expiration time in seconds since epoch. Leave blank to never expire.")
    )]
    pub expires: Option<usize>,
}
