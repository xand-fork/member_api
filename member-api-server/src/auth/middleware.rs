use std::task::{Context, Poll};

use actix_service::{Service, Transform};
use actix_web::{
    dev::{ServiceRequest, ServiceResponse},
    Error, HttpResponse,
};
use futures::future::{ok, Either, Ready};

use crate::auth::RouteConfig;
use std::collections::HashMap;

impl<S> Transform<S, ServiceRequest> for RouteConfig
where
    S: Service<ServiceRequest, Response = ServiceResponse, Error = Error>,
{
    type Response = ServiceResponse;
    type Error = Error;
    type Transform = AuthMiddleware<S>;
    type InitError = ();
    type Future = Ready<Result<Self::Transform, Self::InitError>>;

    fn new_transform(&self, service: S) -> Self::Future {
        ok(AuthMiddleware {
            service,
            config: self.clone(),
        })
    }
}

pub struct AuthMiddleware<S> {
    service: S,
    config: RouteConfig,
}

impl<S> Service<ServiceRequest> for AuthMiddleware<S>
where
    S: Service<ServiceRequest, Response = ServiceResponse, Error = Error>,
{
    type Response = ServiceResponse;
    type Error = Error;

    #[allow(clippy::type_complexity)]
    type Future = Either<S::Future, Ready<Result<Self::Response, Self::Error>>>;

    fn poll_ready(&self, cx: &mut Context) -> Poll<Result<(), Self::Error>> {
        self.service.poll_ready(cx)
    }

    fn call(&self, req: ServiceRequest) -> Self::Future {
        let mut headers: HashMap<String, String> = HashMap::default();
        let request_headers = req.headers();

        for key in request_headers.keys() {
            let key_str = key.to_string();
            let header_value = request_headers.get(key).unwrap();
            // IMPORTANT: the header key name gets lowercases in the call to `to_str()`
            let value_str = header_value.to_str().unwrap().to_string();
            headers.insert(key_str, value_str);
        }

        let route = req.path();
        if self.config.is_authorized(route, &headers) {
            return Either::Left(self.service.call(req));
        }

        let response = HttpResponse::Unauthorized().finish();
        let unauthorized = ok(req.into_response(response));

        Either::Right(unauthorized)
    }
}

#[cfg(test)]
mod tests {

    use crate::{
        auth::jwt::Claims,
        tests::test_context::{web_service::client::TestWebServiceClient, TestContext},
    };
    use http::StatusCode;

    #[actix_rt::test]
    async fn jwt_enabled_request_with_valid_token_is_accepted() {
        // Given
        let secret = "some-secret-key".to_string();
        let context = TestContext {
            secret: Some(secret.clone()),
        };

        let app = context.start_web_server().await;

        let claims = Claims {
            sub: "tpfs".to_string(),
            company: "tpfs".to_string(),
            exp: usize::max_value(),
        };
        let token = crate::auth::jwt::generate_jwt_token(secret, claims);

        // When
        let resp = app
            .get_with_headers(
                "/api/v1/banks",
                [("Authorization", format!("Bearer {}", token))],
            )
            .await;

        // Then
        assert_ne!(resp.status(), StatusCode::UNAUTHORIZED);
        assert_eq!(resp.status(), StatusCode::OK);
    }

    #[actix_rt::test]
    async fn root_endpoint_should_be_anonymous() {
        // Given
        let context = TestContext {
            secret: Some("Blah blah blah secret key".to_string()),
        };

        let app = context.start_web_server().await;

        // When
        let resp = app.get("/").await;

        assert!(resp.status().is_success());
    }

    #[actix_rt::test]
    async fn health_endpoint_should_be_anonymous() {
        // Given
        let context = TestContext {
            secret: Some("Blah blah blah secret key".to_string()),
        };

        let app = context.start_web_server().await;

        // When
        let resp = app.get("/api/v1/health").await;

        assert_ne!(resp.status(), StatusCode::UNAUTHORIZED);
    }

    #[actix_rt::test]
    async fn jwt_enabled_request_without_token_is_unauthorized() {
        // Given
        let secret = "some-secret-key".to_string();
        let context = TestContext {
            secret: Some(secret.clone()),
        };

        let app = context.start_web_server().await;

        // When
        let resp = app.get("/api/v1/banks").await;

        assert!(resp.status().is_client_error());
        assert_eq!(resp.status(), StatusCode::UNAUTHORIZED);
    }

    #[actix_rt::test]
    async fn jwt_enabled_request_with_invalid_token_is_unauthorized() {
        // Given
        let secret = "some-secret-key".to_string();
        let context = TestContext {
            secret: Some(secret.clone()),
        };

        let app = context.start_web_server().await;

        // When
        let resp = app
            .get_with_headers("/api/v1/banks", [("Authorization", "Bearer Invalid token")])
            .await;

        // Then
        assert!(resp.status().is_client_error());
        assert_eq!(resp.status(), StatusCode::UNAUTHORIZED);
    }

    #[actix_rt::test]
    async fn non_jwt_request_without_token_is_authorized() {
        // Given
        let context = TestContext { secret: None };

        let app = context.start_web_server().await;

        // When
        let resp = app.get("/api/v1/banks").await;

        assert_ne!(resp.status(), StatusCode::UNAUTHORIZED);
        assert_eq!(resp.status(), StatusCode::OK);
    }
}
