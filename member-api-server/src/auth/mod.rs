pub mod jwt;
mod middleware;

use crate::auth::jwt::JwtAuthenticator;
use regex::Regex;
use std::collections::HashMap;

///
/// Routes are anonymous by default so there is no need to list them
/// in the config. However, there may be cases when a route
/// is anonymous but would otherwise match another route configuration.
/// In this case it is necessary to list them in config.
/// Routes are tested for matching in the order that they are represented
/// in config. First match wins.
///
#[derive(Debug, Clone, Serialize, Eq, PartialEq)]
pub struct RouteConfig {
    pub routes: Vec<Route>,
}

impl RouteConfig {
    /// Long term strategy should be to remove these helper methods
    /// in favor of a config-file based route configuration.
    pub fn noauth() -> RouteConfig {
        RouteConfig { routes: vec![] }
    }

    /// Long term strategy should be to remove these helper methods
    /// in favor of a config-file based route configuration.
    pub fn default_from_secret(secret: &str) -> RouteConfig {
        let health_route = Route {
            auth: AuthenticationMethod::Anonymous,
            matching_routes: vec!["/api/v1/health".to_string()],
        };

        let jwt_route = Route {
            auth: AuthenticationMethod::Jwt {
                secret: secret.to_string(),
            },
            matching_routes: vec!["/api/v1*".to_string()],
        };

        RouteConfig {
            routes: vec![health_route, jwt_route],
        }
    }

    pub fn is_authorized(&self, route: &str, headers: &HashMap<String, String>) -> bool {
        let matching_route = self.find_matching_route(route);
        matching_route.is_authorized(headers)
    }

    fn find_matching_route(&self, route: &str) -> Route {
        let result = self.routes.iter().find(|r| r.matches(route));
        if let Some(value) = result {
            value.clone()
        } else {
            Route {
                auth: AuthenticationMethod::Anonymous,
                matching_routes: vec![],
            }
        }
    }
}

#[derive(Debug, Clone, Serialize, Eq, PartialEq)]
pub struct Route {
    pub auth: AuthenticationMethod,
    pub matching_routes: Vec<String>,
}

impl Route {
    fn matches(&self, route: &str) -> bool {
        self.matching_routes.iter().any(|config| {
            let regex = Regex::new(config).unwrap();
            regex.is_match(route)
        })
    }

    pub fn is_authorized(&self, headers: &HashMap<String, String>) -> bool {
        match &self.auth {
            AuthenticationMethod::Anonymous => true,
            AuthenticationMethod::Jwt { secret } => {
                let authenticator = JwtAuthenticator {
                    secret: secret.to_string(),
                };
                authenticator.is_authorized(headers)
            }
        }
    }
}

#[derive(Debug, Clone, Serialize, Eq, PartialEq)]
pub enum AuthenticationMethod {
    Anonymous,
    Jwt { secret: String },
}

pub trait Authenticator {
    fn is_authorized(&self, _map: &HashMap<String, String>) -> bool;
}

#[logging_event]
pub enum AuthorizationEvents {
    AuthorizationFailed { error: String },
}

#[cfg(test)]
pub mod tests {
    use super::*;
    use crate::auth::jwt::Claims;

    fn generate_secret_and_token() -> (String, String) {
        let claims = Claims {
            sub: "tpfs".to_string(),
            company: "tpfs".to_string(),
            exp: usize::max_value(),
        };
        let secret = "some special secret".to_string();
        let token = crate::auth::jwt::generate_jwt_token(secret.clone(), claims);

        (secret, token)
    }

    fn jwt_header(token: &str) -> HashMap<String, String> {
        let mut headers = HashMap::default();
        headers.insert("authorization".to_string(), format!("Bearer {}", token));
        headers
    }

    fn jwt_route(route: &str, secret: String) -> RouteConfig {
        let jwt_route = Route {
            auth: AuthenticationMethod::Jwt { secret },
            matching_routes: vec![route.to_string()],
        };
        RouteConfig {
            routes: vec![jwt_route],
        }
    }

    #[test]
    pub fn routes_are_anonymous_by_default() {
        let config = RouteConfig { routes: vec![] };

        let headers = HashMap::default();

        let authorized = config.is_authorized("/", &headers);

        assert!(authorized);
    }

    #[test]
    pub fn jwt_route_no_token() {
        // Given
        let (secret, _token) = generate_secret_and_token();
        let headers = HashMap::default();
        let config = jwt_route("/some", secret);

        // When
        let authorized = config.is_authorized("/some", &headers);

        // Then
        assert!(!authorized);
    }

    #[test]
    pub fn jwt_route_valid_token() {
        // Given
        let (secret, token) = generate_secret_and_token();
        let headers = jwt_header(&token);
        let config = jwt_route("/some", secret);

        // When
        let authorized = config.is_authorized("/some", &headers);

        // Then
        assert!(authorized);
    }

    #[test]
    pub fn jwt_route_invalid_token() {
        // Given
        let (secret, _token) = generate_secret_and_token();
        let headers = jwt_header("this token should not work.");
        let config = jwt_route("/some", secret);

        // When
        let authorized = config.is_authorized("/some", &headers);

        // Then
        assert!(!authorized);
    }

    #[test]
    pub fn jwt_route_wrong_header_format() {
        // Given
        let (secret, token) = generate_secret_and_token();
        let mut headers = HashMap::default();
        headers.insert("Authorization".to_string(), format!("Woops {}", token));

        let config = jwt_route("/some", secret);

        // When
        let authorized = config.is_authorized("/some", &headers);

        // Then
        assert!(!authorized, "This route should not have been authorized.");
    }

    #[test]
    pub fn real_world_home_route_is_anonymous() {
        // Given
        let (secret, _token) = generate_secret_and_token();
        let headers: HashMap<String, String> = HashMap::default();
        let config = RouteConfig::default_from_secret(&secret);

        // When
        let authorized = config.is_authorized("/", &headers);

        // Then
        assert!(authorized);
    }

    #[test]
    pub fn real_world_health_endpoint_is_anonymous() {
        // Given
        let (secret, _token) = generate_secret_and_token();
        let headers: HashMap<String, String> = HashMap::default();
        let config = RouteConfig::default_from_secret(&secret);

        // When
        let authorized = config.is_authorized("/api/v1/health", &headers);

        // Then
        assert!(authorized);
    }

    #[test]
    pub fn real_world_bank_accounts_endpoint_is_secured() {
        // Given
        let (secret, _token) = generate_secret_and_token();
        let headers: HashMap<String, String> = HashMap::default();
        let config = RouteConfig::default_from_secret(&secret);

        // When
        let authorized = config.is_authorized("/api/v1/bank-accounts", &headers);

        // Then
        assert!(!authorized);
    }
}
