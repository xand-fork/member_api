use crate::auth::{Authenticator, AuthorizationEvents};

use jwt::{decode, Validation};
use regex::Regex;
use std::collections::HashMap;
use thiserror::Error;

use actix_web::{HttpResponse, ResponseError};
use std::sync::Arc;

use crate::auth::jwt::AuthError::Decoding;
use jsonwebtoken::*;

pub fn generate_jwt_token(secret: String, claims: Claims) -> String {
    let key = jsonwebtoken::EncodingKey::from_secret(&secret.into_bytes());
    jsonwebtoken::encode(&Header::default(), &claims, &key).unwrap()
}

/// Note: The names on this struct are required
/// by the `jsonwebtoken` library to be unchanged.
/// Boo.
#[derive(Clone, Debug, Serialize, Deserialize, Eq, PartialEq)]
pub struct Claims {
    pub sub: String,
    pub company: String,
    pub exp: usize,
}

pub struct JwtAuthenticator {
    pub secret: String,
}

impl Authenticator for JwtAuthenticator {
    fn is_authorized(&self, map: &HashMap<String, String>) -> bool {
        match self.verify_token(map) {
            Ok(_claims) => true,
            Err(e) => {
                warn!(AuthorizationEvents::AuthorizationFailed {
                    error: format!("{:?}", e)
                });
                false
            }
        }
    }
}

impl JwtAuthenticator {
    fn verify_token(
        &self,
        headers: &HashMap<String, String>,
    ) -> AuthResult<jwt::TokenData<Claims>> {
        let token = self.get_token(headers)?;
        self.decode_token(&token)
    }

    fn decode_token(&self, token: &str) -> AuthResult<jwt::TokenData<Claims>> {
        let secret = self.secret.as_ref();

        decode::<Claims>(
            token,
            &jwt::DecodingKey::from_secret(secret),
            &Validation::default(),
        )
        .map_err(|e| Decoding {
            message: "JWT authorization not satisfied while decoding token".to_string(),
            source: Arc::new(e),
        })
    }

    fn get_token(&self, headers: &HashMap<String, String>) -> AuthResult<String> {
        let value = Self::get_header_value(headers)?;
        Self::validate_auth_header_value(&value)?;

        let split_value: Vec<&str> = value.split_whitespace().collect();

        if let Some(token) = split_value.get(1) {
            Ok((*token).to_string())
        } else {
            Err(AuthError::BadFormat {
                message: "JWT Token must be in format 'Bearer <token>'".to_string(),
            })
        }
    }

    fn get_header_value(headers: &HashMap<String, String>) -> AuthResult<String> {
        match headers.get("authorization") {
            Some(value) => {
                Self::validate_auth_header_value(value)?;
                Ok(value.to_string())
            }
            None => Err(AuthError::BadRequest {
                message: "No Authorization Header Found.".to_string(),
            }),
        }
    }

    fn validate_auth_header_value(value: &str) -> AuthResult<()> {
        // https://stackoverflow.com/questions/55201011/what-characters-are-allowed-in-a-jwt-token
        let jwt_regex: regex::Regex = Regex::new(r"^Bearer\s[a-zA-Z0-9-_\.]+$").unwrap();

        if jwt_regex.is_match(value) {
            Ok(())
        } else {
            Err(AuthError::BadFormat {
                message: "JWT Token must be in format 'Bearer <token>'".to_string(),
            })
        }
    }
}

type AuthResult<T> = std::result::Result<T, AuthError>;

#[derive(Clone, Debug, Serialize, Error)]
pub enum AuthError {
    #[error("Unauthorized {}, {}", message, source)]
    Decoding {
        message: String,
        #[from(jsonwebtoken::errors::Error, Arc::new)]
        #[serde(serialize_with = "xand_utils::snafu_extensions::debug_serialize")]
        source: Arc<jsonwebtoken::errors::Error>,
    },

    #[error("Bad Request {}", message)]
    BadRequest { message: String },

    #[error("Bad Format {}", message)]
    BadFormat { message: String },
}

#[logging_event]
enum AuthEvent {
    Error { body: AuthError },
}

impl ResponseError for AuthError {
    fn error_response(&self) -> HttpResponse {
        error!(AuthEvent::Error { body: self.clone() });

        match self {
            Self::Decoding { message, source } => {
                HttpResponse::Unauthorized().json(format!("{} : {}", message, source))
            }
            Self::BadFormat { message } => HttpResponse::Unauthorized().json(message),
            Self::BadRequest { message } => HttpResponse::Unauthorized().json(message),
        }
    }
}

#[cfg(test)]
pub mod tests {

    use super::*;

    #[test]
    fn auth_error_decoding() {
        let source: jsonwebtoken::errors::Error =
            jsonwebtoken::errors::ErrorKind::InvalidToken.into();

        let error = AuthError::Decoding {
            message: "Error Text".into(),
            source: Arc::new(source),
        };

        let response = error.error_response();

        assert_eq!(401, response.status());
    }

    #[test]
    fn auth_error_bad_format() {
        let error = AuthError::BadFormat {
            message: "Error Text".into(),
        };

        let response = error.error_response();

        assert_eq!(401, response.status());
    }
}
