-- Your SQL goes here
CREATE TABLE account (
    id INTEGER PRIMARY KEY NOT NULL,
    short_name TEXT NOT NULL,
    account_number TEXT NOT NULL,
    bank_id INTEGER NOT NULL,
    UNIQUE (account_number, bank_id),
    FOREIGN KEY(bank_id) REFERENCES bank(id)
);
CREATE TABLE bank (
    id INTEGER PRIMARY KEY NOT NULL,
    name TEXT NOT NULL UNIQUE,
    routing_number TEXT NOT NULL UNIQUE,
    claims_account TEXT NOT NULL,
    adapter_type_id INTEGER NOT NULL,
    url TEXT NOT NULL,
    mcb_secret_user_name TEXT,
    mcb_secret_password TEXT,
    mcb_secret_client_app_ident TEXT,
    mcb_secret_organization_id TEXT,
    tp_secret_api_key_id TEXT,
    tp_secret_api_key_value TEXT,
    FOREIGN KEY(adapter_type_id) REFERENCES adapter_type(id)
);
CREATE TABLE adapter_type (
    id INTEGER PRIMARY KEY NOT NULL,
    type TEXT NOT NULL UNIQUE
);
INSERT INTO adapter_type(id, type)
VALUES (1, 'mcb'),
    (2, 'treasury-prime');