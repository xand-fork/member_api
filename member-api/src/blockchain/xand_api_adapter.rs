use crate::{
    blockchain::{Blockchain, CreateRequest, Receipt, RedeemRequest, SendClaimsRequest},
    error,
    error::Error,
    logging::LoggingEvent,
    Result,
};
use snafu::ResultExt;
use std::{convert::TryInto, sync::Arc};
use xand_address::Address;

use xand_api_client::{
    errors::XandApiClientError, CorrelationId, PendingCreateRequest, PendingRedeemRequest, Send,
    TransactionIdError, XandApiClientTrait, XandTransaction,
};

impl From<TransactionIdError> for Error {
    fn from(from: TransactionIdError) -> Error {
        match from.clone() {
            TransactionIdError::Encoding { input } => Error::Format {
                message: format!("Error formatting transaction id: {:?}", from),
                input: format!("{:?}", input),
            },
            TransactionIdError::Parsing { value, .. } => Error::Format {
                message: format!("Error formatting transaction id: {:?}", from),
                input: value,
            },
        }
    }
}

#[derive(Clone, new)]
pub struct XandApiAdapter {
    xand_client: Arc<dyn XandApiClientTrait>,
}

#[async_trait::async_trait]
impl Blockchain for XandApiAdapter {
    async fn issue_create_request(&self, request: CreateRequest) -> Result<Receipt> {
        let correlation_id = CorrelationId::gen_random();
        let correlation_id_str = correlation_id.to_string();
        info!(LoggingEvent::SubmitCreateRequest(
            correlation_id_str.clone()
        ));

        let txn = PendingCreateRequest {
            account: request.account,
            correlation_id,
            amount_in_minor_unit: request.amount,
            completing_transaction: None,
        };
        let receipt = self
            .xand_client
            .submit_transaction_wait(request.address, XandTransaction::CreateRequest(txn))
            .await
            .context(error::XandClientError {
                message: "while issuing create request".to_string(),
            })?;

        let result = Receipt {
            transaction_id: receipt.id.to_string(),
            correlation_id: Some(correlation_id_str),
        };

        Ok(result)
    }

    async fn redeem(&self, request: RedeemRequest) -> Result<Receipt> {
        let correlation_id = CorrelationId::gen_random();
        let correlation_id_str = correlation_id.to_string();
        info!(LoggingEvent::SubmitRedeem(correlation_id_str.clone()));

        let txn = PendingRedeemRequest {
            account: request.account,
            correlation_id,
            amount_in_minor_unit: request.amount,
            completing_transaction: None,
        };
        let receipt = self
            .xand_client
            .submit_transaction_wait(request.address, XandTransaction::RedeemRequest(txn))
            .await
            .context(error::XandClientError {
                message: "while attempting to redeem".to_string(),
            })?;

        Ok(Receipt {
            transaction_id: receipt.id.to_string(),
            correlation_id: Some(correlation_id_str),
        })
    }

    async fn send_claims(&self, request: SendClaimsRequest) -> Result<Receipt> {
        info!(LoggingEvent::SendClaimsToWallet {
            from: request.address.clone(),
            to: request.to.clone(),
        });

        let txn = Send {
            amount_in_minor_unit: request.amount,
            destination_account: request.to,
        };
        let receipt = self
            .xand_client
            .submit_transaction_wait(request.address, XandTransaction::Send(txn))
            .await
            .context(error::XandClientError {
                message: "while sending claims".to_string(),
            })?;
        Ok(Receipt {
            transaction_id: receipt.id.to_string(),
            correlation_id: None,
        })
    }

    async fn balance(&self, key: Address) -> Result<u64> {
        info!(LoggingEvent::RetrieveWalletBalance(key.clone()));
        let res = self
            .xand_client
            .get_balance(&key.to_string())
            .await
            .context(error::XandClientError {
                message: "while retrieving balance".to_string(),
            })?;
        let balance = res.ok_or_else(|| Error::XandClientError {
            message: "while attempting to read balance, which may be confidential".to_string(),
            source: XandApiClientError::NotFound {
                message: "Balance not available".to_string(),
            },
        })?;

        let formatted_balance = balance
            .try_into()
            .map_err(|_| Error::numeric_cast_failure::<u128, u64>(balance))?;
        Ok(formatted_balance)
    }

    async fn check_connectivity(&self) -> Result<()> {
        // Retrieving the current block is used as a proxy for connectivity health.
        // The actual block history doesn't matter, as long as we can retrieve it.
        self.xand_client.get_current_block().await?;
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::{blockchain::SendClaimsRequest, transaction::Reference};
    use pseudo::Mock;
    use std::convert::TryFrom;
    use xand_address::Address;
    use xand_api_client::{
        errors::XandApiClientError,
        mock::{committed_status, MockXandApiClient},
        BankAccountId, BankAccountInfo,
    };

    const TEST_USER: &str = "5Gph9iMER7s492Rm7i51oJkCr5snibMVWV6oqfEXEuxhv5xM";
    // Biggie
    const TEST_RECIPIENT: &str = "5EgqcJ1sbFkUvySAmxc3bThRePvdmw3zCrXMjqYPaSSSzbkQ";
    // It's ya boi 2pac!
    const TEST_ADDRESS: &str = "5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ";

    #[tokio::test]
    async fn test_issue_create_request() {
        // Given
        let mock_xand_client = Arc::new(MockXandApiClient {
            submits: Mock::new(Ok(vec![Ok(committed_status())])),
            ..Default::default()
        });

        let substrate_adapter = build_test_xand_api_adapter(mock_xand_client.clone());

        let account_id: BankAccountId = Default::default();
        let account_info: BankAccountInfo = account_id.into();
        // When
        let issue_create_request_req = CreateRequest {
            address: TEST_ADDRESS.parse().unwrap(),
            account: account_info,
            amount: 100,
            reference: Reference::default(),
        };
        substrate_adapter
            .issue_create_request(issue_create_request_req)
            .await
            .unwrap();

        // Then
        assert_eq!(mock_xand_client.submits.num_calls(), 1);
        let (_, txn) = &mock_xand_client.submits.calls()[0];
        match txn {
            XandTransaction::CreateRequest(pm) => assert_eq!(pm.amount_in_minor_unit, 100),
            _ => panic!("Wrong transaction type!"),
        }
    }

    #[tokio::test]
    async fn test_redeem() {
        // Given
        let mock_xand_client = Arc::new(MockXandApiClient {
            submits: Mock::new(Ok(vec![Ok(committed_status())])),
            ..Default::default()
        });

        let substrate_adapter = build_test_xand_api_adapter(mock_xand_client.clone());
        let account_id: BankAccountId = Default::default();
        let account_info: BankAccountInfo = account_id.into();

        // When
        let redeem_req = RedeemRequest {
            address: TEST_ADDRESS.parse().unwrap(),
            account: account_info,
            amount: 100,
            reference: Reference::default(),
        };
        substrate_adapter.redeem(redeem_req).await.unwrap();

        // Then
        assert_eq!(mock_xand_client.submits.num_calls(), 1);
        let (_, txn) = &mock_xand_client.submits.calls()[0];
        match txn {
            XandTransaction::RedeemRequest(ps) => assert_eq!(ps.amount_in_minor_unit, 100),
            _ => panic!("Wrong transaction type!"),
        }
    }

    #[tokio::test]
    async fn test_submission_errors_surface() {
        // Given
        let mock_xand_client = Arc::new(MockXandApiClient {
            submits: Mock::new(Err(XandApiClientError::BadReplyError {
                message: "Ahh!".to_string(),
            })),
            ..Default::default()
        });

        let substrate_adapter = build_test_xand_api_adapter(mock_xand_client.clone());
        let account_id: BankAccountId = Default::default();
        let account_info: BankAccountInfo = account_id.into();

        // Ensure all the types of transaction submissions bubble up errors
        let redeem_req = RedeemRequest {
            address: TEST_ADDRESS.parse().unwrap(),
            account: account_info.clone(),
            amount: 100,
            reference: Reference::default(),
        };
        let result = substrate_adapter.redeem(redeem_req).await;
        assert!(result.is_err());
        assert_eq!(mock_xand_client.submits.num_calls(), 1);

        let issue_create_request_req = CreateRequest {
            address: TEST_ADDRESS.parse().unwrap(),
            amount: 100,
            account: account_info,
            reference: Reference::default(),
        };
        let result = substrate_adapter
            .issue_create_request(issue_create_request_req)
            .await;
        assert!(result.is_err());
        assert_eq!(mock_xand_client.submits.num_calls(), 2);

        let send_claims_req = SendClaimsRequest {
            address: TEST_ADDRESS.parse().unwrap(),
            to: TEST_RECIPIENT.parse().unwrap(),
            amount: 100,
            reference: Reference::default(),
        };
        let result = substrate_adapter.send_claims(send_claims_req).await;
        assert!(result.is_err());
        assert_eq!(mock_xand_client.submits.num_calls(), 3);
    }

    #[tokio::test]
    async fn test_transfer() {
        // Given
        let mock_xand_client = Arc::new(MockXandApiClient {
            submits: Mock::new(Ok(vec![Ok(committed_status())])),
            ..Default::default()
        });
        let substrate_adapter = build_test_xand_api_adapter(mock_xand_client.clone());
        let recip_addr: Address = TEST_RECIPIENT.parse().unwrap();

        // When
        let send_claims_req = SendClaimsRequest {
            address: TEST_ADDRESS.parse().unwrap(),
            to: TEST_RECIPIENT.parse().unwrap(),
            amount: 100,
            reference: Reference::default(),
        };
        substrate_adapter
            .send_claims(send_claims_req)
            .await
            .unwrap();

        // Then
        assert_eq!(mock_xand_client.submits.num_calls(), 1);
        let (_, txn) = &mock_xand_client.submits.calls()[0];
        match txn {
            XandTransaction::Send(s) => {
                assert_eq!(s.amount_in_minor_unit, 100);
                assert_eq!(s.destination_account, recip_addr)
            }
            _ => panic!("Wrong transaction type!"),
        }
    }

    #[tokio::test]
    async fn test_balance_successful() {
        // Given
        let current_balance: u64 = 42_u64; // Format provided by the Xand API
        let mock_xand_client = Arc::new(MockXandApiClient {
            balance: Mock::new(Ok(u128::from(current_balance).into())),
            ..Default::default()
        });
        let substrate_adapter = build_test_xand_api_adapter(mock_xand_client.clone());

        // When
        let result = substrate_adapter.balance(TEST_USER.parse().unwrap()).await;

        // Then
        assert_eq!(result.unwrap(), current_balance);
        assert_eq!(mock_xand_client.balance.num_calls(), 1);
        assert_eq!(mock_xand_client.balance.calls()[0], TEST_USER.to_string());
    }

    /// Builds the SubstrateAdapter with the given MockXandAPIClient for testing purposes
    fn build_test_xand_api_adapter(mock_xand_client: Arc<MockXandApiClient>) -> XandApiAdapter {
        XandApiAdapter::new(mock_xand_client)
    }

    #[tokio::test]
    async fn test_zero_balance_successful() {
        // Given
        // A member can have 0 XAND wallet balance
        let zero_balance: u64 = 0_u64; // Format provided by the Member API
        let mock_xand_client = Arc::new(MockXandApiClient {
            balance: Mock::new(Ok(u128::try_from(zero_balance).unwrap().into())),
            ..Default::default()
        });
        let substrate_adapter = build_test_xand_api_adapter(mock_xand_client.clone());

        // When
        let result = substrate_adapter.balance(TEST_USER.parse().unwrap()).await;

        // Then
        assert_eq!(result.unwrap(), zero_balance);
        assert_eq!(mock_xand_client.balance.num_calls(), 1);
        assert_eq!(mock_xand_client.balance.calls()[0], TEST_USER.to_string());
    }

    #[tokio::test]
    async fn test_balance_api_error() {
        // Given
        // Some unspecified error happens at the XAND Api client level
        let mock_xand_client = Arc::new(MockXandApiClient {
            balance: Mock::new(Err(XandApiClientError::BadReplyError {
                message: "Something went wrong at the Xand API level".to_string(),
            })),
            ..Default::default()
        });
        let substrate_adapter = build_test_xand_api_adapter(mock_xand_client.clone());

        // When
        let result = substrate_adapter.balance(TEST_USER.parse().unwrap()).await;

        // Then
        assert_eq!(mock_xand_client.balance.num_calls(), 1);
        assert_eq!(
            mock_xand_client.balance.calls()[0].to_string(),
            TEST_USER.to_string()
        );
        assert!(result.is_err());
        assert!(matches!(result, Err(Error::XandClientError { .. }))); // Generic error since we don't know why the call failed
    }

    #[tokio::test]
    async fn test_unavailable_balance_error() {
        // Given
        // Amount is unavailable, possibly because cannot be decrypted. This is expected for amounts not belonging to the member.
        let mock_xand_client = Arc::new(MockXandApiClient {
            balance: Mock::new(Ok(None)),
            ..Default::default()
        });
        let substrate_adapter = build_test_xand_api_adapter(mock_xand_client.clone());

        // When
        let result = substrate_adapter.balance(TEST_USER.parse().unwrap()).await;

        // Then
        assert_eq!(mock_xand_client.balance.num_calls(), 1);
        assert_eq!(mock_xand_client.balance.calls()[0], TEST_USER.to_string());
        assert!(result.is_err());
        assert!(matches!(
            result,
            Err(Error::XandClientError {
                source: XandApiClientError::NotFound { .. },
                ..
            })
        ));
    }

    #[tokio::test]
    async fn test_u128_max_balance_returns_appropriate_error() {
        // Given
        // Unlikely that a member would have a balance equivalent to the u128 max, but still possibility given that Xand API responds with u128
        let max_balance: u128 = u128::max_value(); // Format provided by the XAND API

        let mock_xand_client = Arc::new(MockXandApiClient {
            balance: Mock::new(Ok(max_balance.into())),
            ..Default::default()
        });
        let substrate_adapter = build_test_xand_api_adapter(mock_xand_client.clone());

        // When
        let result = substrate_adapter.balance(TEST_USER.parse().unwrap()).await;

        // Then
        assert!(result.is_err());
        assert!(matches!(result, Err(Error::PrimitiveCastFailure { .. })));
        // TODO https://dev.azure.com/transparentsystems/xand-network/_workitems/edit/6394
        // Assess if we are ok with this error case where the u128 is passed in and overflows a u64 used by member-api
    }
}
