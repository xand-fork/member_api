use crate::{health::*, LoggingEvent};
use std::sync::Arc;
use xand_secrets::{CheckHealthError, SecretKeyValueStore};

pub enum SecretStoreHealthChecker {
    SecretStore(Arc<dyn SecretKeyValueStore>),
    FailedToCreate,
}

impl SecretStoreHealthChecker {
    fn log_secret_store_check_health_error(error: CheckHealthError) {
        error!(LoggingEvent::ErrorWithMessage(
            String::from("Secret store reported a health issue"),
            error.into()
        ));
    }
}

#[async_trait::async_trait]
impl Service for SecretStoreHealthChecker {
    fn name(&self) -> String {
        "secret store".to_string()
    }

    async fn health(&self) -> ServiceStatus {
        match self {
            SecretStoreHealthChecker::SecretStore(secret_store) => {
                match secret_store.check_health().await {
                    Ok(_) => ServiceStatus::Up,
                    Err(e) => {
                        Self::log_secret_store_check_health_error(e);
                        ServiceStatus::Down {
                            details: "Secret store is unhealthy or unavailable.".to_string(),
                        }
                    }
                }
            }
            SecretStoreHealthChecker::FailedToCreate => ServiceStatus::Down {
                details: "Secret store initialization failed.".to_string(),
            },
        }
    }
}

#[cfg(test)]
mod tests {
    use super::{super::Service, SecretStoreHealthChecker};
    use crate::health::ServiceStatus;
    use async_trait::async_trait;
    use std::{error::Error as StdError, fmt::Display, sync::Arc};
    use xand_secrets::{CheckHealthError, ReadSecretError, Secret, SecretKeyValueStore};

    pub enum HealthStatus {
        Healthy,
        AuthenticationError,
        RemoteInternalError,
        UnreachableError,
    }

    struct FakeSecretStore(HealthStatus);

    #[derive(Debug)]
    struct TestError(String);

    impl Display for TestError {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            write!(f, "{}", self.0)
        }
    }

    impl StdError for TestError {
        fn source(&self) -> Option<&(dyn StdError + 'static)> {
            None
        }
    }

    #[async_trait]
    impl SecretKeyValueStore for FakeSecretStore {
        async fn read(&self, key: &str) -> Result<Secret<String>, ReadSecretError> {
            Err(ReadSecretError::KeyNotFound {
                key: String::from(key),
            })
        }

        async fn check_health(&self) -> Result<(), CheckHealthError> {
            match self.0 {
                HealthStatus::Healthy => Ok(()),
                HealthStatus::AuthenticationError => Err(CheckHealthError::Authentication {
                    internal_error: Box::new(TestError(String::from("Authentication"))),
                }),
                HealthStatus::RemoteInternalError => Err(CheckHealthError::RemoteInternal {
                    internal_error: Box::new(TestError(String::from("RemoteInternal"))),
                }),
                HealthStatus::UnreachableError => Err(CheckHealthError::Unreachable {
                    internal_error: Box::new(TestError(String::from("Unreachable"))),
                }),
            }
        }
    }

    #[tokio::test]
    async fn test_healthy() {
        let service =
            SecretStoreHealthChecker::SecretStore(Arc::new(FakeSecretStore(HealthStatus::Healthy)));

        assert_eq!("secret store", service.name());
        assert_eq!(ServiceStatus::Up, service.health().await);
    }

    #[tokio::test]
    async fn test_failed_creation() {
        let service = SecretStoreHealthChecker::FailedToCreate;

        assert_eq!("secret store", service.name());
        assert_eq!(
            ServiceStatus::Down {
                details: String::from("Secret store initialization failed.")
            },
            service.health().await
        );
    }

    #[tokio::test]
    async fn test_auth_error() {
        let service = SecretStoreHealthChecker::SecretStore(Arc::new(FakeSecretStore(
            HealthStatus::AuthenticationError,
        )));

        assert_eq!(
            ServiceStatus::Down {
                details: String::from("Secret store is unhealthy or unavailable.")
            },
            service.health().await
        );
    }

    #[tokio::test]
    async fn test_remote_error() {
        let service = SecretStoreHealthChecker::SecretStore(Arc::new(FakeSecretStore(
            HealthStatus::RemoteInternalError,
        )));

        assert_eq!(
            ServiceStatus::Down {
                details: String::from("Secret store is unhealthy or unavailable.")
            },
            service.health().await
        );
    }

    #[tokio::test]
    async fn test_unreachable_error() {
        let service = SecretStoreHealthChecker::SecretStore(Arc::new(FakeSecretStore(
            HealthStatus::UnreachableError,
        )));

        assert_eq!(
            ServiceStatus::Down {
                details: String::from("Secret store is unhealthy or unavailable.")
            },
            service.health().await
        );
    }
}
