use super::Service;
use async_trait::async_trait;

pub(crate) struct InvalidService {
    name: String,
    details: String,
}

#[async_trait]
impl Service for InvalidService {
    fn name(&self) -> String {
        self.name.clone()
    }
    async fn health(&self) -> super::ServiceStatus {
        super::ServiceStatus::Down {
            details: self.details.clone(),
        }
    }
}

impl InvalidService {
    pub fn new(name: String, details: String) -> InvalidService {
        InvalidService { name, details }
    }
}

#[cfg(test)]
pub mod tests {
    use crate::health::{invalid::InvalidService, Service, ServiceHealth};

    #[tokio::test]
    async fn service_health_down_with_invalid_service() {
        let services: Vec<Box<dyn Service>> = vec![Box::new(InvalidService::new(
            String::from("Test error name"),
            String::from("Test error details"),
        ))];

        let health = ServiceHealth::check(services).await;

        assert!(!health.up());
    }
}
