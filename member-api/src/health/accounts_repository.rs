use crate::{
    accounts::repository::{errors::AccountsRepositoryError, AccountsRepository},
    LoggingEvent,
};

use super::{Service, ServiceStatus};

pub struct AccountsRepositoryHealthService {
    repository: Box<dyn AccountsRepository>,
}

impl AccountsRepositoryHealthService {
    fn log_check_health_error(error: AccountsRepositoryError) {
        error!(LoggingEvent::ErrorMessageOnly(format!(
            "Accounts repository health-check returned error. {}",
            error
        )));
    }
}

#[async_trait::async_trait]
impl Service for AccountsRepositoryHealthService {
    fn name(&self) -> String {
        String::from("Accounts storage backend")
    }

    async fn health(&self) -> ServiceStatus {
        self.repository
            .check_health()
            .err()
            .map_or(ServiceStatus::Up, |e| {
                Self::log_check_health_error(e);
                ServiceStatus::Down {
                    details: String::from("An error occurred while accessing the store."),
                }
            })
    }
}

impl AccountsRepositoryHealthService {
    pub fn new(repository: Box<dyn AccountsRepository>) -> Self {
        Self { repository }
    }
}

#[cfg(test)]
pub mod tests {
    use crate::{
        accounts::{
            repository::{errors::AccountsRepositoryError, sqlite::SqliteAccountsRepository},
            testing::MockAccountsRepository,
        },
        health::{Service, ServiceHealth},
        tests::test_context::temp_db::TestDatabase,
    };

    use super::AccountsRepositoryHealthService;
    use crate::accounts::repository::errors::AccountsRepositoryErrorSource;

    #[tokio::test]
    async fn service_health_up_with_healthy_repository() {
        let accounts_repository = MockAccountsRepository::default();
        accounts_repository.mock_check_health.return_ok(());

        let services: Vec<Box<dyn Service>> = vec![Box::new(AccountsRepositoryHealthService::new(
            Box::new(accounts_repository),
        ))];

        let health = ServiceHealth::check(services).await;

        assert!(health.up());
    }

    #[tokio::test]
    async fn service_health_down_with_erroring_repository() {
        let accounts_repository = MockAccountsRepository::default();
        accounts_repository
            .mock_check_health
            .return_err(AccountsRepositoryError::AccessFailure {
                msg: "An arbitrary error occurred".into(),
                source: AccountsRepositoryErrorSource::AccountsRepository,
            });

        let services: Vec<Box<dyn Service>> = vec![Box::new(AccountsRepositoryHealthService::new(
            Box::new(accounts_repository),
        ))];

        let health = ServiceHealth::check(services).await;

        assert!(!health.up());
    }

    #[tokio::test]
    async fn integ_service_health_down_with_erroring_sqlite_repository() {
        // Given
        let db = TestDatabase::default();
        // intentionally omitted db initialization; no migrations have been applied.
        let accounts_repository = SqliteAccountsRepository::new(db.conn_pool());

        // When
        let services: Vec<Box<dyn Service>> = vec![Box::new(AccountsRepositoryHealthService::new(
            Box::new(accounts_repository),
        ))];
        let health = ServiceHealth::check(services).await;

        // Then
        assert!(!health.up());
        assert_eq!(health.down.len(), 1);
        assert_eq!(health.down[0].service.as_str(), "Accounts storage backend");
    }
}
