use unicode_segmentation::UnicodeSegmentation;
use unicode_truncate::{Alignment, UnicodeTruncateStr};

use crate::{
    banks::BankAdapter, database::models::account::stored_bank_account::StoredBankAccount,
};

pub mod repository;
pub mod web;

#[cfg(test)]
pub mod testing;

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Account {
    pub id: Option<i32>,
    pub bank_id: i32,
    pub short_name: String,
    pub account_number: String,
}

/// Details for an account. Currently serialzied over the wire.
#[derive(Clone, Debug, Eq, PartialEq)]
pub struct AccountDetails {
    pub id: i32,
    pub bank_id: i32,
    pub bank_name: String,
    pub short_name: String,
    pub account_number: String,
    pub routing_number: String,
}

pub const UNRESOLVED_ACCOUNT_NUMBER: &str = "N/A";
impl AccountDetails {
    pub fn unresolvable() -> Self {
        AccountDetails {
            id: -1,
            bank_id: -1,
            short_name: "UNRESOLVABLE".to_string(),
            bank_name: "UNRESOLVABLE BANK".to_string(),
            account_number: UNRESOLVED_ACCOUNT_NUMBER.to_string(),
            routing_number: "UNRESOLVABLE".to_string(),
        }
    }

    pub fn masked_account_number(&self) -> String {
        let account_number = self.clone().account_number;
        if account_number.eq(&UNRESOLVED_ACCOUNT_NUMBER.to_string()) {
            return account_number;
        }

        let last_digits_account_number = account_number
            .graphemes(true)
            .rev()
            .collect::<String>()
            .unicode_pad(4, Alignment::Left, true)
            .graphemes(true)
            .rev()
            .collect::<String>()
            .replace(' ', "X");
        format!("XXXXXXXX{}", last_digits_account_number)
    }
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct BankAccountConnectionData {
    pub account_id: i32,
    pub bank_id: i32,
    pub routing_number: String,
    pub account_number: String,
    pub claims_account: String,
    pub adapter: BankAdapter,
}

#[cfg(test)]
impl Default for Account {
    fn default() -> Self {
        Account {
            id: Some(1),
            bank_id: 1,
            short_name: "Gringott's".to_string(),
            account_number: "123456789012".to_string(),
        }
    }
}

#[cfg(test)]
impl Default for AccountDetails {
    fn default() -> Self {
        AccountDetails {
            id: 1,
            bank_id: 1,
            bank_name: "Gringott's Bank".to_string(),
            short_name: "Gringott's".to_string(),
            account_number: "123456789012".to_string(),
            routing_number: "12345678".to_string(),
        }
    }
}

impl From<StoredBankAccount> for Account {
    fn from(account: StoredBankAccount) -> Self {
        Account {
            id: Some(account.id),
            bank_id: account.bank_id,
            short_name: account.short_name,
            account_number: account.account_number,
        }
    }
}

#[cfg(test)]
pub mod tests {
    use crate::tests::TestDataBuilder;

    use super::AccountDetails;

    #[test]
    fn account_details_test_data_builder() {
        let details = AccountDetails::build(2);

        assert_eq!(details.account_number, "000000000003");
        assert_eq!(details.routing_number, "000000003");
    }

    #[test]
    fn account_details_masked_account_number() {
        let details = AccountDetails::build(2);
        assert_eq!(details.masked_account_number(), "XXXXXXXX0003");
    }

    #[test]
    fn account_details_masked_account_number_handles_short_number_strings() {
        let details =
            AccountDetails::build_with(2, |account, _| account.account_number = "03".to_string());
        assert_eq!(details.masked_account_number(), "XXXXXXXXXX03".to_string());
    }

    #[test]
    fn account_details_masked_account_number_handles_long_number_strings() {
        let details = AccountDetails::build_with(2, |account, _| {
            account.account_number = "12340000000003".to_string()
        });
        assert_eq!(details.masked_account_number(), "XXXXXXXX0003".to_string());
    }

    #[test]
    fn account_details_masked_account_number_with_long_unicode_under_4_chars() {
        let account_number = "éeé";
        let details = AccountDetails::build_with(2, |account, _| {
            account.account_number = account_number.to_string()
        });
        assert_eq!(details.masked_account_number(), "XXXXXXXXXéeé".to_string());
    }

    #[test]
    fn account_details_masked_account_number_with_long_unicode_over_4_chars() {
        let account_number = "éeééeeeeeeéeee";
        let details = AccountDetails::build_with(2, |account, _| {
            account.account_number = account_number.to_string()
        });
        assert_eq!(details.masked_account_number(), "XXXXXXXXéeee".to_string());
    }
}
