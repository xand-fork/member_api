use crate::database::DatabaseError;
use thiserror::Error;

pub type AccountsRepositoryResult<T> = Result<T, AccountsRepositoryError>;

#[derive(Clone, Eq, PartialEq, Debug, Serialize, Deserialize, Error)]
pub enum AccountsRepositoryErrorSource {
    #[error("Database")]
    InternalDatabase,
    #[error("Accounts Repository")]
    AccountsRepository,
}

#[derive(Clone, Eq, PartialEq, Debug, Serialize, Deserialize, Error)]
pub enum AccountsRepositoryError {
    #[error("access failure: {msg:?} (Source: {source:?}")]
    AccessFailure {
        msg: String,
        source: AccountsRepositoryErrorSource,
    },
    #[error("not found")]
    NotFound,
    #[error("repository error: {msg:?} (Source: {source:?}")]
    RepositoryFailure {
        msg: String,
        source: AccountsRepositoryErrorSource,
    },
    #[error("validation error: {msg:?} (Source: {source:?}")]
    ValidationFailure {
        msg: String,
        source: AccountsRepositoryErrorSource,
    },
    #[error("unknown error: {msg:?} (Source: {source:?}")]
    Unknown {
        msg: String,
        source: AccountsRepositoryErrorSource,
    },
}
impl AccountsRepositoryError {
    pub fn contains_internal_db_msg(&mut self) -> bool {
        match self.clone() {
            AccountsRepositoryError::AccessFailure { source, msg: _msg }
            | AccountsRepositoryError::RepositoryFailure { source, msg: _msg }
            | AccountsRepositoryError::ValidationFailure { source, msg: _msg }
            | AccountsRepositoryError::Unknown { source, msg: _msg } => match source {
                AccountsRepositoryErrorSource::InternalDatabase => true,
                AccountsRepositoryErrorSource::AccountsRepository => false,
            },
            AccountsRepositoryError::NotFound => false,
        }
    }
}

impl From<DatabaseError> for AccountsRepositoryError {
    fn from(error: DatabaseError) -> Self {
        match error {
            DatabaseError::ConnectionError { source } => AccountsRepositoryError::AccessFailure {
                msg: source.to_string(),
                source: AccountsRepositoryErrorSource::InternalDatabase,
            },
            DatabaseError::MigrationError { source } => {
                AccountsRepositoryError::RepositoryFailure {
                    msg: source.to_string(),
                    source: AccountsRepositoryErrorSource::InternalDatabase,
                }
            }
            DatabaseError::ParseError { source } => AccountsRepositoryError::RepositoryFailure {
                msg: source.to_string(),
                source: AccountsRepositoryErrorSource::InternalDatabase,
            },
            DatabaseError::PathError {
                msg,
                is_internal_msg,
            } => AccountsRepositoryError::AccessFailure {
                msg,
                source: if is_internal_msg {
                    AccountsRepositoryErrorSource::InternalDatabase
                } else {
                    AccountsRepositoryErrorSource::AccountsRepository
                },
            },
            DatabaseError::ValidationError {
                msg,
                is_internal_msg,
            } => AccountsRepositoryError::ValidationFailure {
                msg,
                source: if is_internal_msg {
                    AccountsRepositoryErrorSource::InternalDatabase
                } else {
                    AccountsRepositoryErrorSource::AccountsRepository
                },
            },
            DatabaseError::QueryError { source } => AccountsRepositoryError::RepositoryFailure {
                msg: source.to_string(),
                source: AccountsRepositoryErrorSource::InternalDatabase,
            },
            DatabaseError::NotFound => AccountsRepositoryError::NotFound,
            DatabaseError::UnhandledError { source } => {
                AccountsRepositoryError::RepositoryFailure {
                    msg: source.to_string(),
                    source: AccountsRepositoryErrorSource::InternalDatabase,
                }
            }
        }
    }
}

impl From<AccountsRepositoryError> for crate::Error {
    fn from(input: AccountsRepositoryError) -> Self {
        match input {
            AccountsRepositoryError::NotFound => crate::Error::NotFound {
                message: "Record not found".to_string(),
            },
            _ => crate::Error::Database {
                message: format!("{:?}", input),
            },
        }
    }
}
