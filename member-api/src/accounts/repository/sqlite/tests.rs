use std::collections::HashSet;

use crate::{
    accounts::{
        repository::{
            errors::{AccountsRepositoryError, AccountsRepositoryErrorSource},
            AccountsRepository,
        },
        testing::AccountNumbers,
        Account, AccountDetails,
    },
    banks::{repository::BanksRepository, Bank},
    tests::{TestContext, TestDataBuilder},
};

#[test]
fn sqlite_accounts_repository_can_insert_and_get() {
    let ctx = TestContext::default();
    let bank = Bank::build_with(12345, |bank, _| {
        bank.id = None;
    });
    let bank = ctx.banks_repository.insert(bank).unwrap();
    let bank_id = bank.id.unwrap();

    let account = Account::build_with(11, |account, _index| {
        account.id = None;
        account.bank_id = bank_id;
    });

    let result = ctx.accounts_repository.insert(account.clone()).unwrap();

    assert_eq!(
        result,
        AccountDetails {
            id: 1,
            bank_id: bank_id as i32,
            bank_name: bank.name,
            short_name: account.short_name,
            account_number: account.account_number,
            routing_number: bank.routing_number,
        }
    );
}

#[test]
fn sqlite_accounts_repository_can_insert_and_get_all() {
    let ctx = crate::tests::TestContext::default();
    let (_, accounts) = ctx.generate_banks_and_accounts(2, 2);

    let result = ctx.accounts_repository.get_all().unwrap();

    assert_eq!(result.len(), accounts.len());
}

#[test]
fn sqlite_accounts_repository_cannot_insert_duplicate_account_numbers_for_bank() {
    let ctx = crate::tests::TestContext::default();
    let banks = ctx.generate_banks(1).unwrap();
    let bank = banks[0].clone();

    let accounts = Account::build_vec_with(2, |account, _index| {
        account.id = None;
        account.bank_id = bank.id.unwrap();
        account.account_number = 12345.to_account_number();
    });

    ctx.accounts_repository.insert(accounts[0].clone()).unwrap();

    let result = ctx
        .accounts_repository
        .insert(accounts[1].clone())
        .unwrap_err();

    assert_eq!(
        result,
        AccountsRepositoryError::ValidationFailure {
            msg: "UniqueViolation \"UNIQUE constraint failed: account.account_number, account.bank_id\"".into(),
            source: AccountsRepositoryErrorSource::InternalDatabase,
        }
    );
}

#[test]
fn sqlite_accounts_repository_cannot_get_missing() {
    let ctx = crate::tests::TestContext::default();

    let error = ctx.accounts_repository.get(0).unwrap_err();

    assert_eq!(error, AccountsRepositoryError::NotFound);
}

#[test]
fn sqlite_accounts_repository_can_delete() {
    let ctx = crate::tests::TestContext::default();

    let (_, accounts) = ctx.generate_banks_and_accounts(1, 1);

    ctx.accounts_repository.remove(accounts[0].id).unwrap();
}

#[test]
fn sqlite_accounts_repository_cannot_remove_nonexisting() {
    let ctx = crate::tests::TestContext::default();

    let error = ctx.accounts_repository.remove(0).unwrap_err();

    assert_eq!(error, AccountsRepositoryError::NotFound);
}

#[test]
fn sqlite_accounts_repository_can_insert_and_replace_with_upsert() {
    let ctx = crate::tests::TestContext::default();
    let banks = ctx.generate_banks(1).unwrap();
    let bank = banks[0].clone();
    let bank_id = bank.id.unwrap();

    let account = Account::build_with(11, |mut item, _| {
        item.id = None;
        item.bank_id = bank_id;
    });

    let insert_result = ctx.accounts_repository.insert(account).unwrap();

    let updated_account = Account {
        id: Some(insert_result.id),
        bank_id,
        short_name: "Old Account".to_string(),
        account_number: "9999999999".to_string(),
    };

    let account_result = ctx
        .accounts_repository
        .update(updated_account.clone())
        .unwrap();

    assert_eq!(
        account_result,
        AccountDetails {
            id: insert_result.id,
            bank_id,
            bank_name: bank.name,
            short_name: updated_account.short_name,
            account_number: updated_account.account_number,
            routing_number: bank.routing_number,
        }
    );
}

#[test]
fn sqlite_accounts_repository_can_get_all_empty() {
    let ctx = crate::tests::TestContext::default();

    let get_all_result = ctx.accounts_repository.get_all().unwrap();
    assert_eq!(get_all_result.len(), 0);
}

#[test]
fn sqlite_accounts_repository_cannot_insert_account_with_missing_bank() {
    let ctx = crate::tests::TestContext::default();

    let account = Account::build_with(11, |account, _| {
        account.id = None;
    });

    let insert_error = ctx.accounts_repository.insert(account).unwrap_err();

    assert_eq!(
        insert_error,
        AccountsRepositoryError::ValidationFailure {
            msg: "ForeignKeyViolation \"FOREIGN KEY constraint failed\"".into(),
            source: AccountsRepositoryErrorSource::InternalDatabase,
        }
    );
}

#[test]
fn sqlite_accounts_repository_can_filter_by_bank() {
    let ctx = TestContext::default();

    let _banks = ctx.generate_banks(2).unwrap();

    let accounts = Account::build_vec_with(4, |account, index| {
        account.id = None;
        account.bank_id = (index % 2 + 1) as i32;
    });

    for account in accounts {
        ctx.accounts_repository.insert(account).unwrap();
    }

    let get_single_result = ctx.accounts_repository.get_single_per_bank().unwrap();
    let bank_id_results_set: HashSet<_> = get_single_result.iter().map(|a| a.bank_id).collect();
    let expected_bank_ids = {
        let mut set = HashSet::new();
        set.insert(1);
        set.insert(2);
        set
    };

    assert_eq!(get_single_result.len(), 2);
    assert_eq!(bank_id_results_set, expected_bank_ids);
}
