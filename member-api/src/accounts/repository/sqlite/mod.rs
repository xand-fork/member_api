use crate::{
    accounts::{
        repository::{
            errors::{
                AccountsRepositoryError, AccountsRepositoryErrorSource, AccountsRepositoryResult,
            },
            AccountsRepository,
        },
        Account, AccountDetails, BankAccountConnectionData,
    },
    database::{
        connection_pool_builder::SqliteConnectionPool,
        models::{
            account::{
                new_bank_account::NewBankAccount, stored_bank_account::StoredBankAccount,
                update_bank_account::UpdateBankAccount,
            },
            bank::stored_bank::StoredBank,
        },
        schema::{
            account::dsl::{
                account as account_table, account_number as account_number_field, id as id_field,
            },
            bank::dsl::{bank as bank_table, routing_number as routing_number_field},
        },
    },
};
use diesel::{ExpressionMethods, OptionalExtension, QueryDsl, RunQueryDsl};
use itertools::Itertools;

#[cfg(test)]
mod tests;

#[derive(Clone)]
pub struct SqliteAccountsRepository {
    pub conn_pool: SqliteConnectionPool,
}

// This macro is required in order to get the id of the last account
// inserted into the table.
//
no_arg_sql_function!(
    last_insert_rowid,
    diesel::sql_types::Integer,
    "Represents the SQL last_insert_row() function"
);

impl SqliteAccountsRepository {
    pub fn new(conn_pool: SqliteConnectionPool) -> Self {
        SqliteAccountsRepository { conn_pool }
    }

    pub fn new_boxed(conn_pool: SqliteConnectionPool) -> Box<Self> {
        Box::new(SqliteAccountsRepository::new(conn_pool))
    }

    fn get_account(&self, id: i32) -> AccountsRepositoryResult<AccountDetails> {
        let results: Vec<(StoredBankAccount, StoredBank)> =
            self.conn_pool.connect_and_run(|conn| {
                let join = account_table.filter(id_field.eq(id)).inner_join(bank_table);
                // By default, all columns from both tables are selected
                let rows = join.load(conn)?;
                Ok(rows)
            })?;

        // Take first row from join result.
        match results.into_iter().next() {
            None => Err(AccountsRepositoryError::NotFound),
            Some((account_dto, bank_dto)) => {
                let acct = AccountDetails {
                    id: account_dto.id,
                    bank_name: bank_dto.name,
                    bank_id: account_dto.bank_id,
                    account_number: account_dto.account_number,
                    routing_number: bank_dto.routing_number,
                    short_name: account_dto.short_name,
                };
                Ok(acct)
            }
        }
    }
}

impl AccountsRepository for SqliteAccountsRepository {
    fn update(&self, account: Account) -> AccountsRepositoryResult<AccountDetails> {
        if account.id.is_none() {
            return Err(AccountsRepositoryError::ValidationFailure {
                msg: "Id is required.".to_string(),
                source: AccountsRepositoryErrorSource::AccountsRepository,
            });
        }

        let id = account.id.unwrap_or_default();
        self.conn_pool.connect_and_run(|conn| {
            let edit_account: UpdateBankAccount = account.into();

            diesel::update(account_table.filter(id_field.eq(id)))
                .set(&edit_account)
                .execute(conn)?;
            Ok(())
        })?;

        let result_account = self.get_account(id)?;
        Ok(result_account)
    }

    fn insert(&self, account: Account) -> AccountsRepositoryResult<AccountDetails> {
        if account.id.is_some() {
            return Err(AccountsRepositoryError::ValidationFailure {
                msg: "Id is not allowed.".to_string(),
                source: AccountsRepositoryErrorSource::AccountsRepository,
            });
        }

        let id = self.conn_pool.connect_and_run(|connection| {
            let new_account: NewBankAccount = account.into();
            diesel::insert_into(account_table)
                .values(&new_account)
                .execute(connection)?;

            let id = diesel::select(last_insert_rowid).get_result::<i32>(connection)?;

            Ok(id)
        })?;

        self.get(id)
    }

    fn remove(&self, id: i32) -> AccountsRepositoryResult<()> {
        self.conn_pool
            .connect_and_run(|conn| {
                let num_rows_deleted =
                    diesel::delete(account_table.filter(id_field.eq(id))).execute(conn)?;

                Ok(num_rows_deleted)
            })
            .map_err(Into::into)
            .and_then(|num_rows_deleted| {
                match num_rows_deleted {
                    // Infer that no account was found if no rows were deleted
                    0 => Err(AccountsRepositoryError::NotFound),
                    _ => Ok(()),
                }
            })
    }

    fn get(&self, id: i32) -> AccountsRepositoryResult<AccountDetails> {
        self.get_account(id)
    }

    fn get_by_account_number(
        &self,
        routing_number: &str,
        account_number: &str,
    ) -> AccountsRepositoryResult<AccountDetails> {
        self.conn_pool
            .connect_and_run(|connection| {
                let join = account_table
                    .filter(account_number_field.eq(account_number))
                    .inner_join(bank_table)
                    .filter(routing_number_field.eq(routing_number));

                // By default, all columns from both tables are selected
                let (account_dto, bank_dto) =
                    join.first::<(StoredBankAccount, StoredBank)>(connection)?;

                Ok(AccountDetails {
                    id: account_dto.id,
                    bank_id: bank_dto.id,
                    bank_name: bank_dto.name,
                    short_name: account_dto.short_name,
                    routing_number: bank_dto.routing_number,
                    account_number: account_dto.account_number,
                })
            })
            .map_err(Into::into)
    }

    fn get_all(&self) -> AccountsRepositoryResult<Vec<AccountDetails>> {
        let accounts = self.conn_pool.connect_and_run(|connection| {
            let join = account_table.inner_join(bank_table);

            let results = join.load::<(StoredBankAccount, StoredBank)>(connection)?;

            let accounts: Vec<AccountDetails> = results
                .into_iter()
                .map(|(account_dto, bank_dto)| AccountDetails {
                    id: account_dto.id,
                    bank_id: account_dto.bank_id,
                    bank_name: bank_dto.name,
                    account_number: account_dto.account_number,
                    routing_number: bank_dto.routing_number,
                    short_name: account_dto.short_name,
                })
                .collect();

            Ok(accounts)
        })?;
        Ok(accounts)
    }

    fn get_single_per_bank(&self) -> AccountsRepositoryResult<Vec<AccountDetails>> {
        // TODO: do this natively as a db query
        Ok(self
            .get_all()?
            .into_iter()
            .unique_by(|a| a.bank_id)
            .collect())
    }

    fn check_health(&self) -> AccountsRepositoryResult<()> {
        self.conn_pool
            .connect_and_run(|conn| {
                let _bank = account_table.first::<StoredBankAccount>(conn).optional()?;
                Ok(())
            })
            .map_err(Into::into)
    }

    fn get_bank_account_connection(
        &self,
        id: i32,
    ) -> AccountsRepositoryResult<BankAccountConnectionData> {
        self.conn_pool
            .connect_and_run(|connection| {
                let join = account_table.filter(id_field.eq(id)).inner_join(bank_table);

                // By default, all columns from both tables are selected
                let (account_dto, bank_dto) =
                    join.first::<(StoredBankAccount, StoredBank)>(connection)?;

                let adapter = bank_dto.map_adapter()?;

                Ok(BankAccountConnectionData {
                    account_id: id,
                    bank_id: bank_dto.id,
                    routing_number: bank_dto.routing_number,
                    account_number: account_dto.account_number,
                    claims_account: bank_dto.claims_account,
                    adapter,
                })
            })
            .map_err(Into::into)
    }
}
