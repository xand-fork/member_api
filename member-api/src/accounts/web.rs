use crate::AccountDetails;

#[derive(Clone, Debug, Deserialize, Eq, PartialEq, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct ApiAccount {
    pub id: i32,
    pub bank_id: i32,
    pub short_name: String,
    pub bank_name: String,
    pub masked_account_number: String,
    pub routing_number: String,
}

impl ApiAccount {
    pub fn unresolvable() -> Self {
        ApiAccount {
            id: -1,
            bank_id: -1,
            short_name: "UNRESOLVABLE".to_string(),
            bank_name: "UNRESOLVABLE BANK".to_string(),
            masked_account_number: "N/A".to_string(),
            routing_number: "UNRESOLVABLE".to_string(),
        }
    }
}

impl From<AccountDetails> for ApiAccount {
    fn from(input: AccountDetails) -> Self {
        let masked_account_number = input.masked_account_number();
        ApiAccount {
            id: input.id,
            bank_id: input.bank_id,
            short_name: input.short_name,
            bank_name: input.bank_name,
            routing_number: input.routing_number,
            masked_account_number,
        }
    }
}
