use super::{
    repository::{
        errors::{AccountsRepositoryError, AccountsRepositoryResult},
        AccountsRepository,
    },
    web::ApiAccount,
    Account, AccountDetails, BankAccountConnectionData,
};
use crate::{accounts::repository::errors::AccountsRepositoryErrorSource, tests::TestDataBuilder};
use pseudo::Mock;

#[derive(Clone)]
pub struct MockAccountsRepository {
    pub mock_get_all: Mock<(), AccountsRepositoryResult<Vec<AccountDetails>>>,
    pub mock_get_by_account_number:
        Mock<(String, String), AccountsRepositoryResult<AccountDetails>>,
    pub mock_check_health: Mock<(), AccountsRepositoryResult<()>>,
}

impl Default for MockAccountsRepository {
    fn default() -> Self {
        MockAccountsRepository {
            mock_get_all: Mock::new(Err(AccountsRepositoryError::Unknown {
                msg: "MockAccountsRepository.get_all() not configured.".to_string(),
                source: AccountsRepositoryErrorSource::AccountsRepository,
            })),
            mock_get_by_account_number: Mock::new(Err(AccountsRepositoryError::Unknown {
                msg: "MockAccountsRepository.get_by_account_number() not configured.".to_string(),
                source: AccountsRepositoryErrorSource::AccountsRepository,
            })),
            mock_check_health: Mock::new(Err(AccountsRepositoryError::Unknown {
                msg: "MockAccountsRepository.check_health() not configured.".to_string(),
                source: AccountsRepositoryErrorSource::AccountsRepository,
            })),
        }
    }
}

impl TestDataBuilder for Account {
    fn build(index: usize) -> Self {
        let account_number = format!("{:0>12}", index + 1);
        let short_name = format!("name {}", index);

        Account {
            account_number,
            short_name,
            id: Some(index as i32),
            bank_id: (index * 2) as i32,
        }
    }
}

impl TestDataBuilder for AccountDetails {
    fn build(index: usize) -> Self {
        let short_name = format!("name {}", index);
        let account_number = (index + 1).to_account_number();
        let routing_number = (index + 1).to_routing_number();
        let bank_name = format!("bank {}", index);

        AccountDetails {
            routing_number,
            account_number,
            short_name,
            bank_name,
            id: index as i32,
            bank_id: (index * 2) as i32,
        }
    }
}

impl TestDataBuilder for ApiAccount {
    fn build(index: usize) -> Self {
        let short_name = format!("name {}", index);
        let truncated_account_number = format!("XXXXXXXXXX{:0>04}", index + 1);
        let routing_number = (index + 1).to_routing_number();
        let bank_name = format!("bank {}", index);

        ApiAccount {
            routing_number,
            masked_account_number: truncated_account_number,
            short_name,
            bank_name,
            id: index as i32,
            bank_id: (index * 2) as i32,
        }
    }
}

impl AccountsRepository for MockAccountsRepository {
    fn update(
        &self,
        _account: Account,
    ) -> crate::accounts::repository::errors::AccountsRepositoryResult<AccountDetails> {
        todo!()
    }

    fn get_by_account_number(
        &self,
        routing_number: &str,
        account_number: &str,
    ) -> AccountsRepositoryResult<AccountDetails> {
        self.mock_get_by_account_number
            .call((routing_number.to_string(), account_number.to_string()))
    }

    fn insert(
        &self,
        _: Account,
    ) -> crate::accounts::repository::errors::AccountsRepositoryResult<AccountDetails> {
        todo!()
    }

    fn remove(
        &self,
        _id: i32,
    ) -> crate::accounts::repository::errors::AccountsRepositoryResult<()> {
        todo!()
    }

    fn get(
        &self,
        _id: i32,
    ) -> crate::accounts::repository::errors::AccountsRepositoryResult<AccountDetails> {
        todo!()
    }

    fn get_all(
        &self,
    ) -> crate::accounts::repository::errors::AccountsRepositoryResult<Vec<AccountDetails>> {
        self.mock_get_all.call(())
    }

    fn get_bank_account_connection(
        &self,
        _id: i32,
    ) -> super::repository::errors::AccountsRepositoryResult<BankAccountConnectionData> {
        todo!()
    }

    fn get_single_per_bank(
        &self,
    ) -> super::repository::errors::AccountsRepositoryResult<Vec<AccountDetails>> {
        todo!()
    }

    fn check_health(&self) -> AccountsRepositoryResult<()> {
        self.mock_check_health.call(())
    }
}

pub trait AccountNumbers {
    fn to_routing_number(&self) -> String;
    fn to_account_number(&self) -> String;
}

impl AccountNumbers for u32 {
    fn to_routing_number(&self) -> String {
        format!("{:0>9}", self)
    }

    fn to_account_number(&self) -> String {
        format!("{:0>12}", self)
    }
}

impl AccountNumbers for i32 {
    fn to_routing_number(&self) -> String {
        format!("{:0>9}", self)
    }

    fn to_account_number(&self) -> String {
        format!("{:0>12}", self)
    }
}

impl AccountNumbers for usize {
    fn to_routing_number(&self) -> String {
        format!("{:0>9}", self)
    }

    fn to_account_number(&self) -> String {
        format!("{:0>12}", self)
    }
}
