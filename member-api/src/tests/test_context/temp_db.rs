use crate::database::connection_pool_builder::{ConnectionPoolBuilder, SqliteConnectionPool};
use std::path::Path;
use tempfile::TempDir;
use uuid::Uuid;

pub struct TestDatabase {
    /// When an instance of TempDir goes out of scope, the underlying directories are cleaned up.
    /// Store the tempdir in this struct, so it is in scope as long as this instance of `TestContext` is.
    tempdir: TempDir,
    conn_pool: SqliteConnectionPool,
}

impl TestDatabase {
    pub fn conn_pool(&self) -> SqliteConnectionPool {
        self.conn_pool.clone()
    }

    pub fn db_path(&self) -> &Path {
        self.tempdir.path()
    }
}

impl Default for TestDatabase {
    fn default() -> Self {
        let tempdir = tempfile::tempdir().unwrap();
        let db_file = format!("test_db_{}.sqlite", Uuid::new_v4());
        let db_path = tempdir.path().join(db_file);
        let db_path_str = db_path.to_str().unwrap().to_string();

        let conn_pool = ConnectionPoolBuilder::build(db_path_str).unwrap();
        Self { tempdir, conn_pool }
    }
}
