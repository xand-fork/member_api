use std::iter::*;
pub trait TestDataBuilder: Sized {
    fn build(index: usize) -> Self;

    fn build_with<F>(index: usize, mut modify: F) -> Self
    where
        F: FnMut(&mut Self, usize),
    {
        let mut result = Self::build(index);
        modify(&mut result, index);
        result
    }

    fn build_vec(count: usize) -> Vec<Self> {
        (0..count).map(Self::build).collect()
    }

    fn build_vec_with<F>(count: usize, mut modify: F) -> Vec<Self>
    where
        F: FnMut(&mut Self, usize),
    {
        (0..count)
            .map(|index| {
                let mut item = Self::build(index);
                modify(&mut item, index);
                item
            })
            .collect()
    }
}

#[cfg(test)]
pub mod tests {
    use super::TestDataBuilder;

    struct Widget {
        id: Option<usize>,
        name: String,
    }

    impl TestDataBuilder for Widget {
        fn build(index: usize) -> Self {
            Widget {
                id: Some(index),
                name: format!("name {}", index),
            }
        }
    }

    #[test]
    fn build_with() {
        let widget = Widget::build_with(1, |item, _index| {
            item.id = None;
        });

        assert_eq!(widget.id, None);
        assert_eq!(widget.name, "name 1".to_string());
    }

    #[test]
    fn build_vec_with() {
        let widgets = Widget::build_vec_with(2, |item, _index| {
            item.id = None;
        });

        assert_eq!(widgets[0].id, None);
        assert_eq!(widgets[1].id, None);
    }
}
