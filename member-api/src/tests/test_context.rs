// Do not care in tests
#![allow(clippy::future_not_send)]

use self::temp_secrets::TestSecrets;
use crate::tests::builder::TestDataBuilder;
use diesel::RunQueryDsl;

use crate::{
    accounts::{
        repository::{
            errors::AccountsRepositoryError, sqlite::SqliteAccountsRepository, AccountsRepository,
        },
        Account, AccountDetails,
    },
    banks::{
        api::BankApi,
        repository::{
            errors::BanksRepositoryError, sqlite::SqliteBanksRepository, BanksRepository,
        },
        testing::{MockBankApiFactory, MockXandBanksApi},
        Bank,
    },
    blockchain::test::MockBlockchain,
    config::ConfigWrapper,
    database,
    member::Member,
    tests::test_context::temp_db::TestDatabase,
    transaction::mocks::MockTransactionRepository,
};

use member_api_config::{MemberApiConfig, SecretStoreConfig, Url, XandApiConfig};
use std::str::FromStr;
use xand_address::Address;
use xand_secrets_local_file::LocalFileSecretStoreConfiguration;

const STARBUCKS: &str = "5Gph9iMER7s492Rm7i51oJkCr5snibMVWV6oqfEXEuxhv5xM";
const MACROHARD: &str = "5EgqcJ1sbFkUvySAmxc3bThRePvdmw3zCrXMjqYPaSSSzbkQ";

pub(crate) mod temp_db;
pub(crate) mod temp_secrets;

pub struct TestContext {
    /// Keep in scope as long as TestContext exists. Underlying db is cleaned up when variable is dropped.
    _testdb: TestDatabase,
    pub secrets: TestSecrets,

    pub starbucks: Address,
    pub macrohard: Address,

    pub xand_banks_api: MockXandBanksApi,

    pub bank_api_factory: MockBankApiFactory,
    pub config: MemberApiConfig,

    pub accounts_repository: SqliteAccountsRepository,
    pub banks_repository: SqliteBanksRepository,
    pub transactions_repository: MockTransactionRepository,

    pub member: Member,

    pub xand_api: MockBlockchain,
}

impl TestContext {
    pub fn drop_db_table(&self, table: &str) {
        let sql = format!("DROP TABLE {}", table);

        self._testdb
            .conn_pool()
            .connect_and_run(|connect| {
                diesel::sql_query(sql.to_string()).execute(connect).unwrap();
                Ok(())
            })
            .unwrap();
    }

    pub fn generate_banks(&self, count: usize) -> Result<Vec<Bank>, BanksRepositoryError> {
        let banks = Bank::build_vec(count);
        let mut results = vec![];
        for mut bank in banks {
            bank.id = None;
            let result = self.banks_repository.insert(bank)?;
            results.push(result);
        }
        Ok(results)
    }

    pub fn generate_accounts(
        &self,
        bank_id: i32,
        count: usize,
    ) -> Result<Vec<AccountDetails>, AccountsRepositoryError> {
        let accounts = Account::build_vec(count);
        let mut results = vec![];
        for mut account in accounts {
            account.id = None;
            account.bank_id = bank_id;
            let result = self.accounts_repository.insert(account)?;
            results.push(result);
        }

        Ok(results)
    }

    pub fn generate_banks_and_accounts(
        &self,
        count_of_banks: usize,
        accounts_per_bank: usize,
    ) -> (Vec<Bank>, Vec<AccountDetails>) {
        let banks = self.generate_banks(count_of_banks).unwrap();
        let mut accounts = vec![];
        for bank in banks.clone() {
            let results = self
                .generate_accounts(bank.id.unwrap(), accounts_per_bank)
                .unwrap();
            for account in results {
                accounts.push(account);
            }
        }

        (banks, accounts)
    }
}

fn construct_test_config() -> MemberApiConfig {
    let fake_validator = Url::parse("http://fake_validator").unwrap();
    MemberApiConfig {
        path: None,
        port: None,
        workers: None,
        database: "./data/test-member-api-db".into(),
        jwt_secret: None,
        xand_api: XandApiConfig {
            url: fake_validator,
            auth: None,
            timeout_seconds: None,
        },
        secret_store: SecretStoreConfig::LocalFile(LocalFileSecretStoreConfiguration {
            yaml_file_path: String::default(),
        }),
        address: Address::from_str("5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ").unwrap(),
    }
}

impl Default for TestContext {
    fn default() -> TestContext {
        let starbucks = STARBUCKS.parse().unwrap();
        let macrohard = MACROHARD.parse().unwrap();

        let mut config = construct_test_config();
        let xand_api = MockBlockchain::default();

        // TODO:
        // use in-memory database
        // https://docs.rs/sqlite/0.25.3/sqlite/
        //

        let testdb = TestDatabase::default();
        config.database = testdb.db_path().to_path_buf();

        let conn_pool = testdb.conn_pool();
        database::initialize(&conn_pool).unwrap();

        let secrets = TestSecrets::default();
        config.secret_store = SecretStoreConfig::LocalFile(LocalFileSecretStoreConfiguration {
            yaml_file_path: secrets.file_path().to_str().unwrap().into(),
        });
        let config_wrapper = ConfigWrapper::new(config.clone());
        let accounts_repository = SqliteAccountsRepository::new(conn_pool.clone());
        let banks_repository = SqliteBanksRepository::new(conn_pool);

        let transactions_repository = MockTransactionRepository::default();

        let member = config_wrapper
            .create_member(
                Box::new(xand_api.clone()),
                Box::new(banks_repository.clone()),
                Box::new(accounts_repository.clone()),
                Box::new(transactions_repository.clone()),
            )
            .unwrap();

        let bank_api_factory = MockBankApiFactory::default();
        let xand_banks_api = MockXandBanksApi::default();

        bank_api_factory.mock_create.return_ok(BankApi {
            adapter: Box::new(xand_banks_api.clone()),
        });

        TestContext {
            _testdb: testdb,
            secrets,

            starbucks,
            macrohard,

            bank_api_factory,
            config,
            accounts_repository,
            banks_repository,
            transactions_repository,

            member,

            xand_api,
            xand_banks_api,
        }
    }
}
