use crate::error;
use xand_address::Address;

#[logging_event]
pub enum LoggingEvent {
    Error(error::Error),
    ErrorWithMessage(String, error::Error),
    ErrorMessageOnly(String),

    StartedComponent { message: String },
    MemberAPIBankDispatcherFailed(String),

    RetrieveBankBalance(String),
    RetrieveBankBalanceSuccessful(String),
    FundClaims(String),
    ClaimsFundingSuccessful(String),

    RetrieveWalletBalance(Address),
    SendClaimsToWallet { from: Address, to: Address },
    SubmitCreateRequest(String),
    SubmitRedeem(String),
    RetrieveTxnStatus(String),

    RetrieveTxnHistory(String),

    KeyPairFilePathDoesNotExist(String),
    CreateUser,
    CreateUserFromKey,

    BanksRepositoryError(String),
    AccountsRepositoryError(String),
    Informational(String),
    Fatal(String),
}
