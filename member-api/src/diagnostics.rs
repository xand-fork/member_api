pub fn count_fds() -> usize {
    match std::env::consts::OS {
        "linux" => {
            let pid = std::process::id();
            let command = format!("/proc/{}/fd", pid);
            std::fs::read_dir(command).unwrap().count()
        }
        "macos" => {
            // would be nice to learn how to count fd's on mac but
            // it's not important right now.

            // https://doc.rust-lang.org/std/env/consts/constant.OS.html
            0
        }
        _ => 0,
    }
}

pub fn dump_fd_info(title: &str) {
    let fd_count = count_fds();
    log::debug!("Diagnostics: fd-count={}: {}", fd_count, title);
}
