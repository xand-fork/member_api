use crate::{
    accounts::{
        repository::{errors::AccountsRepositoryError, AccountsRepository},
        AccountDetails,
    },
    banks::repository::BanksRepository,
    blockchain::{Blockchain, Receipt},
    transaction::{TransactionRepository, *},
    Error, Result,
};
use xand_address::Address;
use xand_api_client::{BankAccountId, BankAccountInfo};

#[derive(Clone)]
pub struct Member {
    pub address: Address,
    pub xand_api: Box<dyn Blockchain>,
    pub accounts_repository: Box<dyn AccountsRepository>,
    pub banks_repository: Box<dyn BanksRepository>,
    pub transactions: Box<dyn TransactionRepository>,
}

impl Member {
    fn get_account_by_id(&self, account_id: i32) -> Result<AccountDetails> {
        self.accounts_repository
            .get(account_id)
            .map_err(|e| match e {
                AccountsRepositoryError::NotFound => Error::Validation {
                    message: format! {"Account id not found: {}", account_id},
                },
                _ => e.into(),
            })
    }

    pub async fn issue_create_request(&self, account_id: i32, amount: u64) -> Result<Receipt> {
        let account = self.get_account_by_id(account_id)?;

        let account_info = BankAccountInfo::Unencrypted(BankAccountId {
            routing_number: account.routing_number,
            account_number: account.account_number,
        });

        let request = crate::blockchain::CreateRequest {
            address: self.address.clone(),
            account: account_info,
            amount,
            reference: Reference::default(),
        };
        self.xand_api.issue_create_request(request).await
    }

    pub async fn issue_redeem_request(&self, account_id: i32, amount: u64) -> Result<Receipt> {
        let account = self.get_account_by_id(account_id)?;

        let account_info = BankAccountInfo::Unencrypted(BankAccountId {
            routing_number: account.routing_number,
            account_number: account.account_number,
        });

        let request = crate::blockchain::RedeemRequest {
            address: self.address.clone(),
            account: account_info,
            amount,
            reference: Reference::default(),
        };
        self.xand_api.redeem(request).await
    }

    pub async fn send_claims(&self, to: Address, amount: u64) -> Result<Receipt> {
        let request = crate::blockchain::SendClaimsRequest {
            address: self.address.clone(),
            to,
            amount,
            reference: Reference::default(),
        };
        self.xand_api.send_claims(request).await
    }

    pub async fn get_balance(&self) -> Result<u64> {
        self.xand_api.balance(self.address.clone()).await
    }

    pub async fn get_transactions(
        &self,
        page: Option<usize>,
        size: Option<usize>,
    ) -> Result<TransactionHistory> {
        let request = TransactionHistoryRequest {
            page: page.unwrap_or_default(),
            size: size.unwrap_or(250_000),
            address: self.address.clone(),
        };

        self.transactions.get_history(request).await
    }

    pub async fn get_transaction(&self, transaction_id: String) -> Result<Transaction> {
        self.transactions.get(transaction_id).await
    }
}
