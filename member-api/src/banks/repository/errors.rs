use crate::database::DatabaseError;
use thiserror::Error;

#[derive(Clone, Eq, PartialEq, Debug, Serialize, Deserialize, Error)]
pub enum BanksRepositoryErrorSource {
    #[error("Database")]
    InternalDatabase,
    #[error("Banks Repository")]
    BanksRepository,
}

#[derive(Clone, Eq, PartialEq, Debug, Serialize, Deserialize, Error)]
pub enum BanksRepositoryError {
    #[error("access failure: {msg:?} (Source: {source:?}")]
    Access {
        msg: String,
        source: BanksRepositoryErrorSource,
    },
    #[error("query error: {msg:?} (Source: {source:?}")]
    QueryError {
        msg: String,
        source: BanksRepositoryErrorSource,
    },
    #[error("not found")]
    NotFound,
    #[error("repository error: {msg:?} (Source: {source:?}")]
    Repository {
        msg: String,
        source: BanksRepositoryErrorSource,
    },
    #[error("validation error: {msg:?} (Source: {source:?}")]
    Validation {
        msg: String,
        source: BanksRepositoryErrorSource,
    },
    #[error("unknown error: {msg:?} (Source: {source:?}")]
    Unknown {
        msg: String,
        source: BanksRepositoryErrorSource,
    },
}
impl BanksRepositoryError {
    pub fn contains_internal_db_msg(&mut self) -> bool {
        match self.clone() {
            BanksRepositoryError::Access { source, msg: _msg }
            | BanksRepositoryError::Repository { source, msg: _msg }
            | BanksRepositoryError::Validation { source, msg: _msg }
            | BanksRepositoryError::QueryError { source, msg: _msg }
            | BanksRepositoryError::Unknown { source, msg: _msg } => match source {
                BanksRepositoryErrorSource::InternalDatabase => true,
                BanksRepositoryErrorSource::BanksRepository => false,
            },
            BanksRepositoryError::NotFound => false,
        }
    }
}

pub type BanksRepositoryResult<T> = Result<T, BanksRepositoryError>;

impl From<DatabaseError> for BanksRepositoryError {
    fn from(error: DatabaseError) -> Self {
        match error {
            DatabaseError::ConnectionError { source } => BanksRepositoryError::Access {
                msg: source.to_string(),
                source: BanksRepositoryErrorSource::InternalDatabase,
            },
            DatabaseError::MigrationError { source } => BanksRepositoryError::Repository {
                msg: source.to_string(),
                source: BanksRepositoryErrorSource::InternalDatabase,
            },
            DatabaseError::ParseError { source } => BanksRepositoryError::Repository {
                msg: source.to_string(),
                source: BanksRepositoryErrorSource::InternalDatabase,
            },
            DatabaseError::UnhandledError { source } => BanksRepositoryError::Repository {
                msg: source.to_string(),
                source: BanksRepositoryErrorSource::InternalDatabase,
            },
            DatabaseError::PathError {
                msg,
                is_internal_msg,
            } => BanksRepositoryError::Access {
                msg,
                source: if is_internal_msg {
                    BanksRepositoryErrorSource::InternalDatabase
                } else {
                    BanksRepositoryErrorSource::BanksRepository
                },
            },
            DatabaseError::ValidationError {
                msg,
                is_internal_msg,
            } => BanksRepositoryError::Validation {
                msg,
                source: if is_internal_msg {
                    BanksRepositoryErrorSource::InternalDatabase
                } else {
                    BanksRepositoryErrorSource::BanksRepository
                },
            },
            DatabaseError::QueryError { source } => BanksRepositoryError::QueryError {
                msg: source.to_string(),
                source: BanksRepositoryErrorSource::InternalDatabase,
            },
            DatabaseError::NotFound => BanksRepositoryError::NotFound,
        }
    }
}

impl From<BanksRepositoryError> for crate::Error {
    fn from(input: BanksRepositoryError) -> Self {
        match input {
            BanksRepositoryError::NotFound => crate::Error::NotFound {
                message: "Record not found".to_string(),
            },
            _ => crate::Error::Database {
                message: format!("{:?}", input),
            },
        }
    }
}
