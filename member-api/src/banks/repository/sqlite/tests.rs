use crate::{
    accounts::testing::AccountNumbers,
    banks::{
        repository::{
            errors::{BanksRepositoryError, BanksRepositoryErrorSource},
            BanksRepository,
        },
        Bank, BankAdapter,
    },
    tests::{TestContext, TestDataBuilder},
};

#[test]
fn sqlite_banks_repository_can_insert_and_get() {
    let ctx = TestContext::default();

    let bank = Bank::build_with(12, |bank, _| {
        bank.id = None;
    });

    let result = ctx.banks_repository.insert(bank.clone()).unwrap();
    assert_eq!(
        result,
        Bank {
            id: Some(1),
            name: bank.name,
            routing_number: bank.routing_number,
            claims_account: bank.claims_account,
            adapter: bank.adapter
        }
    );
}

#[test]
fn sqlite_banks_repository_can_insert_and_get_all() {
    let ctx = TestContext::default();

    let count: usize = 3;
    let banks = Bank::build_vec_with(3, |bank, _| {
        bank.id = None;
    });

    for bank in banks {
        ctx.banks_repository.insert(bank).unwrap();
    }

    let result = ctx.banks_repository.get_all().unwrap();

    assert_eq!(result.len(), count);
}

#[test]
fn sqlite_banks_repository_cannot_get_missing() {
    let ctx = TestContext::default();

    let error = ctx.banks_repository.get(0).unwrap_err();

    assert_eq!(error, BanksRepositoryError::NotFound);
}

#[test]
fn sqlite_banks_repository_can_delete() {
    let ctx = TestContext::default();

    let banks = ctx.generate_banks(1).unwrap();
    let bank = banks[0].clone();

    ctx.banks_repository.remove(bank.id.unwrap()).unwrap();
}

#[test]
fn sqlite_banks_repository_cannot_remove_nonexisting() {
    let ctx = TestContext::default();

    let error = ctx.banks_repository.remove(0).unwrap_err();

    assert_eq!(error, BanksRepositoryError::NotFound);
}

#[test]
fn sqlite_banks_repository_can_insert_and_replace_with_update() {
    let ctx = TestContext::default();

    let bank = Bank::build_with(12, |bank, _| {
        bank.id = None;
    });

    let bank = ctx.banks_repository.insert(bank).unwrap();

    let new_adapter = BankAdapter::build(1);

    let updated_bank = Bank {
        id: bank.id,
        name: "New Name".to_string(),
        routing_number: 1134.to_routing_number(),
        claims_account: 9999.to_account_number(),
        adapter: new_adapter,
    };

    let update_result = ctx.banks_repository.update(updated_bank.clone()).unwrap();

    assert_eq!(update_result, updated_bank);
}

#[test]
fn sqlite_banks_repository_cannot_use_incompatible_db() {
    let ctx = TestContext::default();

    ctx.drop_db_table("ACCOUNT");
    ctx.drop_db_table("BANK");

    let bank = Bank::build_with(12, |bank, _| {
        bank.id = None;
    });

    let banks_err = ctx.banks_repository.insert(bank).unwrap_err();

    assert_eq!(
        banks_err,
        BanksRepositoryError::Repository {
            msg: "no such table: bank".into(),
            source: BanksRepositoryErrorSource::InternalDatabase
        }
    );
}

#[test]
fn sqlite_banks_repository_can_get_all_empty() {
    let ctx = TestContext::default();

    let get_all_result = ctx.banks_repository.get_all().unwrap();
    assert_eq!(get_all_result.len(), 0);
}

#[test]
fn sqlite_banks_repository_cannot_duplicate_name() {
    let ctx = TestContext::default();

    let banks = Bank::build_vec_with(2, |bank, _| {
        bank.id = None;
        bank.name = "fredbob".to_string();
    });

    ctx.banks_repository.insert(banks[0].clone()).unwrap();

    let insert_error = ctx.banks_repository.insert(banks[1].clone()).unwrap_err();

    assert_eq!(
        insert_error,
        BanksRepositoryError::Validation {
            msg: "UniqueViolation \"UNIQUE constraint failed: bank.name\"".into(),
            source: BanksRepositoryErrorSource::InternalDatabase
        }
    );
}

#[test]
fn sqlite_banks_repository_cannot_duplicate_routing_number() {
    let ctx = TestContext::default();

    let banks = Bank::build_vec_with(2, |item, _| {
        item.id = None;
        item.routing_number = 999.to_routing_number();
    });

    ctx.banks_repository.insert(banks[0].clone()).unwrap();
    let insert_error = ctx.banks_repository.insert(banks[1].clone()).unwrap_err();

    assert_eq!(
        insert_error,
        BanksRepositoryError::Validation {
            msg: "UniqueViolation \"UNIQUE constraint failed: bank.routing_number\"".into(),
            source: BanksRepositoryErrorSource::InternalDatabase,
        }
    );
}
