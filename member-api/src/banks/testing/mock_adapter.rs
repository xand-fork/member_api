use async_trait::async_trait;
use pseudo::Mock;
use xand_banks::{
    date_range::DateRange,
    models::{account::TransferRequest, BankBalance, BankTransaction, BankTransferResponse},
    BankAdapter, Result,
};

#[derive(Clone, Debug)]
pub struct MockBankAdapter {
    pub balance: Mock<String, Result<BankBalance>>,
    pub transfer: Mock<(TransferRequest, Option<String>), Result<BankTransferResponse>>,
    pub history: Mock<(String, DateRange), Result<Vec<BankTransaction>>>,
}

impl MockBankAdapter {
    pub fn new(
        balance_res: Result<BankBalance>,
        transfer_res: Result<BankTransferResponse>,
        history_res: Result<Vec<BankTransaction>>,
    ) -> Self {
        Self {
            balance: Mock::new(balance_res),
            transfer: Mock::new(transfer_res),
            history: Mock::new(history_res),
        }
    }
}

impl Default for MockBankAdapter {
    fn default() -> Self {
        Self::new(
            Ok(BankBalance::default()),
            Ok(BankTransferResponse::default()),
            Ok(vec![]),
        )
    }
}

#[async_trait]
impl BankAdapter for MockBankAdapter {
    async fn balance(&self, account_id: &str) -> Result<BankBalance> {
        self.balance.call(account_id.to_string())
    }

    async fn transfer(
        &self,
        request: TransferRequest,
        correlation_id: Option<String>,
    ) -> Result<BankTransferResponse> {
        self.transfer.call((request, correlation_id))
    }

    async fn history(
        &self,
        account_id: &str,
        date_range: DateRange,
    ) -> Result<Vec<BankTransaction>> {
        self.history.call((account_id.to_string(), date_range))
    }

    fn memo_char_limit(&self) -> u32 {
        42
    }
}
