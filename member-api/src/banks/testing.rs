pub mod mock_adapter;

use pseudo::Mock;
use xand_banks::models::{account::TransferRequest, BankTransferResponse};

use crate::{accounts::testing::AccountNumbers, tests::TestDataBuilder};

use super::{
    api::{
        bank_adapter_factory::BankAdapterFactory, errors::XandBanksClientConstructionError, BankApi,
    },
    repository::{
        errors::{BanksRepositoryError, BanksRepositoryResult},
        BanksRepository,
    },
    Bank, BankAdapter, McbAdapterConfig, TreasuryPrimeAdapterConfig,
};
use crate::banks::repository::errors::BanksRepositoryErrorSource;

#[derive(Clone)]
pub struct MockBanksRepository {
    pub mock_check_health: Mock<(), BanksRepositoryResult<()>>,
}

impl Default for MockBanksRepository {
    fn default() -> Self {
        MockBanksRepository {
            mock_check_health: Mock::new(Err(BanksRepositoryError::Unknown {
                msg: "MockBanksRepository.check_health() not configured.".to_string(),
                source: BanksRepositoryErrorSource::BanksRepository,
            })),
        }
    }
}

#[async_trait::async_trait]
impl BanksRepository for MockBanksRepository {
    fn update(&self, _bank: Bank) -> crate::banks::repository::errors::BanksRepositoryResult<Bank> {
        todo!()
    }

    fn insert(&self, _: Bank) -> crate::banks::repository::errors::BanksRepositoryResult<Bank> {
        todo!()
    }

    fn remove(&self, _id: i32) -> BanksRepositoryResult<()> {
        todo!()
    }

    fn get(&self, _id: i32) -> BanksRepositoryResult<Bank> {
        todo!()
    }

    fn get_all(&self) -> BanksRepositoryResult<Vec<Bank>> {
        todo!()
    }

    fn check_health(&self) -> BanksRepositoryResult<()> {
        self.mock_check_health.call(())
    }
}

impl TestDataBuilder for BankAdapter {
    fn build(index: usize) -> Self {
        match index % 2 {
            0 => BankAdapter::Mcb(McbAdapterConfig::build(index)),
            _ => BankAdapter::TreasuryPrime(TreasuryPrimeAdapterConfig::build(index)),
        }
    }
}

impl TestDataBuilder for Bank {
    fn build(index: usize) -> Self {
        let routing_number = (index + 1).to_routing_number();
        let claims_account = (index + 100).to_account_number();
        let adapter = BankAdapter::build(index);
        Bank {
            id: Some(index as i32),
            name: format!("name {}", index),
            routing_number,
            adapter,
            claims_account,
        }
    }
}

impl TestDataBuilder for McbAdapterConfig {
    fn build(index: usize) -> McbAdapterConfig {
        McbAdapterConfig {
            url: format!("http://mcb:{}/api", 9000 + index).parse().unwrap(),
            secret_user_name: format!("secret user name {}", index),
            secret_password: format!("secret_password {}", index),
            secret_client_app_ident: format!("secret_client_app_ident {}", index),
            secret_organization_id: format!("secret_organization_id {}", index),
        }
    }
}

impl TestDataBuilder for TreasuryPrimeAdapterConfig {
    fn build(index: usize) -> TreasuryPrimeAdapterConfig {
        TreasuryPrimeAdapterConfig {
            url: format!("http://treasury-prime:{}", 10000 + index)
                .parse()
                .unwrap(),
            secret_api_key_id: format!("secret_api_key_id {}", index),
            secret_api_key_value: format!("secret_api_key_value {}", index),
        }
    }
}

#[derive(Clone)]
pub struct MockBankApiFactory {
    pub mock_create: Mock<BankAdapter, Result<BankApi, XandBanksClientConstructionError>>,
}

impl Default for MockBankApiFactory {
    fn default() -> Self {
        let error = XandBanksClientConstructionError {
            message: "MockBankApiFactory: create()".to_string(),
        };
        let mock_create = Mock::new(Err(error));

        MockBankApiFactory { mock_create }
    }
}

impl BankAdapterFactory for MockBankApiFactory {
    fn get_secrets(&self) -> std::sync::Arc<dyn xand_secrets::SecretKeyValueStore> {
        unimplemented!()
    }

    fn create(&self, input: BankAdapter) -> Result<BankApi, XandBanksClientConstructionError> {
        self.mock_create.call(input)
    }
}

impl Default for McbAdapterConfig {
    fn default() -> Self {
        McbAdapterConfig::build(1)
    }
}

impl Default for TreasuryPrimeAdapterConfig {
    fn default() -> Self {
        TreasuryPrimeAdapterConfig::build(1)
    }
}

#[derive(Clone)]
pub struct MockXandBanksApi {
    pub mock_get_balance: Mock<String, xand_banks::Result<xand_banks::models::BankBalance>>,
    pub mock_transfer:
        Mock<(TransferRequest, Option<String>), xand_banks::Result<BankTransferResponse>>,
}

impl Default for MockXandBanksApi {
    fn default() -> Self {
        let balance_error = Err(xand_banks::errors::XandBanksErrors::General {
            message: "MockXandBanksApi: no bank api behavior specified for balance()".to_string(),
        });

        let transfer_error = Err(xand_banks::errors::XandBanksErrors::General {
            message: "MockXandBanksApi: no bank api behavior specified for transfer()".to_string(),
        });

        MockXandBanksApi {
            mock_get_balance: Mock::new(balance_error),
            mock_transfer: Mock::new(transfer_error),
        }
    }
}

#[async_trait::async_trait]
impl xand_banks::BankAdapter for MockXandBanksApi {
    async fn balance(
        &self,
        account_number: &str,
    ) -> xand_banks::Result<xand_banks::models::BankBalance> {
        self.mock_get_balance.call(account_number.to_string())
    }

    async fn transfer(
        &self,
        request: xand_banks::models::account::TransferRequest,
        metadata: Option<String>,
    ) -> xand_banks::Result<xand_banks::models::BankTransferResponse> {
        self.mock_transfer.call((request, metadata))
    }

    async fn history(
        &self,
        _account_number: &str,
        _date_range: xand_banks::date_range::DateRange,
    ) -> xand_banks::Result<Vec<xand_banks::models::BankTransaction>> {
        todo!()
    }

    fn memo_char_limit(&self) -> u32 {
        todo!()
    }
}
