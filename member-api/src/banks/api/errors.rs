use std::fmt::Display;

#[derive(Clone, Debug, Serialize)]
pub struct XandBanksClientConstructionError {
    pub message: String,
}

impl Display for XandBanksClientConstructionError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.message)
    }
}

impl std::error::Error for XandBanksClientConstructionError {}

#[derive(Debug)]
pub struct BalanceError {
    pub message: String,
    pub inner: Box<dyn std::error::Error>,
}

impl Display for BalanceError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.message)
    }
}

impl std::error::Error for BalanceError {}

#[derive(Debug)]
pub struct FundClaimsError {
    pub message: String,
    pub inner: Box<dyn std::error::Error>,
}

impl Display for FundClaimsError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.message)
    }
}

impl std::error::Error for FundClaimsError {}
