use crate::database::{error::DieselConnError, DatabaseError};
use diesel::{
    r2d2::{self, ConnectionManager},
    SqliteConnection,
};

pub use sqlite_connection_pool::{Connection, SqliteConnectionPool};
mod sqlite_connection_pool;

/// Limit number of connections in shared connection pool
const SQLITE_MAX_CONNS: u32 = 1;

pub struct ConnectionPoolBuilder {}

impl ConnectionPoolBuilder {
    pub fn build(db_path: String) -> crate::Result<SqliteConnectionPool, DatabaseError> {
        let mut builder = r2d2::Builder::default();
        builder = builder.max_size(SQLITE_MAX_CONNS);

        let manager = ConnectionManager::<SqliteConnection>::new(db_path);

        let pool = builder
            // min_idle controls the minimum number of connections to have open at any time. We
            // want our app to be able to load even if the database is initially unavailable (if
            // it's an external service, connection issues or transient downtime are possible);
            // continued failures will be reported by the /health endpoint. So, we set the minimum
            // connections to zero, allowing it to build the pool freely without initially
            // connecting to the db at all.
            // Context:
            // https://gitlab.com/TransparentIncDevelopment/product/apps/member_api/-/merge_requests/103#note_428392524
            .min_idle(Some(0))
            .build(manager)
            .map_err(|e| DieselConnError::PoolBuild { msg: e.to_string() })?;
        Ok(pool.into())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn build_succeeds_if_database_file_not_exists() {
        // Given
        let db_filepath = "file://does-not-exist".to_string();

        // When
        let result = ConnectionPoolBuilder::build(db_filepath);

        // Then
        // the pool itself was created
        let _pool = result.unwrap();
    }
}
