// https://stackoverflow.com/questions/56853059/use-of-undeclared-type-or-module-when-using-diesels-belongs-to-attribute
use crate::{accounts::Account, database::schema::account};

#[derive(Clone, Debug, Deserialize, Eq, PartialEq, Serialize, Default, Insertable)]
#[serde(rename_all = "kebab-case")]
#[table_name = "account"]
pub struct NewBankAccount {
    pub short_name: String,
    pub account_number: String,
    pub bank_id: i32,
}

impl From<Account> for NewBankAccount {
    fn from(account: Account) -> Self {
        NewBankAccount {
            short_name: account.short_name,
            account_number: account.account_number,
            bank_id: account.bank_id,
        }
    }
}
