use crate::{accounts::Account, database::schema::account};

/// Allow update the short name and account number
/// we're not updating the id or moving the account between banks.
#[derive(Clone, Debug, Deserialize, Eq, PartialEq, Serialize, Default, Insertable, AsChangeset)]
#[serde(rename_all = "kebab-case")]
#[table_name = "account"]
pub struct UpdateBankAccount {
    pub short_name: String,
    pub account_number: String,
}

impl From<Account> for UpdateBankAccount {
    fn from(input: Account) -> Self {
        UpdateBankAccount {
            short_name: input.short_name,
            account_number: input.account_number,
        }
    }
}
