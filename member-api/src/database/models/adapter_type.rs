use crate::database::schema::adapter_type;

#[derive(Clone, Debug, Deserialize, Eq, PartialEq, Serialize, Default, Queryable, Identifiable)]
#[serde(rename_all = "kebab-case")]
#[table_name = "adapter_type"]
pub struct StoredAdapterType {
    pub id: i32,
    pub type_: String,
}
