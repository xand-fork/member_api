use crate::{
    banks::{Bank, BankAdapter},
    database::{models::bank::stored_bank::StoredBank, schema::bank},
};
pub mod stored_bank;

#[derive(Clone, Debug, Deserialize, Eq, PartialEq, Serialize, Default, Insertable, AsChangeset)]
#[serde(rename_all = "kebab-case")]
#[table_name = "bank"]
pub struct BankData {
    pub name: String,
    pub routing_number: String,
    pub claims_account: String,
    pub adapter_type_id: i32,
    pub url: String,
    pub mcb_secret_user_name: Option<String>,
    pub mcb_secret_password: Option<String>,
    pub mcb_secret_client_app_ident: Option<String>,
    pub mcb_secret_organization_id: Option<String>,
    pub tp_secret_api_key_id: Option<String>,
    pub tp_secret_api_key_value: Option<String>,
}

impl From<Bank> for BankData {
    fn from(input: Bank) -> Self {
        BankData {
            name: input.name.clone(),
            routing_number: input.routing_number.clone(),
            claims_account: input.claims_account.clone(),
            adapter_type_id: StoredBank::get_bank_adapter_id(&input.adapter),
            url: input.adapter.get_url().to_string(),
            mcb_secret_user_name: input.adapter.mcb_secret_user_name(),
            mcb_secret_password: input.adapter.mcb_secret_password(),
            mcb_secret_client_app_ident: input.adapter.mcb_secret_client_app_ident(),
            mcb_secret_organization_id: input.adapter.mcb_secret_organization_id(),
            tp_secret_api_key_id: input.adapter.tp_secret_api_key_id(),
            tp_secret_api_key_value: input.adapter.tp_secret_api_key_value(),
        }
    }
}

pub trait OptionalMcbAdapter {
    fn mcb_secret_user_name(&self) -> Option<String>;
    fn mcb_secret_password(&self) -> Option<String>;
    fn mcb_secret_client_app_ident(&self) -> Option<String>;
    fn mcb_secret_organization_id(&self) -> Option<String>;
}

impl OptionalMcbAdapter for BankAdapter {
    fn mcb_secret_user_name(&self) -> Option<String> {
        match self {
            BankAdapter::Mcb(adapter) => Some(adapter.secret_user_name.clone()),
            _ => None,
        }
    }

    fn mcb_secret_password(&self) -> Option<String> {
        match self {
            BankAdapter::Mcb(adapter) => Some(adapter.secret_password.clone()),
            _ => None,
        }
    }

    fn mcb_secret_client_app_ident(&self) -> Option<String> {
        match self {
            BankAdapter::Mcb(adapter) => Some(adapter.secret_client_app_ident.clone()),
            _ => None,
        }
    }

    fn mcb_secret_organization_id(&self) -> Option<String> {
        match self {
            BankAdapter::Mcb(adapter) => Some(adapter.secret_organization_id.clone()),
            _ => None,
        }
    }
}

pub trait OptionalTreasuryPrimeAdapter {
    fn tp_secret_api_key_id(&self) -> Option<String>;
    fn tp_secret_api_key_value(&self) -> Option<String>;
}

impl OptionalTreasuryPrimeAdapter for BankAdapter {
    fn tp_secret_api_key_id(&self) -> Option<String> {
        match self {
            BankAdapter::TreasuryPrime(adapter) => Some(adapter.secret_api_key_id.clone()),
            _ => None,
        }
    }

    fn tp_secret_api_key_value(&self) -> Option<String> {
        match self {
            BankAdapter::TreasuryPrime(adapter) => Some(adapter.secret_api_key_value.clone()),
            _ => None,
        }
    }
}
