use crate::database::{error::DieselConnError, DatabaseError};

use diesel::{
    r2d2::{ConnectionManager, Pool, PooledConnection},
    Connection as DieselConnection, SqliteConnection,
};

#[derive(Clone)]
pub struct SqliteConnectionPool {
    inner: Pool<ConnectionManager<SqliteConnection>>,
}

impl SqliteConnectionPool {
    /// Requests connection from self's pool for running closure.
    /// Ensures that connections are released to the Pool when queries are complete.
    pub fn connect_and_run<F, R>(&self, f: F) -> std::result::Result<R, DatabaseError>
    where
        F: FnOnce(&Connection) -> std::result::Result<R, DatabaseError>,
    {
        let conn = self
            .inner
            .get()
            .map_err(|e| DieselConnError::PoolBuild { msg: e.to_string() })?;

        conn.execute("PRAGMA foreign_keys = ON; PRAGMA journal_mode=WAL;")?;
        f(&conn)
    }
}

impl From<Pool<ConnectionManager<SqliteConnection>>> for SqliteConnectionPool {
    fn from(inner: Pool<ConnectionManager<SqliteConnection>>) -> Self {
        Self { inner }
    }
}

/// Type alias for simplifying references to underlying diesel type
pub type Connection = PooledConnection<ConnectionManager<diesel::SqliteConnection>>;
