use crate::database::connection_pool_builder::SqliteConnectionPool;
pub use error::DatabaseError;

pub mod connection_pool_builder;
pub(crate) mod error;
pub mod models;
pub mod schema;

embed_migrations!();

/// Initialize a database file by applying schema migrations to it, creating it if not exists
pub fn initialize(conn_pool: &SqliteConnectionPool) -> Result<(), DatabaseError> {
    conn_pool.connect_and_run(|connection| {
        embedded_migrations::run_with_output(connection, &mut std::io::stdout())?;
        Ok(())
    })?;

    Ok(())
}
