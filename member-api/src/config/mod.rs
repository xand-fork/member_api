use crate::{
    accounts::repository::AccountsRepository,
    banks::{api::bank_adapter_factory::BankAdapterFactory, repository::BanksRepository},
    blockchain::{xand_api_adapter::XandApiAdapter, Blockchain},
    health::{
        accounts_repository::AccountsRepositoryHealthService,
        banks::{BankHealthService, BanksServiceCreationError},
        banks_repository::BanksRepositoryHealthService,
        invalid::InvalidService,
        secret_store::SecretStoreHealthChecker,
        xand_api::XandApiClientHealthChecker,
        Service,
    },
    logging::LoggingEvent,
    member::Member,
    primitives::Result,
    transaction::{mapper::TransactionMapper, TransactionRepository, TransactionRepositoryImpl},
};
use cfg::{Config, Environment, File as CfgFile};
use futures::executor::block_on;
use glob::glob;
use member_api_config::{MemberApiConfig, SecretStoreConfig};
use snafu::Snafu;
use std::{path::PathBuf, rc::Rc, result::Result as StdResult, sync::Arc};
use xand_address::Address;
use xand_api_client::{ReconnectingXandApiClient, XandApiClientTrait};
use xand_banks::errors::XandBanksErrors;
use xand_secrets::{ExposeSecret, Secret, SecretKeyValueStore};
use xand_secrets_local_file::LocalFileSecretKeyValueStore;
use xand_secrets_vault::{VaultSecretKeyValueStore, VaultSecretStoreCreationError};

#[cfg(test)]
mod tests;

/// This is used throughout member-api to create instances of Clients, Repositories,
/// Services, and more. For Debug output, it logs a secret as `"[REDACTED String]"`.
/// It intentionally does not impl Serialize to avoid unintentional
/// serialization of secrets it stores. Build the inner struct directly
/// if you need to serialize and deserialize config data.
#[derive(Clone, Debug)]
pub struct ConfigWrapper(MemberApiConfig);

impl ConfigWrapper {
    pub fn new(member_api_cfg: MemberApiConfig) -> Self {
        ConfigWrapper(member_api_cfg)
    }

    pub fn port(&self) -> &Option<u64> {
        &self.0.port
    }
    pub fn workers(&self) -> &Option<usize> {
        &self.0.workers
    }
    pub fn database(&self) -> &PathBuf {
        &self.0.database
    }

    /// Use the configuration-provided secret key to return the jwt secret value from the Member
    /// API's secret store.
    pub fn jwt_secret(&self) -> Result<Option<String>> {
        match &self.0.jwt_secret {
            Some(secret) => Ok(Some(
                self.get_secret_config_value(secret)?
                    .expose_secret()
                    .clone(),
            )),
            None => Ok(None),
        }
    }

    pub fn address(&self) -> &Address {
        &self.0.address
    }

    fn get_config_path(
        paths: Vec<&str>,
    ) -> std::result::Result<&std::path::Path, ConfigurationError> {
        let config_path = paths
            .iter()
            .copied()
            .map(std::path::Path::new)
            .find(|p| p.exists());

        match config_path {
            Some(value) => {
                if value.is_dir() {
                    Ok(value)
                } else {
                    Err(ConfigurationError::ConfigPathMustBeADirectory {
                        path: value.to_str().unwrap().to_string(),
                    })
                }
            }
            None => Err(ConfigurationError::NoConfigFoldersFound {
                paths: paths.iter().copied().map(|s| s.to_string()).collect(),
            }),
        }
    }

    pub fn load(paths: Vec<&str>) -> std::result::Result<ConfigWrapper, ConfigurationError> {
        let config_path = Self::get_config_path(paths)?;
        let mut config = Config::default();

        let absolute_path = std::fs::canonicalize(config_path)
            .expect("Getting an absolute path from a relative one should never fail.")
            .to_str()
            .expect("Getting a string from the config path should never fail.")
            .to_string();

        info!(LoggingEvent::Informational(format!(
            "Loading config files from {:?}",
            &absolute_path
        )));

        let config_files = glob(&format!("{}/*.toml", absolute_path))?
            .chain(glob(&format!("{}/*.yaml", absolute_path))?)
            .chain(glob(&format!("{}/*.yml", absolute_path))?)
            .chain(glob(&format!("{}/*.json", absolute_path))?)
            .map(|path| CfgFile::from(path.unwrap()))
            .collect::<Vec<_>>();

        if config_files.is_empty() {
            return Err(ConfigurationError::NoConfigFilesFound {
                path: absolute_path,
            });
        }

        config.merge(config_files)?;
        config.merge(Environment::with_prefix("member-api").separator("_"))?;

        let mut cfg: MemberApiConfig =
            serde_path_to_error::deserialize(config).map_err(|err| ConfigurationError::Format {
                message: err.into_inner().to_string(),
            })?;

        cfg.path = Some(absolute_path);

        Ok(ConfigWrapper(cfg))
    }

    pub(crate) fn create_secret_store(
        &self,
    ) -> StdResult<Arc<dyn SecretKeyValueStore>, SecretStoreCreationError> {
        ::log::info!("Creating secret store");
        match &self.0.secret_store {
            SecretStoreConfig::Vault(config) => Ok(Arc::new(
                VaultSecretKeyValueStore::create_from_config(config.clone())?,
            )),
            SecretStoreConfig::LocalFile(config) => Ok(Arc::new(
                LocalFileSecretKeyValueStore::create_from_config(config.clone()),
            )),
        }
    }

    fn get_xand_api_service(&self, xand_api: Box<dyn Blockchain>) -> Box<dyn Service> {
        Box::new(XandApiClientHealthChecker { xand_api })
    }

    fn get_repository_services(
        &self,
        banks_repository: Box<dyn BanksRepository>,
        accounts_repository: Box<dyn AccountsRepository>,
    ) -> Vec<Box<dyn Service>> {
        vec![
            Box::new(BanksRepositoryHealthService::new(banks_repository)),
            Box::new(AccountsRepositoryHealthService::new(accounts_repository)),
        ]
    }

    fn log_secret_store_error(error: SecretStoreCreationError) {
        error!(LoggingEvent::ErrorWithMessage(
            String::from("Failed to create HealthService due to secret store connection error"),
            error.into()
        ));
    }

    fn log_bank_service_error(error: BanksServiceCreationError) {
        error!(LoggingEvent::ErrorWithMessage(
            String::from("Failed to initialize banks"),
            error.into()
        ));
    }

    pub fn get_services(
        &self,
        bank_adapters: &dyn BankAdapterFactory,
        banks_repository: Box<dyn BanksRepository>,
        accounts_repository: Box<dyn AccountsRepository>,
        xand_api: Box<dyn Blockchain>,
    ) -> Vec<Box<dyn Service>> {
        let mut services = vec![self.get_xand_api_service(xand_api)];
        services.extend(
            self.get_repository_services(banks_repository.clone(), accounts_repository.clone()),
        );

        match self.create_secret_store() {
            Ok(secret_store) => {
                services.push(Box::new(SecretStoreHealthChecker::SecretStore(
                    secret_store.clone(),
                )));
            }
            Err(error) => {
                Self::log_secret_store_error(error);
                services.push(Box::new(SecretStoreHealthChecker::FailedToCreate));
            }
        }

        match BankHealthService::create_services(
            banks_repository.as_ref(),
            accounts_repository.as_ref(),
            bank_adapters,
        ) {
            Ok(mut bank_services) => services.append(&mut bank_services),
            Err(e) => {
                Self::log_bank_service_error(e);
                services.push(Box::new(InvalidService::new(
                    "All banks".into(),
                    "Unable to check bank health.".into(),
                )));
            }
        }

        services
    }

    pub fn create_blockchain(&self, xand_client: XandApiClientRef) -> Box<dyn Blockchain> {
        Box::new(XandApiAdapter::new(xand_client))
    }

    pub fn create_xand_client(&self) -> Result<Box<dyn XandApiClientTrait>> {
        let jwt_key = self.0.xand_api.auth.as_ref().and_then(|x| x.jwt.as_ref());

        let timeout = self.0.xand_api.timeout_seconds.unwrap_or(30);

        let result: ReconnectingXandApiClient = jwt_key.map_or_else(
            || {
                let api: ReconnectingXandApiClient =
                    block_on(ReconnectingXandApiClient::stub(self.0.xand_api.url.clone()));
                Ok(api)
            },
            |jwt_key: &String| -> Result<ReconnectingXandApiClient> {
                let jwt = self.get_secret_config_value(jwt_key)?;

                Ok(ReconnectingXandApiClient::stub_jwt(
                    self.0.xand_api.url.clone(),
                    jwt.expose_secret().clone(),
                ))
            },
        )?;

        let duration = std::time::Duration::from_secs(timeout);
        let result = result.with_timeout(duration);

        let duration = std::time::Duration::from_secs(timeout);
        let result = result.with_timeout(duration);

        Ok(Box::new(result))
    }

    pub fn create_member(
        &self,
        xand_api: Box<dyn Blockchain>,
        banks_repository: Box<dyn BanksRepository>,
        accounts_repository: Box<dyn AccountsRepository>,
        transactions: Box<dyn TransactionRepository>,
    ) -> Result<Member> {
        Ok(Member {
            address: self.0.address.clone(),
            xand_api,
            banks_repository,
            accounts_repository,
            transactions: transactions.clone(),
        })
    }

    pub fn create_transaction_repository(
        &self,
        xand_api_client: XandApiClientRef,
        accounts: Box<dyn AccountsRepository>,
    ) -> Box<dyn TransactionRepository> {
        let mapper = TransactionMapper { accounts };
        let repository = TransactionRepositoryImpl {
            client: xand_api_client,
            mapper,
        };
        Box::new(repository)
    }

    pub(crate) fn get_secret_config_value(&self, key: &str) -> Result<Secret<String>> {
        let secret_store = self.create_secret_store()?;

        block_on(secret_store.read(key)).map_err(Into::into)
    }
}

#[derive(Clone, Debug, Serialize, Snafu)]
pub(crate) enum SecretStoreCreationError {
    #[snafu(display("{}", source))]
    #[serde(serialize_with = "xand_utils::snafu_extensions::debug_serialize")]
    Vault {
        source: Rc<VaultSecretStoreCreationError>,
    },
}

impl From<VaultSecretStoreCreationError> for SecretStoreCreationError {
    fn from(source: VaultSecretStoreCreationError) -> Self {
        SecretStoreCreationError::Vault {
            source: Rc::new(source),
        }
    }
}

#[derive(Debug, Snafu)]
pub enum ConfigurationError {
    #[snafu(display("{}", account_id))]
    OrphanedBankAccount {
        account_id: String,
    },

    #[snafu(display("key: {}; path: {}", title, path))]
    FolderNotFound {
        title: String,
        path: String,
    },

    #[snafu(display("{}", message))]
    Format {
        message: String,
    },

    #[snafu(display("{:?}", paths))]
    NoConfigFoldersFound {
        paths: Vec<String>,
    },

    #[snafu(display("{:?}", path))]
    ConfigPathMustBeADirectory {
        path: String,
    },

    #[snafu(display("{:?}", path))]
    NoConfigFilesFound {
        path: String,
    },

    #[snafu(display("{}", source))]
    XandBanks {
        source: XandBanksErrors,
    },

    InvalidConfigFileSearchPattern,

    #[snafu(display("Could not load config file(s): {}", source))]
    ParsingError {
        source: cfg::ConfigError,
    },

    #[snafu(display("Field required and must not be blank: {}", field))]
    RequiredField {
        field: String,
    },

    #[snafu(display("Multiple accounts for same bank using same account number is not allowed. Account number: '{}'", account_number))]
    DuplicateAccountNumber {
        account_number: String,
    },
}

impl From<glob::PatternError> for ConfigurationError {
    fn from(_input: glob::PatternError) -> Self {
        ConfigurationError::InvalidConfigFileSearchPattern
    }
}

impl From<cfg::ConfigError> for ConfigurationError {
    fn from(input: cfg::ConfigError) -> Self {
        ConfigurationError::ParsingError { source: input }
    }
}

impl From<XandBanksErrors> for ConfigurationError {
    fn from(input: XandBanksErrors) -> Self {
        ConfigurationError::XandBanks { source: input }
    }
}

type XandApiClientRef = Arc<dyn XandApiClientTrait>;
