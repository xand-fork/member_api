use std::convert::TryFrom;
use xand_api_client::{CorrelationId, Transaction};

/// `Nonce` does not have a valid default implementation, so this function hard-codes
/// the empty string that `member_api::Transaction`'s `correlation_id` field expects in the null case
pub fn correlation_id_from_transaction(input: &Transaction) -> String {
    CorrelationId::try_from(input.get_correlation_id().unwrap_or_default())
        .map_or_else(|_| String::new(), |n| n.to_string())
}
