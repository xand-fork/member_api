use crate::transaction::{
    Transaction, TransactionHistory, TransactionHistoryRequest, TransactionRepository,
};
use pseudo::Mock;

#[derive(Clone)]
pub struct MockTransactionRepository {
    pub all_transactions: Mock<(), Result<Vec<Transaction>, crate::Error>>,
}

#[async_trait::async_trait]
impl TransactionRepository for MockTransactionRepository {
    async fn get(&self, id: String) -> Result<Transaction, crate::Error> {
        let transactions = self.all_transactions.call(())?;

        let results: Vec<Transaction> = transactions
            .into_iter()
            .filter(|t| t.transaction_id == id)
            .collect();
        if results.is_empty() {
            return Err(crate::Error::NotFound {
                message: format!("No transaction with id {} found.", id),
            });
        }

        Ok(results[0].clone())
    }

    async fn get_history(
        &self,
        _request: TransactionHistoryRequest,
    ) -> Result<TransactionHistory, crate::Error> {
        let transactions = self.all_transactions.call(())?;
        let result = TransactionHistory {
            total: transactions.len(),
            transactions,
        };
        Ok(result)
    }
}

impl Default for MockTransactionRepository {
    fn default() -> MockTransactionRepository {
        let error = crate::Error::Generic {
            message: "no mock behavior defined".to_string(),
        };

        MockTransactionRepository {
            all_transactions: Mock::new(Err(error)),
        }
    }
}
