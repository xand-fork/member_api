use super::correlation_id::correlation_id_from_transaction;
use crate::{
    accounts::{web::ApiAccount, AccountDetails},
    transaction::{Status, Transaction, TransactionHistory, TransactionState},
};
use snafu::Snafu;
use xand_api_client::{
    BankAccountInfo, CreateRequestCompletion, PendingCreateRequest, PendingRedeemRequest,
    RedeemRequestCompletion, TransactionHistoryPage, TransactionIdError, XandTransaction,
};

#[derive(Clone)]
pub struct TransactionMapper {
    pub accounts: Box<dyn crate::accounts::repository::AccountsRepository>,
}

#[allow(clippy::large_enum_variant)]
#[derive(Clone, Debug, Serialize, Snafu)]
pub enum TransactionMappingError {
    BankRepositoryErrors { message: String },

    TransactionId { source: TransactionIdError },

    SerializationError { value: String, message: String },

    BankDecryptionError,
}

impl From<TransactionMappingError> for crate::Error {
    fn from(input: TransactionMappingError) -> Self {
        crate::Error::TransactionMapping { source: input }
    }
}

impl TransactionMapper {
    // NOTE: Isn't dealing with pagination, which remains unimplemented
    pub fn map_from_xand_history(
        &self,
        input: &TransactionHistoryPage,
    ) -> Result<TransactionHistory, TransactionMappingError> {
        let accounts =
            self.accounts
                .get_all()
                .map_err(|e| TransactionMappingError::BankRepositoryErrors {
                    message: format!("{}", e),
                })?;

        let transactions = input
            .data
            .clone()
            .into_iter()
            .map(|x| {
                let bank_account = self.find_matching_account(&x, &accounts).map(|e| {
                    let result: ApiAccount = e.into();
                    result
                });

                self.map_with_account(&x, bank_account)
            })
            .collect();

        Ok(TransactionHistory {
            total: input.total as usize,
            transactions,
        })
    }

    fn find_matching_account(
        &self,
        input: &xand_api_client::Transaction,
        accounts: &[AccountDetails],
    ) -> Option<AccountDetails> {
        if let Some(account) = input.get_bank_info() {
            if let BankAccountInfo::Unencrypted(id) = account {
                let search = accounts.iter().find(|account| {
                    account.routing_number == id.routing_number
                        && account.account_number == id.account_number
                });

                search
                    .cloned()
                    .or_else(|| Some(AccountDetails::unresolvable()))
            } else {
                Some(AccountDetails::unresolvable())
            }
        } else {
            None
        }
    }

    fn map_with_account(
        &self,
        input: &xand_api_client::Transaction,
        bank_account: Option<ApiAccount>,
    ) -> Transaction {
        let amount_in_minor_unit = input.get_amount().unwrap_or_default();
        let confirmation_id = input
            .get_confirmation_id()
            .map_or_else(String::new, |item| item.to_string());
        let cancellation_id = input
            .get_cancellation_id()
            .map_or_else(String::new, |item| item.to_string());

        let status = input.get_transaction_status();
        let correlation_id = correlation_id_from_transaction(input);

        Transaction {
            transaction_id: input.transaction_id.to_string(),
            operation: input.txn.clone().into(),
            signer_address: input.signer_address.to_string(),
            amount_in_minor_unit,
            correlation_id,
            destination_address: input
                .get_destination_addr()
                .as_ref()
                .map(ToString::to_string)
                .unwrap_or_default(),
            datetime: input.timestamp,
            status,
            bank_account,
            confirmation_id,
            cancellation_id,
        }
    }

    pub fn map_from_xand_model(&self, input: &xand_api_client::Transaction) -> Transaction {
        let bank_account = self
            .get_bank_account(input)
            .unwrap_or_else(|_| Some(ApiAccount::unresolvable()));

        self.map_with_account(input, bank_account)
    }

    fn get_bank_account(
        &self,
        input: &xand_api_client::Transaction,
    ) -> Result<Option<ApiAccount>, TransactionMappingError> {
        if let Some(account) = input.get_bank_info() {
            let id = match account {
                BankAccountInfo::Unencrypted(acct) => Ok(acct),
                _ => Err(TransactionMappingError::BankDecryptionError),
            }?;

            let bank_account = self
                .accounts
                .get_by_account_number(&id.routing_number, &id.account_number)
                .map_err(|e| TransactionMappingError::BankRepositoryErrors {
                    message: format!(
                        "Error retrieving account for routing/account {}/{}: {}",
                        id.routing_number, id.account_number, e
                    ),
                })?;
            Ok(Some(bank_account.into()))
        } else {
            Ok(None)
        }
    }
}

pub trait TransactionMapping {
    fn get_confirmation_id(&self) -> Option<xand_api_client::TransactionId>;
    fn get_cancellation_id(&self) -> Option<xand_api_client::TransactionId>;
    fn get_transaction_status(&self) -> Status;
    fn get_transaction_state(&self) -> TransactionState;
    fn is_confirmed(&self) -> bool;
    fn is_cancelled(&self) -> bool;
    fn requires_completing_transaction(&self) -> bool;
}

impl TransactionMapping for xand_api_client::Transaction {
    fn requires_completing_transaction(&self) -> bool {
        matches!(
            self.txn,
            XandTransaction::CreateRequest(..) | XandTransaction::RedeemRequest(..)
        )
    }

    fn get_cancellation_id(&self) -> Option<xand_api_client::TransactionId> {
        match self.txn.clone() {
            XandTransaction::CreateRequest(PendingCreateRequest {
                completing_transaction: Some(CreateRequestCompletion::Cancellation(transaction_id)),
                ..
            }) => Some(transaction_id),
            XandTransaction::CreateRequest(PendingCreateRequest {
                completing_transaction: Some(CreateRequestCompletion::Expiration(transaction_id)),
                ..
            }) => Some(transaction_id),
            XandTransaction::RedeemRequest(PendingRedeemRequest {
                completing_transaction: Some(RedeemRequestCompletion::Cancellation(transaction_id)),
                ..
            }) => Some(transaction_id),
            _ => None,
        }
    }

    fn get_confirmation_id(&self) -> Option<xand_api_client::TransactionId> {
        match self.txn.clone() {
            XandTransaction::CreateRequest(PendingCreateRequest {
                completing_transaction: Some(CreateRequestCompletion::Confirmation(transaction_id)),
                ..
            }) => Some(transaction_id),
            XandTransaction::RedeemRequest(PendingRedeemRequest {
                completing_transaction: Some(RedeemRequestCompletion::Confirmation(transaction_id)),
                ..
            }) => Some(transaction_id),
            _ => None,
        }
    }

    fn get_transaction_state(&self) -> TransactionState {
        match self.status {
            xand_api_client::TransactionStatus::Committed
            | xand_api_client::TransactionStatus::Finalized => {
                if self.is_confirmed() {
                    TransactionState::Confirmed
                } else if self.is_cancelled() {
                    TransactionState::Cancelled
                } else {
                    TransactionState::Pending
                }
            }
            xand_api_client::TransactionStatus::Invalid(..) => TransactionState::Invalid,
            xand_api_client::TransactionStatus::Pending => TransactionState::Pending,
            xand_api_client::TransactionStatus::Unknown => TransactionState::Unknown,
        }
    }

    fn is_confirmed(&self) -> bool {
        if self.requires_completing_transaction() {
            self.get_confirmation_id().is_some()
        } else {
            true
        }
    }

    fn is_cancelled(&self) -> bool {
        self.get_cancellation_id().is_some()
    }

    fn get_transaction_status(&self) -> Status {
        let state = self.get_transaction_state();
        let details = match self.status.clone() {
            xand_api_client::TransactionStatus::Invalid(details) => Some(details),
            _ => None,
        };

        Status { state, details }
    }
}
