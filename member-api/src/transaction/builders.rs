use super::*;
use crate::tests::*;
use chrono::Duration;

impl TestDataBuilder for TransactionType {
    fn build(index: usize) -> Self {
        match index % 3 {
            1 => TransactionType::Create,
            2 => TransactionType::CreateRequest,
            _ => TransactionType::Payment,
        }
    }
}

impl TestDataBuilder for Status {
    fn build(index: usize) -> Self {
        let state = TransactionState::build(index);

        let details = match index % 2 {
            1 => Some(format!("transaction state details {}", index)),
            _ => None,
        };

        Status { state, details }
    }
}

impl TestDataBuilder for TransactionState {
    fn build(index: usize) -> Self {
        match index % 5 {
            1 => TransactionState::Unknown,
            2 => TransactionState::Pending,
            3 => TransactionState::Confirmed,
            4 => TransactionState::Invalid,
            _ => TransactionState::Cancelled,
        }
    }
}

impl TestDataBuilder for Transaction {
    fn build(index: usize) -> Self {
        let bank_account = match index % 2 {
            1 => Some(ApiAccount::build(index)),
            _ => None,
        };

        let confirmation_id = match index % 2 {
            1 => format!("confirmation: {}", index),
            _ => "".to_string(),
        };

        let cancellation_id = match index % 2 {
            1 => "".to_string(),
            _ => format!("cancellation: {}", index),
        };

        let datetime: DateTime<Utc> = Utc::now() + Duration::days(137);
        let transaction_id = format!("{:x}", index);

        let destination_address = format!("destination: {}", index);
        let correlation_id = format!("correlation_id: {}", index);
        let operation = TransactionType::build(index);
        let signer_address = "self".to_string();
        let status = Status::build(index);

        Transaction {
            amount_in_minor_unit: index as u64,
            bank_account,
            cancellation_id,
            confirmation_id,
            datetime,
            destination_address,
            correlation_id,
            operation,
            signer_address,
            status,
            transaction_id,
        }
    }
}
