# Member API

This API will be deployed and consumed by an **Member** of the XAND Network.

The current swagger documentation can be found [here](./content/swagger/swagger.yaml).

## Versioning

This repo is home to two crates: `member-api` and `member-api-config`. To avoid
confusion over the relationship between configuration and application versions,
they share the same crate version. Note that the configuration type is part of
the public API and thus breaking changes to it require a major version bump of
both.

The CI will, when publishing the crates, automatically set the version of
`member-api-config` to match `member-api` to simplify the developer
workflow.

**When making changes to either crate, you should bump the version in
`member-api/Cargo.toml` as needed, and leave the other crate's version
unchanged.**

This repo also contains a TypeScript client. When the CI publishes the
client, it will also automatically set the `client` version to match
`member-api`. The version specified in `client/package.json` should
remain unchanged.

## Dev Setup
This repo uses diesel and Sqlite. Run the `deps.sh` script to get those installed on your system.

## Configuration

On startup the Member API will look in the configuration directory (can be specified on the CLI or defaults to `config`)
and merge all files found there into its overall configuration.

The Member API will fail startup if certain required configuration elements have not been set.

Further documentation on specific configuration settings can be found in our [user docs](./docs/user/README.md)

### JWT configuration

The member-api supports securing the connection via JWT. To enable this authentication, follow the [instructions](./docs/user/README.md#jwt-configuration).

For a simpler experience, run the following:

```bash
cargo run -- jwt --company TPFS
```

After you've chosen your secret and generated its corresponding token you can issue a curl request against the member-api server.

If JWT is enabled, any curl requests without the token will be rejected with a 401 Unauthorized HTTP response.

To test that your generated token is valid, you can issue a curl request with it included in the header.

`curl -H 'Accept: application/json' -H "Authorization: Bearer ${TOKEN}" -I ${MEMBER_API_SERVER_URL}`

An accepted token here will result in a 200 OK response along with an HTML dump of the member-api homepage.


### Database Scripts

Create the db and run migrations:
```bash
diesel setup --database-url="{DATABASE_URL}"
```

To run migrations manually: 
```bash
diesel migration run --database-url="{DATABASE_URL}"
```

`--database-url=*` can be omitted if `DATABASE_URL` env var is set.

To create a migration: 
```bash
diesel migration generate {name}
```

### Data types
This crate depends on types defined in `xand-api-client`, as well as other canonical crates. 

It should not depend on types from thermite-related crates such as `xand-models`. 

## Publishing

### Prerelease/Beta Versions

Prereleases should be used to test out a concept in a different docker image (not to be used for final 
releases). You can manually push a prerelease/beta version for a branch by finding the corresponding pipeline 
in gitlab and clicking the play button next to the `publish-beta` job. Prereleases will also be automatically 
published as merges occur to master.

### Publishing a version change to release

The versioning of the crate should be maintained via [semantic versioning](https://semver.org/) and
there are helper scripts to help update the version in the Cargo.toml and Cargo.lock files.
This version change can happen on your branch, hopefully right before the MR is approved to be merged.

Use one of the following:
* `make publish-patch`
* `make publish-minor`
* `make publish-major`

Publishing the change should be done after an MR has merged to master. Once merged, pull from master and 
rebase, then tag a release using the script helper.

```shell
git checkout master
git pull
make tag-release
git push --tags
```


