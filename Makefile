# PUBLISHING =====================

tag-release:
	./.ci/tag_release.sh

publish-patch:
	./.ci/prepare_release.sh patch

publish-minor:
	./.ci/prepare_release.sh minor

publish-major:
	./.ci/prepare_release.sh major
