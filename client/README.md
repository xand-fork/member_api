# Generated member-api typescript client

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Dependencies](#dependencies)
- [Generated client](#generated-client)
- [Enhanced member-api typescript client](#enhanced-member-api-typescript-client)
  - [Dependencies](#dependencies-1)
  - [Workflows](#workflows)
  - [Getting started](#getting-started)
  - [Errors](#errors)
  - [Polling timeout](#polling-timeout)
    - [Polling configuration](#polling-configuration)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->


## Dependencies

In order to build, you must have [`openapi-generator`](https://github.com/OpenAPITools/openapi-generator) 
installed. This is an npm package which depends on java under the covers.

    sudo apt-get install default-jdk
    npm install -g openapi-generator

## Generated client
The Member API generated client is defined in [SwaggerDoc](`/member-api/content/swagger/Original.yaml).

If the SwaggerDoc file should change, you can regenerate the 
client by running:

    yarn build 
    
## Dependencies

This library depends on the member API generated client code, for more info see [Generated Client](#generated-client).

The library requires configured instances of the wallets, banks and transactions APIs. 

## Workflows 

The [WorkflowApi](../client/src/workflows) library is a wrapper around the Member-API typescript client. 

It enhances the client by implementing these commonly used methods: Create Request, Redeem, and Polling for transaction confirmation

It throws typed exceptions and provides detailed error messages that are useful for end user messaging. 


1. **Create Request Workflow**
    * Makes a create request
    * Transfers money to claims account
    * Waits a specified time while polling for confirmation
1. **Redeem Workflow**
    * Makes redeem request
    * Waits a specified time while polling for confirmation
1. **Create And Wait**    
    * Makes a create request
    * Waits a specified time while polling for request to exist in any state
1. **Redeem And Wait**
    * Makes a redeem request
    * Waits a specified time while polling for request to exist in any state
1. **Pay And Wait**
    * Makes a payment request
    * Waits a specified time while polling for request to exist in any state

### Sample usage

The Workflow API exposes the following convenience methods:

```typescript
class WorkflowAPI {
    constructor(...)
    public async createWorkflow(...);
    public async redeemWorkflow(...);
    ...
}
```

To use, add the following to your import of this module:

```typescript
import {WorkflowApi} from "xand-member-api-client";
```

Create an instance of a Workflow API by passing it configured instances of required APIs:


```typescript
const workflowApi = new WorkflowApi(walletsApi, banksApi, transactionsApi)
```

You can then make the workflow calls:

```typescript
const txn = await workflowApi.createRequestWorkflow(reqArgs);
```

## TransactionPollerApi
The [TransactionPollerApi](../client/src/workflows/transactionPollerApi.js) library is another wrapper around the Member-API typescript client. It enhances the client by implementing these commonly used polling methods.

It throws typed exceptions and provides detailed error messages that are useful for end user messaging. 

1. **Poll for transaction confirmation**
    * Polls for a specified time waiting for transaction to enter **confirmed** state.

1. **Poll for transaction resolution**
    * Polls for a specified time waiting for transaction to enter any three of the final transaction states: **confirmed**, **invalid**, **cancelled**
1. **Poll for transaction**
    * Polls for a specified time waiting for transaction to exist, in **ANY** state.

### Sample usage

The Transaction Poller API exposes the following convenience methods:

```typescript
class WorkflowAPI {
    constructor(...)
    public async awaitTransactionConfirmation(...);
    public async awaitTransactionResolution(...);
    public async awaitTransaction(...);
}
```

To use, add the following to your import of this module:

```typescript
import {TransactionPollerApi} from "xand-member-api-client";
```

Create an instance of a TransactionPollerApi by passing it a TransactionsApi APIs:


```typescript
const api = new TransactionPollerApi(transactionsApi)
```

You can then make the workflow calls:

```typescript
const txn = await api.awaitTransaction({transactionId: 123});
```

## Errors

WorkflowError is returned if anything fails. The error contains a `ErrorType` enum that identifies the error type, and a message from the source error if it exists.

class WorkflowError extends Error {
    errorType: ErrorType
}

**ErrorTypes**:
1. **cancelledTransactionError**: Create/redeem transaction resolved to `cancelled` status.
1. **createRequestFailed**: Encountered error while submitting create request to member-api. See message for more info.
1. **failedTransaction**: Create/redeem transaction resolved to `invalid` status.
1. **fetchingTransactionFailed**: Encountered error while requesting transaction status.
1. **pollingTimeoutError**: Polling timeout reached while transaction was still in `pending` status.
1. **redeemRequestFailed**: Encountered error while submitting redeem request to member-api. See message for more info.
1. **transferToClaimsAccountFailed**: Encountered error while transfering money to claims account. See message for more info.

### Polling timeout
Each convenience method polls for transaction status. 

When the retry limit is reached, and the transaction is not progress from `pending`, then the method throws a timeout exception.


#### Polling configuration

Polling duration and frequency are configurable. Configuration is optional. If not specified, a **fallback default** is provided in the [Retrier](src/retrier.ts),

To customize polling configurations, include a `PollingConfig` object in the function call parameter. 

```typescript
class PollingConfig {
    wait: number; // in milliseconds
    retries: number;
}
```

For example:
 ```typescript
 createRequestApi.createRequestWorkflow({
    address: <someaddress>,
    accountId: <someaccountid>,
    amount: <amount>,
    pollingConfig: {
        wait: 500, 
        retries: 100
    }
 });
```


