import { Retrier } from "./retrier";
import { Transaction, MemberApi, TransactionState, OperationType, TransactionsApi } from "../api";
import { WorkflowError, ErrorType } from "./errors";

export type AwaitTransactionRequest = {
    transactionId: string;
    retrier?: Retrier;
}

// tslint:disable-next-line:max-classes-per-file
export class TransactionsPollerApi {

    private api: TransactionsApi;

    constructor(transactionsApi: TransactionsApi) {
        this.api = transactionsApi;
    }

    public async awaitTransactionConfirmation(
        request: AwaitTransactionRequest) : Promise<Transaction> {
        const transactionId = request.transactionId;
        const transaction = await this.awaitTransactionResolution(request);
        if (transaction.status.state === TransactionState.Invalid)
            throw new WorkflowError(ErrorType.failedTransaction, Error(`Tx: ${transactionId} is in 'invalid' status`));
        else if (transaction.status.state === TransactionState.Cancelled)
            throw new WorkflowError(ErrorType.cancelledTransactionError, Error(`Tx: ${transactionId} is in 'cancelled' status`));
        return transaction;
    }

    public async awaitTransactionResolution(request: AwaitTransactionRequest): Promise<Transaction> {
        let retrier = request.retrier || new Retrier();
        let id = request.transactionId;
        try {
            return await retrier.execute(async () => {
                const resp = await this.api.getTransactionById(id);
                const tx = resp.data;
                
                if (tx.status.state === TransactionState.Pending) {
                    throw new WorkflowError(ErrorType.pollingTimeoutError, Error(`Timed out waiting for pending tx: ${id}`));
                }
    
                return tx;
            });
        }
        catch (error) {
            if (error.errorType === ErrorType.pollingTimeoutError)
                throw error;
            throw new WorkflowError(ErrorType.fetchingTransactionFailed, error);
        }
    }

    public async awaitTransaction(request: AwaitTransactionRequest): Promise<Transaction> {
        let retrier = request.retrier || new Retrier();
        try {
            return await retrier.execute(async () => {
                return (await this.api.getTransactionById(request.transactionId)).data
            });
        } 
        catch (error) {
            if (error.errorType === ErrorType.pollingTimeoutError)
                throw error;
            throw new WorkflowError(ErrorType.fetchingTransactionFailed, error);
        }
        
    }
}


