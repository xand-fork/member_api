import { Receipt, MemberApi, TransactionsApi} from "../api";
import { TransactionsPollerApi } from "./transactionsPollerApi";
import { WorkflowError, ErrorType } from "./errors";
import {Retrier} from "./retrier";

export class PaymentApi {
    private transactionsApi: TransactionsApi;
    private TransactionsPollerApi: TransactionsPollerApi;

    constructor(transactionsApi: TransactionsApi, TransactionsPollerApi: TransactionsPollerApi) {
        this.transactionsApi = transactionsApi;
        this.TransactionsPollerApi = TransactionsPollerApi;
    }

    public async payAndWait(request: PaymentRequest): Promise<Receipt> {
        let receipt: Receipt;
        try {
            receipt = (await this.transactionsApi.sendClaimsTransaction({
                toAddress: request.toAddress,
                amountInMinorUnit: request.amount
            })).data;
        }
        catch(error) {
            throw new WorkflowError(ErrorType.paymentRejected, error);
        }
        
        await this.TransactionsPollerApi.awaitTransaction({
            transactionId: receipt.transactionId,
            retrier: request.retrier
        });

        return receipt;
    }
}

export type PaymentRequest = {
    fromAddress: string;
    toAddress: string;
    amount: number;
    retrier?: Retrier;
}

