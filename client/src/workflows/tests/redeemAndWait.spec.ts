import { expect } from "chai";
import { RedeemApi } from "../redeem";
// @ts-ignore
import td from "testdouble";
import { MemberApi, OperationType, Receipt, TransactionsApi, TransactionState, } from "../../api";
import { TransactionsPollerApi } from "../transactionsPollerApi";
import { WorkflowError, ErrorType } from "../errors";
import { Retrier } from "../retrier";


const transactionId = "thetxid";
const address = "theaddress";
const accountId = 12345;
const amount: number = 55555555;
const fakeErrorMessage = "An API error!";

describe("RedeemAndWait", () => {
    let transactionsApi: TransactionsApi;
    let TransactionsPollerApi: TransactionsPollerApi;
    let redeemApi: RedeemApi;
    let fakedReceipt: Receipt;
    let returnedReceipt: Receipt;
    let retrier: Retrier;

    beforeEach(() => {
        retrier = new Retrier(1,1);
        transactionsApi = td.object<TransactionsApi>();
        TransactionsPollerApi = td.object<TransactionsPollerApi>();
        redeemApi = new RedeemApi(transactionsApi, TransactionsPollerApi);
    });

    describe("the redeem request is accepted", () => {
        fakedReceipt = {transactionId: transactionId};
        beforeEach(() => {
            td.when(transactionsApi.redeemRequestTransaction({accountId: accountId, amountInMinorUnit: amount}))
                .thenResolve({data: fakedReceipt});
        });

        describe("polling returns a transaction", () => {
            beforeEach(() => {
                td.when(TransactionsPollerApi.awaitTransaction({transactionId: transactionId, retrier: retrier}))
                    .thenResolve({
                        transactionId: transactionId,
                        amountInMinorUnit: amount,
                        datetime: "222222",
                        operation: OperationType.Redeem,
                        status: {state: TransactionState.Confirmed}
                    });
            });

            it("the receipt is returned", async () => {
                returnedReceipt = await redeemApi.redeemAndWait({
                    accountId: accountId,
                    address: address,
                    amount: amount,
                    retrier: retrier
                });
            
                expect(returnedReceipt).to.equal(fakedReceipt);
            });
        });

        describe("polling returns an error", () => {
            beforeEach(() => {
                td.when(TransactionsPollerApi.awaitTransaction({transactionId: transactionId}))
                    .thenReject(new WorkflowError(ErrorType.pollingTimeoutError, new Error(fakeErrorMessage)));
            });

            it("the polling error is returned", async () => {
                try {
                    returnedReceipt = await redeemApi.redeemAndWait({
                        accountId: accountId,
                        address: address,
                        amount: amount,
                        retrier: new Retrier(1,1)
                    });
                }
                catch (error){
                    expect(error.errorType).to.equal(ErrorType.pollingTimeoutError);
                    expect(error.message).to.equal(fakeErrorMessage);
                }
            });
        });
    });

    describe("the redeem request is rejected", () => {
        beforeEach(() => {
            td.when(transactionsApi.redeemRequestTransaction({accountId: accountId, amountInMinorUnit: amount}))
                .thenReject(new Error(fakeErrorMessage));
        });

        it("the redeem request failed error is thrown", async () => {
            try {
                await redeemApi.redeemAndWait({
                    accountId: accountId,
                    address: address,
                    amount: amount,
                    retrier: new Retrier(1,1)
                });
            }
            catch (error) {
                expect(error.errorType).to.equal(ErrorType.redeemRequestFailed);
                expect(error.message).to.equal(fakeErrorMessage);
            };
        });
    });
});