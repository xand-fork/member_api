import { expect } from "chai";
import { CreateApi, CreateRequestParams } from "../create";
// @ts-ignore
import td from "testdouble";
import {
  AccountsApi,
  BanksApi,
  MemberApi,
  OperationType,
  Receipt,
  Transaction,
  TransactionsApi,
  TransactionState,
} from "../../api";
import { TransactionsPollerApi } from "../transactionsPollerApi";
import { WorkflowError, ErrorType } from "../errors";
import { Retrier } from "../retrier";

export class CreateTestContext {
  public fakeTransactionsApi: TransactionsApi;
  public fakeBanksApi: BanksApi;
  public fakeAccountsApi: AccountsApi;
  public createRequestApi: CreateApi;
  private fakePollerApi: TransactionsPollerApi;
  private walletAddress: string =
    "5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ";
  private bankAccountId: number = 123456;
  private transactionId: string = "txn_id";
  private correlationId: string = "456456";
  private fakeTransaction: Transaction | null;
  private transactionReceived: Transaction | null;
  private errorReceived: any;
  private pollingError: WorkflowError | null;
  private createRequestError: WorkflowError | null;
  private claimsAcctTransferError: WorkflowError | null;
  private fakeReceipt: Receipt | null;
  private retrier: Retrier;
  private claimsAcctTransferAmtInMinorUnit: number;

  constructor() {
    this.fakeTransactionsApi = td.object<TransactionsApi>();
    this.fakeBanksApi = td.object<BanksApi>();
    this.fakePollerApi = td.object<TransactionsPollerApi>();
    this.fakeAccountsApi = td.object<AccountsApi>();
    this.createRequestApi = new CreateApi(
      this.fakeTransactionsApi,
      this.fakePollerApi
    );

    this.fakeTransaction = null;
    this.transactionReceived = null;
    this.createRequestError = null;
    this.claimsAcctTransferError = null;
    this.fakeReceipt = null;
    this.pollingError = null;
    this.claimsAcctTransferAmtInMinorUnit = -1;
    this.retrier = new Retrier(1, 1);
  }

  public setClaimsAcctTransferAmtInMinorUnit(amt: number) {
    this.claimsAcctTransferAmtInMinorUnit = amt;
    return this;
  }

  public setWalletAddress(address: string) {
    this.walletAddress = address;
    return this;
  }

  public setCreateTransactionConfirmed() {
    this.fakeTransaction = {
      transactionId: this.transactionId,
      operation: OperationType.CreateRequest,
      amountInMinorUnit: this.claimsAcctTransferAmtInMinorUnit,
      status: { state: TransactionState.Confirmed },
      datetime: "2011-10-05T14:48:00.000Z",
    };
    return this;
  }

  public setCreateTransactionPending() {
    this.fakeTransaction = {
      transactionId: this.transactionId,
      operation: OperationType.CreateRequest,
      amountInMinorUnit: this.claimsAcctTransferAmtInMinorUnit,
      status: { state: TransactionState.Pending },
      datetime: "2011-10-05T14:48:00.000Z",
    };
    return this;
  }

  public setCreateTransactionCancelled() {
    this.fakeTransaction = {
      transactionId: this.transactionId,
      operation: OperationType.CreateRequest,
      amountInMinorUnit: this.claimsAcctTransferAmtInMinorUnit,
      status: { state: TransactionState.Cancelled },
      datetime: "2011-10-05T14:48:00.000Z",
    };
    return this;
  }

  public setCreateTransactionInvalid() {
    this.fakeTransaction = {
      transactionId: this.transactionId,
      operation: OperationType.CreateRequest,
      amountInMinorUnit: this.claimsAcctTransferAmtInMinorUnit,
      status: { state: TransactionState.Invalid },
      datetime: "2011-10-05T14:48:00.000Z",
    };
    return this;
  }

  public setupCreateRequest() {
    this.fakeReceipt = { transactionId: this.transactionId, correlationId: this.correlationId };
    td.when(
      this.fakeTransactionsApi.createRequestTransaction(
        td.matchers.anything()
      )
    ).thenResolve({ data: this.fakeReceipt });
    return this;
  }

  public setupPollingReturnsFinalizedTransaction() {
    td.when(
      this.fakePollerApi.awaitTransactionConfirmation({
        transactionId: this.transactionId,
        retrier: this.retrier,
      })
    ).thenResolve(this.fakeTransaction!);
    return this;
  }

  public setupPollingReturnsCancelledTransaction() {
    this.pollingError = new WorkflowError(
      ErrorType.cancelledTransactionError,
      Error("Create request cancelled")
    );
    td.when(
      this.fakePollerApi.awaitTransactionConfirmation({
        transactionId: this.transactionId,
        retrier: this.retrier,
      })
    ).thenReject(this.pollingError);
    return this;
  }

  public setupCreateRequestThrowsError() {
    this.createRequestError = new WorkflowError(
      ErrorType.createRequestFailed,
      Error("Could not make a create request for some reason")
    );
    td.when(
      this.fakeTransactionsApi.createRequestTransaction({
        accountId: this.bankAccountId,
        amountInMinorUnit: this.claimsAcctTransferAmtInMinorUnit,
      })
    ).thenReject(this.createRequestError);
    return this;
  }

  public setupClaimsAcctTransferThrowsError() {
    this.claimsAcctTransferError = new WorkflowError(
      ErrorType.transferToClaimsAccountFailed,
      Error("Could not transfer to claims account for some reason")
    );
    td.when(
      this.fakeTransactionsApi.fundClaimsTransaction(
        td.matchers.anything(),
        td.matchers.anything()
      )
    ).thenReject(this.claimsAcctTransferError);
    return this;
  }

  public setupPollingTimesOut() {
    this.pollingError = new WorkflowError(
      ErrorType.pollingTimeoutError,
      Error("Polling timed out before a finalized transaction could be found")
    );
    td.when(
      this.fakePollerApi.awaitTransactionConfirmation({
        transactionId: this.transactionId,
        retrier: this.retrier,
      })
    ).thenReject(this.pollingError);
    return this;
  }

  public async initiateCreateWorkflow() {
    try {
      this.transactionReceived = await this.createRequestApi.createRequestWorkflow(
        {
          accountId: this.bankAccountId,
          amount: this.claimsAcctTransferAmtInMinorUnit,
          retrier: this.retrier,
        }
      );
    } catch (err) {
      this.errorReceived = err;
    }
  }

  // Requires bank and correlation id info
  public thenBankTransferWasSubmitted() {
    td.verify(
      this.fakeTransactionsApi.fundClaimsTransaction(this.transactionId, {
        amountInMinorUnit: this.claimsAcctTransferAmtInMinorUnit,
        correlationId: this.correlationId,
        bankAccountId: this.bankAccountId
      })
    );
  }

  public thenTransactionIsFinalized() {
    expect(this.transactionReceived!.status.state).to.equal(
      TransactionState.Confirmed
    );
  }

  public thenErrorReturnedIs(error: ErrorType) {
    expect(this.errorReceived.errorType).to.deep.equal(error);
  }
}
