import { expect } from "chai";
import { PaymentApi } from "../paymentApi";
// @ts-ignore
import td from "testdouble";
import { OperationType, Receipt, TransactionState, MemberApi, TransactionsApi, } from "../../api";
import { TransactionsPollerApi } from "../transactionsPollerApi";
import { WorkflowError, ErrorType } from "../errors";
import { Retrier } from "../retrier";


const transactionId = "thetxid";
const fromAddress = "from_address";
const toAddress = "to_address";
const amount: number = 55555555;
const fakeErrorMessage = "An API error!";

describe("PayAndWait", () => {
    let transactionsApi: TransactionsApi;
    let TransactionsPollerApi: TransactionsPollerApi;
    let paymentApi: PaymentApi;
    let fakedReceipt: Receipt;
    let returnedReceipt: Receipt;
    let retrier: Retrier;

    beforeEach(() => {
        retrier = new Retrier(1,1);
        transactionsApi = td.object<TransactionsApi>();
        TransactionsPollerApi = td.object<TransactionsPollerApi>();
        paymentApi = new PaymentApi(transactionsApi, TransactionsPollerApi);
    });

    describe("the payment request is accepted", () => {
        fakedReceipt = {transactionId: transactionId};
        beforeEach(() => {
            td.when(transactionsApi.sendClaimsTransaction({toAddress, amountInMinorUnit: amount}))
                .thenResolve({data: fakedReceipt});
        });

        describe("polling returns a transaction", () => {
            beforeEach(() => {
                td.when(TransactionsPollerApi.awaitTransaction({transactionId: transactionId, retrier: retrier}))
                    .thenResolve({
                        transactionId: transactionId,
                        amountInMinorUnit: amount,
                        datetime: "222222",
                        operation: OperationType.Payment,
                        status: {state: TransactionState.Confirmed}
                    });
            });

            it("the receipt is returned", async () => {
                returnedReceipt = await paymentApi.payAndWait({
                    fromAddress,
                    toAddress,
                    amount: amount,
                    retrier: retrier
                });
            
                expect(returnedReceipt).to.equal(fakedReceipt);
            });
        });

        describe("polling returns an error", () => {
            beforeEach(() => {
                td.when(TransactionsPollerApi.awaitTransaction({transactionId: transactionId}))
                    .thenReject(new WorkflowError(ErrorType.pollingTimeoutError, new Error(fakeErrorMessage)));
            });

            it("the polling error is returned", async () => {
                try {
                    returnedReceipt = await paymentApi.payAndWait({
                        fromAddress,
                        toAddress,
                        amount: amount,
                        retrier: new Retrier(1,1)
                    });
                }
                catch (error){
                    expect(error.errorType).to.equal(ErrorType.pollingTimeoutError);
                    expect(error.message).to.equal(fakeErrorMessage);
                }
            });
        });
    });

    describe("the payment request is rejected", () => {
        beforeEach(() => {
            td.when(transactionsApi.sendClaimsTransaction({toAddress, amountInMinorUnit: amount}))
                .thenReject(new Error(fakeErrorMessage));
        });

        it("the payment request failed error is thrown", async () => {
            try {
                await paymentApi.payAndWait({
                    fromAddress,
                    toAddress,
                    amount: amount,
                    retrier: new Retrier(1,1)
                });
            }
            catch (error) {
                expect(error.errorType).to.equal(ErrorType.paymentRejected);
                expect(error.message).to.equal(fakeErrorMessage);
            };
        });
    });
});