import {TransactionState} from "../../api";
import {PollingTestContext} from "./pollingTestContext"
import { Retrier } from "../retrier";

describe("Given a PollerApi", () => {
    let context: PollingTestContext;

    beforeEach(() => {
        context = new PollingTestContext();
    });

    describe("And a confirmed create request", () => {
        beforeEach(() => {
            context.addTransactionToReturnSequence(TransactionState.Confirmed);
        });

        describe("When I awaitTransactionConfirmation", () => {
            beforeEach(async () => await context.awaitConfirmation());

            it("Then it returns a confirmed transaction", () => {
                context.thenTransactionIsReturnedWithState(TransactionState.Confirmed)
            });
        });

        describe("When I awaitTransaction", () => {
            beforeEach(async () => await context.awaitTransaction());

            it("Then it returns a confirmed transaction", () => {
                context.thenTransactionIsReturnedWithState(TransactionState.Confirmed)
            });
        });

        describe("When I awaitResolution", () => {
            beforeEach(async () => await context.awaitResolution());

            it("Then it returns a confirmed transaction", () => {
                context.thenTransactionIsReturnedWithState(TransactionState.Confirmed)
            });
        });

    });

    describe("And a invalid create request", () => {
        beforeEach(() => context
            .addTransactionToReturnSequence(TransactionState.Invalid));

        describe("When I awaitTransactionConfirmation", () => {

            beforeEach(async () => await context.awaitConfirmation());

            it("Then it gives failed transaction error", () => {
                context.thenFailedTransactionErrorReturned()
            });
        });

        describe("When I awaitTransaction", () => {
            beforeEach(async () => await context.awaitTransaction());

            it("Then it returns an invalid transaction", () => {
                context.thenTransactionIsReturnedWithState(TransactionState.Invalid)
            });
        });

        describe("When I awaitTransaction", () => {
            beforeEach(async () => await context.awaitTransaction());

            it("Then it returns an invalid transaction", () => {
                context.thenTransactionIsReturnedWithState(TransactionState.Invalid)
            });
        });

    });

    describe("And pending create request", () => {
        beforeEach(() => context
            .addTransactionToReturnSequence(TransactionState.Pending));

        describe("When I await TransactionConfirmation", () => {
            beforeEach(async () => await context.awaitConfirmation());

            it("Then it gives polling timeout error", () => {
                context.thenTimeoutErrorReturned();
            });
        });

        describe("When I await Transaction", () => {
            beforeEach(async () => await context.awaitTransaction());

            it("Then it returns an pending transaction", () => {
                context.thenTransactionIsReturnedWithState(TransactionState.Pending);
            });
        });

        describe("And the transaction transitions to confirmed status on 2nd request", () => {
            beforeEach(() => {
                context.addTransactionToReturnSequence(TransactionState.Confirmed);
            });

            describe("When I awaitTransactionConfirmation with retrier: {retries: 1, delay: 1}", () => {
                beforeEach(async () => {
                    context.useRetrier(new Retrier(1, 1));
                    await context.awaitConfirmation()
                });

                it("Then it returns the timeout error", () => {
                    context.thenTimeoutErrorReturned();
                });
            });

            describe("When I awaitTransactionConfirmation with retrier: {retries: 10, delay: 1}", () => {
                beforeEach(async () => {
                    context.useRetrier(new Retrier(2, 1));
                    await context.awaitConfirmation()
                });

                it("Then it returns the confirmed create request", () => {
                    context.thenTransactionIsReturnedWithState(TransactionState.Confirmed);
                });
            });

            describe("When I awaitTransactionConfirmation with retrier: {retries: 2, delay: 200}", () => {
                beforeEach(async () => {
                    context.useRetrier(new Retrier(2, 200));
                    await context.awaitConfirmation()
                });

                it("Then it takes >199ms to return the confirmed create request", () => {
                    context.thenPollerTakeMoreThanMilliSecsToComplete(195);
                    context.thenTransactionIsReturnedWithState(TransactionState.Confirmed);
                });
            });
        });
    });

    

    describe("And GetTransactionById returns an error", () => {
        beforeEach(() => {
            context.addErrorToReturnSequence();
            context.useRetrier(new Retrier(2, 1));
        });

        describe("When I awaitTransactionConfirmation", () => {
            beforeEach(async () => {
                context.addErrorToReturnSequence();
                await context.awaitConfirmation();
            });

            it("Then it returns an API error", () => {
                context.thenApiErrorReturned()
            });
        });

        describe("And the transaction transitions to confirmed status on 2nd request", () => {
            beforeEach(() => {
                context.addTransactionToReturnSequence(TransactionState.Confirmed);
            });

            describe("When I awaitConfirmation", () => {
                beforeEach(async () => {
                    await context.awaitConfirmation()
                });

                it("Then it returns a confirmed transaction", () => {
                    context.thenTransactionIsReturnedWithState(TransactionState.Confirmed);
                });
            });
            
            describe("When I awaitTransaction", () => {
                beforeEach(async () => {
                    await context.awaitTransaction()
                });

                it("Then it returns a confirmed transaction", () => {
                    context.thenTransactionIsReturnedWithState(TransactionState.Confirmed);
                });
            });

            describe("When I awaitTransaction", () => {
                beforeEach(async () => {
                    await context.awaitResolution()
                });

                it("Then it returns a confirmed transaction", () => {
                    context.thenTransactionIsReturnedWithState(TransactionState.Confirmed);
                });
            });
        });

        describe("And the transaction transitions to pending status on 2nd request", () => {
            beforeEach(() => {
                context.addTransactionToReturnSequence(TransactionState.Pending);
            });

            describe("When I awaitConfirmation", () => {
                beforeEach(async () => {
                    await context.awaitConfirmation();
                });

                it("Then it returns timeout error", () => {
                    context.thenTimeoutErrorReturned();
                });
            });

            describe("When I awaitTransaction", () => {
                beforeEach(async () => {
                    await context.awaitTransaction();
                });

                it("Then it returns a pending transaction", () => {
                    context.thenTransactionIsReturnedWithState(TransactionState.Pending);
                });
            });

            describe("When I awaitResolution", () => {
                beforeEach(async () => {
                    await context.awaitResolution();
                });

                it("Then it returns timeout error", () => {
                    context.thenTimeoutErrorReturned();
                });
            });
        });

        describe("And the transaction transitions to invalid status on 2nd request", () => {
            beforeEach(() => {
                context.addTransactionToReturnSequence(TransactionState.Invalid);
            });

            describe("When I awaitConfirmation", () => {
                beforeEach(async () => {
                    await context.awaitConfirmation();
                });

                it("Then it returns a confirmed transaction", () => {
                    context.thenFailedTransactionErrorReturned();
                });
            });

            describe("When I awaitTransaction", () => {
                beforeEach(async () => {
                    await context.awaitTransaction();
                });

                it("Then it returns a invalid transaction", () => {
                    context.thenTransactionIsReturnedWithState(TransactionState.Invalid);
                });
            });

            describe("When I awaitResolution", () => {
                beforeEach(async () => {
                    await context.awaitResolution();
                });

                it("Then it returns a invalid transaction", () => {
                    context.thenTransactionIsReturnedWithState(TransactionState.Invalid);
                });
            });
        });

        describe("And the transaction transitions to invalid status on 2nd request", () => {
            beforeEach(() => {
                context.addTransactionToReturnSequence(TransactionState.Cancelled);
            });

            describe("When I awaitConfirmation", () => {
                beforeEach(async () => {
                    await context.awaitConfirmation()
                });

                it("Then it returns a confirmed transaction", () => {
                    context.thenCancelledErrorReturned();
                });
            });

            describe("When I awaitTransaction", () => {
                beforeEach(async () => {
                    await context.awaitTransaction()
                });

                it("Then it returns a invalid transaction", () => {
                    context.thenTransactionIsReturnedWithState(TransactionState.Cancelled);
                });
            });

            describe("When I awaitResolution", () => {
                beforeEach(async () => {
                    await context.awaitResolution()
                });

                it("Then it returns a invalid transaction", () => {
                    context.thenTransactionIsReturnedWithState(TransactionState.Cancelled);
                });
            });
        });
    });
});
