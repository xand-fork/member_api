import { expect } from "chai";
// @ts-ignore
import td from "testdouble";
import {
    OperationType,
    MemberApi,
    Transaction,
    TransactionState,
    TransactionsApi,
} from "../../api";
import { WorkflowError, ErrorType} from "../errors";
import { TransactionsPollerApi } from "../transactionsPollerApi";
import { Retrier } from "../retrier";

const singlePollingAttempt = {wait: 1, retries: 1};
const singleRetrier = new Retrier(1, 1);

export class PollingTestContext {
    private fakeTransactionsApi: TransactionsApi;
    private transactionId: string = "123123";
    private retrier: Retrier;
    private pollerApi: TransactionsPollerApi;
    private returnedTransaction: Transaction | null;
    private getTransactionByIdReturnSequence: any[];
    private returnedError: WorkflowError | null;
    private elapsedTime: number;
    private readonly apiErrorMessage = "Some API error";

    constructor() {
        this.fakeTransactionsApi = td.object<TransactionsApi>();
        this.pollerApi = new TransactionsPollerApi(this.fakeTransactionsApi);
        this.getTransactionByIdReturnSequence = [];
        this.retrier = singleRetrier;

        this.returnedError = null;
        this.returnedTransaction = null;
        this.elapsedTime = -1;
    }

    public async awaitConfirmation() {
        if (this.getTransactionByIdReturnSequence.length > 0)
            this.setupMockTransactionReturnSequence();

        try {
            const startTime = Date.now();
            this.returnedTransaction = await this.pollerApi.awaitTransactionConfirmation(
                {
                    transactionId: this.transactionId,
                    retrier: this.retrier,
                });
            this.elapsedTime = Date.now() - startTime;
        }
        catch (error) {
            this.returnedError = error
        }
    }

    public async awaitTransaction() {
        if (this.getTransactionByIdReturnSequence.length > 0)
            this.setupMockTransactionReturnSequence();

        try {
            const startTime = Date.now();
            this.returnedTransaction = await this.pollerApi.awaitTransaction(
                {
                    transactionId: this.transactionId,
                    retrier: this.retrier,
                });
            this.elapsedTime = Date.now() - startTime;
        }
        catch (error) {
            this.returnedError = error
        }
    }

    public async awaitResolution() {
        if (this.getTransactionByIdReturnSequence.length > 0)
            this.setupMockTransactionReturnSequence();

        try {
            const startTime = Date.now();
            this.returnedTransaction = await this.pollerApi.awaitTransactionResolution(
                {
                    transactionId: this.transactionId,
                    retrier: this.retrier,
                });
            this.elapsedTime = Date.now() - startTime;
        }
        catch (error) {
            this.returnedError = error
        }
    }

    private setupMockTransactionReturnSequence() {
        const returnSequence = this.getTransactionByIdReturnSequence.map(this.wrapReturnSequence).reverse();
        td.when(this.fakeTransactionsApi.getTransactionById(this.transactionId))
            .thenDo(() => {
                let value = returnSequence.pop();
                if (value instanceof Error) {
                    throw value;
                }
                return value;
            });
    }

    private wrapReturnSequence(response: any) {
        if (response instanceof Error) {
            return response;
        }
        return {data: response};
    }

    public thenTransactionIsReturnedWithState(state: TransactionState) {
        expect(this.returnedTransaction).to.exist;
        expect(this.returnedTransaction!.status.state).to.equal(state);
    }

    public addErrorToReturnSequence() {
        this.getTransactionByIdReturnSequence.push(new Error(this.apiErrorMessage));
    }

    public addTransactionToReturnSequence(state: TransactionState) {
        const fakeTransaction = {
            transactionId: this.transactionId,
            operation: OperationType.CreateRequest,
            amountInMinorUnit: 534255342,
            status: {state},
            datetime: "2011-10-05T14:48:00.000Z"
        };
        this.getTransactionByIdReturnSequence.push(fakeTransaction);
    }

    public thenFailedTransactionErrorReturned() {
        expect(this.returnedError!.errorType).to.equal(ErrorType.failedTransaction);
        expect(this.returnedError!.message).to.equal(`Tx: ${this.transactionId} is in 'invalid' status`);
    }

    public thenCancelledErrorReturned() {
        expect(this.returnedError!.errorType).to.equal(ErrorType.cancelledTransactionError);
        expect(this.returnedError!.message).to.equal(`Tx: ${this.transactionId} is in 'cancelled' status`);
    }

    public thenTimeoutErrorReturned() {
        expect(this.returnedError!.errorType).to.equal(ErrorType.pollingTimeoutError);
    }

    public useRetrier(retrier: Retrier) {
        this.retrier = retrier;
    };

    public thenPollerTakeMoreThanMilliSecsToComplete(milliSecs: number) {
        expect(this.elapsedTime).is.greaterThan(milliSecs)
    }



    public thenApiErrorReturned() {
        expect(this.returnedError).to.exist
        expect(this.returnedError!.errorType).to.equal(ErrorType.fetchingTransactionFailed)
        expect(this.returnedError!.message).to.contain(this.apiErrorMessage);
    }
}
