import { expect } from "chai";
import { CreateApi } from "../create";
// @ts-ignore
import td from "testdouble";
import { OperationType, Receipt, TransactionState, BanksApi, MemberApi, AccountsApi, TransactionsApi} from "../../api";
import { TransactionsPollerApi } from "../transactionsPollerApi";
import { WorkflowError, ErrorType } from "../errors";
import { Retrier } from "../retrier";


const transactionId = "thetxid";
const address = "theaddress";
const accountId = 123456;
const amount: number = 55555555;
const fakeErrorMessage = "An API error!";

describe("CreateAndWait", () => {
    let transactionsApi: TransactionsApi;
    let TransactionsPollerApi: TransactionsPollerApi;
    let api: CreateApi;
    let fakedReceipt: Receipt;
    let returnedReceipt: Receipt;
    let retrier: Retrier;

    beforeEach(() => {
        retrier = new Retrier(1,1);
        transactionsApi = td.object<TransactionsApi>();
        TransactionsPollerApi = td.object<TransactionsPollerApi>();
        api = new CreateApi(transactionsApi, TransactionsPollerApi);
    });

    describe("the create request is accepted", () => {
        fakedReceipt = {transactionId: transactionId};
        beforeEach(() => {
            td.when(transactionsApi.createRequestTransaction({accountId: accountId, amountInMinorUnit: amount}))
                .thenResolve({data: fakedReceipt});
        });

        describe("polling returns a transaction", () => {
            beforeEach(() => {
                td.when(TransactionsPollerApi.awaitTransaction({transactionId: transactionId, retrier: retrier}))
                    .thenResolve({
                        transactionId: transactionId,
                        amountInMinorUnit: amount,
                        datetime: "222222",
                        operation: OperationType.Create,
                        status: {state: TransactionState.Cancelled}
                    });
            });

            it("the receipt is returned", async () => {
                returnedReceipt = await api.createAndWait({
                    accountId: accountId,
                    amount: amount,
                    retrier: retrier
                });
            
                expect(returnedReceipt).to.equal(fakedReceipt);
            });
        });

        describe("polling returns an error", () => {
            beforeEach(() => {
                td.when(TransactionsPollerApi.awaitTransaction({transactionId: transactionId}))
                    .thenReject(new WorkflowError(ErrorType.pollingTimeoutError, new Error(fakeErrorMessage)));
            });

            it("the polling error is returned", async () => {
                try {
                    returnedReceipt = await api.createAndWait({
                        accountId: accountId,
                        amount: amount,
                        retrier: new Retrier(1,1)
                    });
                }
                catch (error){
                    expect(error.errorType).to.equal(ErrorType.pollingTimeoutError);
                    expect(error.message).to.equal(fakeErrorMessage);
                }
            });
        });
    });

    describe("the create request is rejected", () => {
        beforeEach(() => {
            td.when(transactionsApi.createRequestTransaction({accountId: accountId, amountInMinorUnit: amount}))
                .thenReject(new Error(fakeErrorMessage));
        });

        it("the create request failed error is thrown", async () => {
            try {
                await api.createAndWait({
                accountId: accountId,
                amount: amount,
                retrier: new Retrier(1,1)})
            }
            catch (error) {
                expect(error.errorType).to.equal(ErrorType.createRequestFailed);
                expect(error.message).to.equal(fakeErrorMessage);
            };
        });
    });
});