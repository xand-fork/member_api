import { Receipt, MemberApi, TransactionsApi } from "../api";
import { TransactionsPollerApi } from "./transactionsPollerApi"
import { WorkflowError, ErrorType } from "./errors";
import { Retrier } from "./retrier";

export class RedeemApi {
    private transactionsApi: TransactionsApi;
    private TransactionsPollerApi: TransactionsPollerApi;

    constructor(transactionsApi: TransactionsApi, TransactionsPollerApi: TransactionsPollerApi) {
        this.transactionsApi = transactionsApi;
        this.TransactionsPollerApi = TransactionsPollerApi;
    }

    public async redeemRequestWorkflow(request: RedeemRequest): Promise<Receipt> {
        let receipt: Receipt;
        try {
            receipt = (await this.transactionsApi.redeemRequestTransaction({
                accountId: request.accountId,
                amountInMinorUnit: request.amount})).data;
        }
        catch (error) {
            throw new WorkflowError(ErrorType.redeemRequestFailed, error);
        }
        await this.TransactionsPollerApi.awaitTransactionConfirmation(
            {transactionId: receipt.transactionId, retrier: request.retrier}
            );

        return receipt;
    }

    public async redeemAndWait(request: RedeemRequest): Promise<Receipt> {
        let receipt: Receipt;
        try {
            receipt = (await this.transactionsApi.redeemRequestTransaction({
                accountId: request.accountId,
                amountInMinorUnit: request.amount
            })).data;
        }
        catch(error) {
            throw new WorkflowError(ErrorType.redeemRequestFailed, error);
        }
        
        await this.TransactionsPollerApi.awaitTransaction({
            transactionId: receipt.transactionId,
            retrier: request.retrier
        });

        return receipt;
    }
}

// tslint:disable-next-line:max-classes-per-file
export type RedeemRequest = {
    address: string;
    accountId: number;
    amount: number;
    retrier?: Retrier;
}

