#!/usr/bin/env python3
import argparse
import sys

parser = argparse.ArgumentParser(description='Check if new tag is properly incremented.')
parser.add_argument('new_tag', type=str, help='The proposed tag to check')
parser.add_argument('existing_tags', type=str, help='file containing existing tags')

args = parser.parse_args()


def format_tag(tag):
    if tag.startswith("v"):
        return tag[1:]
    else:
        return tag


# This will check if the old_tag is higher than the new tag.
# It will only compare them if they are release tags (e.g. -beta tags are ignored).
# If the old_tag major or minor version is higher then it isn't considered.
def is_higher(proposed_tag, old_tag):
    # split tags into main semver components
    old_tag = old_tag.split(".")
    proposed_tag = proposed_tag.split(".")

    return len(old_tag) >= 3 and old_tag[2].isdigit() and len(proposed_tag) >= 3 and proposed_tag[2].isdigit() and \
           old_tag[0] == proposed_tag[0] and \
           old_tag[1] == proposed_tag[1] and \
           old_tag[2] > proposed_tag[2]


with open(args.existing_tags) as tag_file:
    existing_tags = tag_file.read().splitlines()

    # exclude the current tag from the comparison set
    if args.new_tag in existing_tags:
        existing_tags.remove(args.new_tag)
    # strip 'v' prefixes
    existing_tags = list(map(format_tag, existing_tags))


conflicting_tags = list(filter(lambda x: is_higher(format_tag(args.new_tag), x), existing_tags))

if len(conflicting_tags) > 0:
    sys.exit("conflicting tags: " + str(conflicting_tags))
