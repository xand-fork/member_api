#!/bin/bash

set -e         # exit early on error
set -x         # print commands before executing
set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable
set -o pipefail # abort if command in pipeline fails

if [ -n "${CI_COMMIT_TAG:-}" ]; then
    git tag -l > tags.txt
    ./tag-version-check.py $CI_COMMIT_TAG tags.txt
fi

echo Verifying $1:$2 does not already exist in the registry.

IMAGE_EXISTS=$(./does-image-exist.sh $1:$2)
if [[ $IMAGE_EXISTS == "yes" ]];
then
    echo "ERROR: The docker image $1:$2 already exists."
    exit 1
fi

### TESTS
### ./check_publish
###  -> should error out with unbound variable

### ./check_publish IMAGE NOT_EXISTING_TAG
### echo $?
### 0

### ./check_publish IMAGE EXISTING_TAG 
### echo $?
### -1
