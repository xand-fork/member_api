NAME := member-api-ci
FULL_NAME := gcr.io/xand-dev/$(NAME)
VERSION := 1.2.0

IMAGE_TAG := $(FULL_NAME):$(VERSION)
CONTAINER_NAME := $(NAME)

RUST_VERSION := 1.65.0
NODE_VERSION := 12

print:
	@echo "Name               : $(FULL_NAME)"
	@echo "Version            : $(VERSION)"
	@echo "CONTAINER_NAME     : $(CONTAINER_NAME)"
	@echo "IMAGE_TAG          : $(IMAGE_TAG)"

build: print
	@echo "Building ${IMAGE_TAG}."
	docker build \
		--build-arg NODE_VERSION=$(NODE_VERSION) \
		--build-arg RUST_VERSION=$(RUST_VERSION) \
		--no-cache \
		-f Dockerfile \
		-t "${IMAGE_TAG}" ./

	docker tag ${IMAGE_TAG} $(FULL_NAME):latest
	docker tag ${IMAGE_TAG} rust-$(RUST_VERSION)
	docker tag ${IMAGE_TAG} node-lts-$(NODE_VERSION)

check-version: print
	# Check if this specific version has already been released by someone else.
	./check-publish.sh $(FULL_NAME) $(VERSION)

publish: check-version
	docker push "${IMAGE_TAG}"
