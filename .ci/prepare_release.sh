#!/usr/bin/env bash

set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable

function helptext {
HELPTEXT=$(cat << END
    This script is intended to be run by humans.
    The purpose of this script is to be able to automate the incrementation of a version and
    and preparing it for the ci system along with pushing.

    There should not be any untracked changes in your repository before incrementing.

    Arguments
        SEMVER_INCREMENT (Required) = The type of semversion increment that will be made by this script.
END
)
echo "$HELPTEXT"
}

function error {
    echo $1
    echo
    echo "$(helptext)"
}

#No Arguments
if [[ $# -eq 0 ]] ; then
    echo "$(helptext)"
    exit 1
fi

SEMVER_INCREMENT=$1

case $SEMVER_INCREMENT in
     major)
          INCREMENT_FLAG="-M"
          ;;
     minor)
          INCREMENT_FLAG="-m"
          ;;
     patch)
          INCREMENT_FLAG="-p"
          ;;
     *)
          error "ERROR: The SEMVER_INCREMENT must be major, minor or patch."
          exit 1
          ;;
esac

git diff-index --quiet HEAD && DIFF=$? || DIFF=$?

if [[ $DIFF -ne 0 ]] ; then
    error "ERROR: You have tracked changes that are uncommitted. You should commit or clear before publishing a version."
    exit 1
fi

source ${BASH_SOURCE%/*}/util.sh

CURRENT_VERSION=$(get_version member-api/Cargo.toml)
echo "Retrieved existing version: $CURRENT_VERSION"
NEXT_VERSION=$(increment_version $INCREMENT_FLAG $CURRENT_VERSION)
echo "Determined next release version: $NEXT_VERSION."
set_version member-api/Cargo.toml $NEXT_VERSION
echo "Set version in Cargo.toml"

# This should make sure a compile works and increment
# the version in the Cargo.lock file.
cargo build -p member-api

git commit -am "v$NEXT_VERSION"
echo "Committed locally: v$NEXT_VERSION"
