#!/bin/bash

# Sets the version of member-api-config to match that of member-api itself in your working tree,
# updating the cross-reference dependency on the config grate in the process. 

set -e

# https://stackoverflow.com/questions/59895/how-to-get-the-source-directory-of-a-bash-script-from-within-the-script-itself#answer-53183593
DIR="$( realpath $( dirname "${BASH_SOURCE[0]}") )"

# Same args as "toml set": file name, key, value
toml_set() {
    NEW_CONTENTS=$(toml set "$1" $2 "$3")
    echo "$NEW_CONTENTS" > "$1"
}

MASTER_TOML="$DIR/../member-api/Cargo.toml"
CONFIG_TOML="$DIR/../member-api-config/Cargo.toml"
SERVER_TOML="$DIR/../member-api-server/Cargo.toml"

MASTER_VERSION=$(toml get "$MASTER_TOML" package.version | tr -d \")
toml_set "$CONFIG_TOML" package.version "$MASTER_VERSION"
toml_set "$SERVER_TOML" package.version "$MASTER_VERSION"
toml_set "$MASTER_TOML" dependencies.member-api-config.version "$MASTER_VERSION"
