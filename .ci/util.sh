function get_version {
    local FILENAME=$1
    echo $(toml get $FILENAME package.version | tr -d '\"')
}

function set_version {
    local FILENAME=$1
    local NEXT_VERSION=$2
    UPDATED_CARGO_TOML="$(toml set $FILENAME package.version $NEXT_VERSION)"
    echo "$UPDATED_CARGO_TOML" > $FILENAME
}

function increment_version {
    echo "$(${BASH_SOURCE%/*}/increment_version.sh $@)"
}
