#!/bin/bash

set -e
set -o nounset # abort on unbound variable

function helptext {
HELPTEXT=$(cat << END
    This script is intended to be run by [humans|automation]
    Given input, this script builds a beta version and modifies the Member API's Cargo.toml, so beta
    crates can be published in CI.

    Arguments
        BETA_ID (Required) = ID value to append to build the crate's beta version
END
)
echo "$HELPTEXT"
}

function error {
    echo "$@"
    echo
    echo "$(helptext)"
    exit 1
}

if [[ $# -eq 0 ]] ; then
    error 'No arguments provided'
fi

BETA_ID=${1:?"$(error 'BETA_ID must be passed' )"}
# https://stackoverflow.com/questions/59895/how-to-get-the-source-directory-of-a-bash-script-from-within-the-script-itself#answer-53183593
DIR="$( realpath $( dirname "${BASH_SOURCE[0]}") )"

MASTER_TOML="$DIR/../member-api/Cargo.toml"

CURRENT_VERSION=$(toml get "$MASTER_TOML" package.version | tr -d '"')
BETA_VERSION="$CURRENT_VERSION-beta.${BETA_ID}"

echo "Setting beta version in "$MASTER_TOML" -> $BETA_VERSION"
NEW_CARGO_TOML=$(toml set "$MASTER_TOML" package.version $BETA_VERSION)
echo "$NEW_CARGO_TOML" > "$MASTER_TOML"
